<?php

namespace App\Services;

use Illuminate\Support\Facades\Session;
use Twilio\Rest\Client;
use Illuminate\Support\Facades\Mail;
use App\Mail\EmailVerificationOTP;

class OTPService
{
    public static function generateOTP()
    {
        return rand(100000, 999999); // Generate a 6-digit OTP
    }

    public static function sendOTPToPhone($phone, $otp)
    {
        // You need to set your Twilio credentials in the .env file
        $sid = env('TWILIO_SID');
        $token = env('TWILIO_TOKEN');
        $from = env('TWILIO_FROM');

        $client = new Client($sid, $token);

        $client->messages->create(
            $phone,
            [
                'from' => $from,
                'body' => "Your OTP code is $otp"
            ]
        );
    }

    public static function sendOTPToEmail($email, $otp)
    {
        // Use Laravel's Mail functionality to send OTP to email
        Mail::to($email)->send(new EmailVerificationOTP($otp));
    }

    public static function saveOTPToSession($key, $otp)
    {
        Session::put($key, $otp);
        Session::put("{$key}_expiry", now()->addMinutes(10)); // Set expiry time of 10 minutes
    }

    public static function verifyOTP($key, $inputOtp)
    {
        $storedOtp = Session::get($key);
        $expiryTime = Session::get("{$key}_expiry");

        if (now()->greaterThan($expiryTime)) {
            Session::forget([$key, "{$key}_expiry"]);
            return false; // OTP expired
        }

        if ($storedOtp && $storedOtp == $inputOtp) {
            Session::forget([$key, "{$key}_expiry"]);
            return true;
        }

        return false;
    }
}
