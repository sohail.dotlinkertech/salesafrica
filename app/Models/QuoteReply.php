<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuoteReply extends Model
{
    use HasFactory;
    
    protected $primarykey='id';
    protected $table='quote_replies';


    protected $fillable=[
          'quote_id',
          'sender_id',
          'title',
          'attachments',
          'reply_text'
    ];

    
    public function quote()
    {
        return $this->belongsTo(Quote::class, 'quote_id', 'id');
    }

}
