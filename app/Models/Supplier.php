<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'company',
        'location',
        'business_category',
        'company_website',
        'foundation_year',
        'contact_person',
        'title',
        'country',
        'city',
        'industries',
        'introduction',
        'inspection_report',
        'letter_of_authorization',
        'image',
        'individual'
    ];

    protected $casts = [
        'industries' => 'array',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
