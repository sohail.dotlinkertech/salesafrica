<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Cache;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $fillable = [
        'name',
        'first_name',
        'last_name',
        'email',
        'phone',
        'password',
        'gender',
        'role',
        'image'
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];


    public function distributor()
    {
        return $this->hasOne(Distributor::class, 'user_id');
    }

    public function salesagent()
    {
        return $this->hasOne(SalesAgent::class, 'user_id');
    }
    public function supplier()
    {
        return $this->hasOne(Supplier::class);
    }

    // public function distributor()
    // {
    //     return $this->hasOne(Distributor::class);
    // }

    public function conversationsAsSender()
    {
        return $this->hasMany(Conversation::class, 'sender_id');
    }

    public function conversationsAsReceiver()
    {
        return $this->hasMany(Conversation::class, 'receiver_id');
    }

    public function sentMessages()
    {
        return $this->hasMany(Message::class, 'sender_id');
    }

    public function receivedMessages()
    {
        return $this->hasMany(Message::class, 'receiver_id');
    }

    public function unreadMessages()
    {
        return $this->receivedMessages()->where('is_read', false);
    }

    public function unreadMessagesCount()
    {
        return $this->unreadMessages()->count();
    }


    public function isOnline()
    {
        return Cache::has('user-is-online-' . $this->id);
    }
}
