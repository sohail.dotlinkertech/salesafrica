<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{
    use HasFactory;

    protected $primaryKey = 'id';
    protected $table = 'quotes';

    protected $fillable = [
        'user_id',
        'title',
        'description'
    ];

    public function replies()
    {
        return $this->hasMany(QuoteReply::class, 'quote_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function supplier()
    {
        return $this->belongsTo(Supplier::class, 'user_id', 'user_id');
    }

    public function salesAgent()
    {
        return $this->belongsTo(SalesAgent::class, 'user_id', 'user_id');
    }

    public function distributor()
    {
        return $this->belongsTo(Distributor::class, 'user_id', 'user_id');
    }

    // Define the accessor for related user
    public function getRelatedUserAttribute()
    {
        if ($this->user->role == 3) {
            return $this->supplier;
        }
         elseif($this->user->role == 4) {
            return $this->distributor;
        }
        else{
            return $this->salesAgent;
        }
    }
}
