<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SalesAgent extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'salesagent_type',
        'location',
        'business_category',
        'company_website',
        'company',
        'date_of_birth',
        'country',
        'city',
        'industries',
        'introduction',
        'image',
        'individual'
    ];

    protected $casts = [
        'industries' => 'array',
    ];
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

}
