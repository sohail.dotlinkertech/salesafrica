<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Distributor extends Model
{
    use HasFactory;

    protected $primarykey = 'id';
    protected $table = 'distributors';
    protected $fillable = [
        'user_id',
        'distributor_type',
        'company',
        'location',
        'business_category',
        'company_website',
        'foundation_year',
        'contact_person',
        'title',
        'country',
        'city',
        'industries',
        'company_registration_paper',
        'introduction',
        'account_type',
        'status',
        'shop_photos',
        'individual'
    ];

    protected $casts = [
        'industries' => 'array',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
