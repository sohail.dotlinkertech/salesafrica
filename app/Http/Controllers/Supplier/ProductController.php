<?php

namespace App\Http\Controllers\Supplier;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    //
    public function productsSupplier()
    {
        $products=Product::all();
        return view('frontend.supplier.products',compact('products'));
    }

    public function addProductsSupplier()
    {
        return view('frontend.supplier.add-product'); 
    }

    public function store(Request $request)
    { 
        // Validate incoming request
        $request->validate([
            'product_name' => 'required|string|max:255',
            'category' => 'required|string|max:255',
            'image' => 'required|image|max:2048' 
        ]);
    
        // Handle image upload
        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = time() . '_' . $image->getClientOriginalName();
            $image->move(public_path('uploads/product'), $imageName);
        } else {
            $imageName = null; 
        }
    
        // Create new product record
        Product::create([
            'product_name' => $request->product_name,
            'category' => $request->category,
            'image' => $imageName, 
            'user_id' => Auth::id() 
        ]);
    
        return redirect()->route('supplier.products')->with('success', 'Product added successfully.');
    }
    
}
