<?php

namespace App\Http\Controllers\Supplier;

use App\Http\Controllers\Controller;
use App\Models\Supplier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{


    public function editSupplier()
    {
        // Get the authenticated user
        $user = Auth::user();

        // Get the supplier details associated with the user
        $supplier = Supplier::where('user_id', $user->id)->firstOrFail();

        // Pass the supplier data to the view
        return view('frontend.supplier.edit-supplier-profile', compact('supplier'));
    }


    public function update(Request $request, $id)
    {
        // dd($request->all());
        // Validate the incoming request data
        $request->validate([
            'company' => 'nullable|string|max:255',
            'location' => 'nullable|string|max:255',
            'country' => 'required|string|max:255',
            'business_category' => 'nullable|string|max:255',
            'company_website' => 'nullable|url|max:255',
            'inspection_report' => 'nullable|image|file|mimes:pdf,doc,docx,png,jpg,gif,svg|max:2048',
            'letter_of_authorization' => 'nullable|image|file|mimes:pdf,doc,docx,png,jpg,gif,svg|max:2048',
            'first_name' => 'nullable|string|max:255',
            'last_name' => 'nullable|string|max:255',
            'email' => 'nullable|email|max:255',
            'phone' => 'nullable|string|max:25',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $supplier = Supplier::findOrFail($id);

        // Retrieve the related user
        $user = $supplier->user;

        // Handle file uploads if they exist
        if ($request->hasFile('inspection_report')) {
            // Delete old inspection report
            if ($supplier->inspection_report) {
                $oldInspectionReportPath = public_path('uploads/inspection_reports/' . $supplier->inspection_report);
                if (file_exists($oldInspectionReportPath)) {
                    unlink($oldInspectionReportPath);
                }
            }

            // Save new inspection report
            $inspectionReport = $request->file('inspection_report');
            $inspectionReportName = time() . '_' . $inspectionReport->getClientOriginalName();
            $inspectionReport->move(public_path('uploads/inspection_reports'), $inspectionReportName);
            $supplier->inspection_report = $inspectionReportName;
        }

        if ($request->hasFile('letter_of_authorization')) {
            // Delete old letter of authorization
            if ($supplier->letter_of_authorization) {
                $oldLetterOfAuthorizationPath = public_path('uploads/authorization_letters/' . $supplier->letter_of_authorization);
                if (file_exists($oldLetterOfAuthorizationPath)) {
                    unlink($oldLetterOfAuthorizationPath);
                }
            }

            // Save new letter of authorization
            $letterOfAuthorization = $request->file('letter_of_authorization');
            $letterOfAuthorizationName = time() . '_' . $letterOfAuthorization->getClientOriginalName();
            $letterOfAuthorization->move(public_path('uploads/authorization_letters'), $letterOfAuthorizationName);
            $supplier->letter_of_authorization = $letterOfAuthorizationName;
        }

        if ($request->hasFile('image')) {
            // Delete old image
            if ($supplier->image) {
                $oldImagePath = public_path('uploads/' . $supplier->image);
                if (file_exists($oldImagePath)) {
                    unlink($oldImagePath);
                }
            }

            // // Save new image
            // $image = $request->file('image');
            // $imageName = time() . '_' . $image->getClientOriginalName();
            // $image->move(public_path('uploads'), $imageName);
            // $supplier->image = $imageName;

            // Save new image
            $image = $request->file('image');
            $imageName = time() . '_' . $image->getClientOriginalName();
            $image->move(public_path('uploads/profile'), $imageName);
            $imageUrl = url('/uploads/profile/' . $imageName);
            $supplier->image = $imageUrl; // Store the URL in the database
        }

        // Update the supplier's data
        $supplier->update([
            'company' => $request->input('company'),
            'location' => $request->input('location'),
            'country' => $request->input('country'),
            'business_category' => $request->input('business_category'),
            'company_website' => $request->input('company_website'),
            // 'first_name' => $request->input('first_name'),
            // 'last_name' => $request->input('last_name'),
            // 'email' => $request->input('email'),
            // 'phone' => $request->input('phone'),
            'inspection_report' => $supplier->inspection_report ?? null,
            'letter_of_authorization' => $supplier->letter_of_authorization ?? null,
            'image' => $supplier->image ?? null,
        ]);

        $user->update([
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'email' => $request->input('email'),
            'phone' => $request->input('phone'),
            'image' => $supplier->image ?? null,

        ]);

        // Redirect back with success message
        return redirect()->route('supplier.edit', $supplier->id)->with('success', 'Profile updated successfully.');
    }
}
