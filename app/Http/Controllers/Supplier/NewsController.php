<?php

namespace App\Http\Controllers\Supplier;

use App\Http\Controllers\Controller;
use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NewsController extends Controller
{
    //


    public function create()
    {
        return view('frontend.supplier.add-news');
    }

    // public function store(Request $request)
    // {
    //     // dd($request->all());
    //     $request->validate([
    //         'title' => 'required|string|max:255',
    //         'description' => 'required|string',
    //     ]);

    //     News::create([
    //         'user_id' => Auth::id(),
    //         'title' => $request->input('title'),
    //         'description' => $request->input('description'),
    //     ]);

    //     return redirect()->route('supplier.brandnews')->with('success', 'News added successfully');
    // }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'title' => 'required|string|max:255',
            'description' => 'required|string',
            // 'media' => 'nullable|file|image|max:10240', // Max file size 10MB, adjust as needed
            'youtube_link' => 'nullable|url',
        ]);

        // Handle media upload or YouTube link
        if ($request->hasFile('media')) {
            // Handle uploaded file (image)
            $image = $request->file('media');
            $imageName = time() . '_' . $image->getClientOriginalName();
            $image->move(public_path('uploads/news'), $imageName);

            // Save to database
            $news = new News();
            $news->user_id = auth()->user()->id; // Assuming user is authenticated
            $news->title = $validatedData['title'];
            $news->description = $validatedData['description'];
            $news->image = '/uploads/news/' . $imageName; // Store the path to the image in database
            $news->save();

        } elseif (!empty($validatedData['youtube_link'])) {
            // Handle YouTube link
            $news = new News();
            $news->user_id = auth()->user()->id; // Assuming user is authenticated
            $news->title = $validatedData['title'];
            $news->description = $validatedData['description'];
            $news->youtube_link = $validatedData['youtube_link']; // Store the YouTube link in database
            $news->save();
        }

        return redirect()->route('supplier.brandnews')->with('success', 'News added successfully.');
    }
    public function editNews($id)
    {
        $news = News::findOrFail($id); 
        return view('frontend.supplier.edit-news', compact('news'));
    }

    public function updateNews(Request $request, $id)
    {
        $request->validate([
            'title' => 'required|string|max:255',
            'description' => 'required|string',
            // Add validation rules for other fields if needed
        ]);
        $news = News::findOrFail($id);
        $news->title = $request->title;
        $news->description = $request->description;
        // Update other fields as necessary
        $news->save();
        return redirect()->route('supplier.brandnews')->with('success', 'News updated successfully.');
    }


    public function deleteNews($id)
    {
        $news = News::findOrFail($id);
        $news->delete();

        return redirect()->route('supplier.brandnews')->with('success', 'News deleted successfully.');
    }
}
