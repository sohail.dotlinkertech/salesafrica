<?php

namespace App\Http\Controllers;

use App\Models\Distributor;
use App\Models\Quote;
use App\Models\QuoteReply;
use App\Models\Supplier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DistributorController extends Controller
{
    //

    public function dashboard()
    {
        return view('frontend.distributor.distributor-panel');
    }

    public function myAccountDistributor()
    {
        return view('frontend.distributor.my-account-distributor');
    }

    public function profileDistributor()
    {
        return view('frontend.distributor.distributor-profile');
    }

    public function viewDistributor()
    {
        return view('frontend.distributor.distributor-view');
    }

    public function transactionInvoicerDistributor()
    {
        return view('frontend.distributor.transaction-invoice-distributor');
    }

   

    public function myRfqSalesAgent()
    {
        $quotes = Quote::with(['user', 'replies', 'supplier', 'salesAgent'])
        ->whereHas('replies', function ($query) {
            $query->where('sender_id',Auth::id());
        }) ->get();

        return view('frontend.distributor.my-quotation')->with(compact('quotes'));
    }
    public function myQuotes()
    {
        $quotes = Quote::with('replies')->where('user_id', Auth::id())->get();
        // dd($quotes);
        return view('frontend.distributor.my-posts-distributor', compact('quotes'));
    }

    public function quotationDistributor()
    {
        return view('frontend.distributor.quotation');
    }

    public function quotation()
    {
        $quotes = Quote::with(['user', 'replies', 'supplier', 'salesAgent'])->whereHas('user', function ($query) {
                $query->where('role', '!=', 4);})->get();
    
        return view('frontend.distributor.quotation')->with(compact('quotes'));
    }
    
    public function shortlistedOpprtunity()
    {
        return view('frontend.distributor.shortlisted-opprtunity');
    }

    public function salesAgentPanelDistributor()
    {
        return view('frontend.distributor.sales-agent-panel');
    }

    public function subscription()
    {
        return view('frontend.distributor.subscription');
    }

}
