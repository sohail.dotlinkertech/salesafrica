<?php

namespace App\Http\Controllers\Distributor;

use App\Http\Controllers\Controller;
use App\Models\Distributor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{


    public function editDistributor()
    {
        // Get the authenticated user
        $user = Auth::user();

        // Get the supplier details associated with the user
        $distributor = Distributor::where('user_id', $user->id)->firstOrFail();

        return view('frontend.distributor.edit-distributor-profile', compact('distributor'));
    }


    public function update(Request $request, $id)
    {
        // dd($request->all());
        // Validate the incoming request data
        $request->validate([
            'company' => 'nullable|string|max:255',
            'location' => 'nullable|string|max:255',
            'country' => 'required|string|max:255',
            'business_category' => 'nullable|string|max:255',
            'company_website' => 'nullable|max:255',
            'inspection_report' => 'nullable|image|file|mimes:pdf,doc,docx,png,jpg,gif,svg|max:2048',
            'letter_of_authorization' => 'nullable|image|file|mimes:pdf,doc,docx,png,jpg,gif,svg|max:2048',
            // 'first_name' => 'nullable|string|max:255',
            // 'last_name' => 'nullable|string|max:255',
            // 'email' => 'nullable|email|max:255',
            // 'phone' => 'nullable|string|max:25',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $distributor = Distributor::findOrFail($id);

        // Retrieve the related user
        $user = $distributor->user;

        if ($request->hasFile('image')) {
            // Delete old image
            if ($distributor->image) {
                $oldImagePath = public_path('uploads' . $distributor->image);
                if (file_exists($oldImagePath)) {
                    unlink($oldImagePath);
                }
            }

            // Save new image
            $image = $request->file('image');
            $imageName = time() . '_' . $image->getClientOriginalName();
            $image->move(public_path('uploads'), $imageName);
            $distributor->image = $imageName;
        }

        // Update the distributor's data
        $distributor->update([
            'company' => $request->input('company'),
            'location' => $request->input('location'),
            'country' => $request->input('country'),
            'business_category' => $request->input('business_category'),
            'company_website' => $request->input('company_website'),
            // 'first_name' => $request->input('first_name'),
            // 'last_name' => $request->input('last_name'),
            // 'email' => $request->input('email'),
            // 'phone' => $request->input('phone'),
            'image' => $distributor->image ?? null,
        ]);

        $user->update([
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'email' => $request->input('email'),
            'phone' => $request->input('phone'),
            'image' => $distributor->image ?? null,

        ]);

        // Redirect back with success message
        return redirect()->route('distributor.edit', $distributor->id)->with('success', 'Profile updated successfully.');
    }
}
