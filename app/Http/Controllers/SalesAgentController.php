<?php

namespace App\Http\Controllers;

use App\Models\SalesAgent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SalesAgentController extends Controller
{
    public function dashboard()
    {
        return view('frontend.salesagent.sales-agent-panel');
    }

  
    public function myAccountSalesagent()
    {
        return view('frontend.salesagent.my-account-sales-agent'); 
    }

    public function profileSalesagent()
    {
        return view('frontend.salesagent.sales-agent-profile'); 
    }

    public function viewSalesagent()
    {
        return view('frontend.salesagent.sales-agent-view');
    }

    public function transactionInvoicerSalesagent()
    {
        return view('frontend.salesagent.transaction-invoice-sales-agent'); 
    }



    public function myRfqSalesAgent()
    {
        return view('frontend.salesagent.my-rfq-sales-agent'); 
    }

  
    public function recieviedRfqSalesAgent()
    {
        return view('frontend.salesagent.recievied-rfq-sales-agent'); 
    }

    public function shortlistedOpprtunity()
    {
        return view('frontend.salesagent.shortlisted-opprtunity'); 
    }

    public function salesagentMyPost()
    {
        return view('frontend.salesagent.my-posts-sales-agent'); 
    }

    public function salesAgentPanelSalesagent()
    {
        return view('frontend.salesagent.sales-agent-panel'); 
    }

    public function subscription()
    {
        return view('frontend.salesagent.subscription');
    }


}
