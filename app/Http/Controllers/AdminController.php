<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\Distributor;
use App\Models\SalesAgent;
use App\Models\Supplier;
use App\Models\User;
use App\Services\OTPService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Stringable;

class AdminController extends Controller
{

    public function dashboard()
    {
        $pageTitle = 'Dashboard';

        // Check if user is authenticated and has admin relationship
        if (Auth::user()->admin) {
            $userCountry = Auth::user()->admin->country;

            // Count total suppliers, distributors, and sales agents for the user's country
            $totalSuppliers = Supplier::where('country', $userCountry)->count();
            $totalDistributors = Distributor::where('country', $userCountry)->count();
            $totalSalesAgents = SalesAgent::where('country', $userCountry)->count();

            // Fetch distributors, sales agents, and suppliers where country matches user's country
            $distributors = Distributor::where('country', $userCountry)->get();
            $salesAgents = SalesAgent::where('country', $userCountry)->get();
            $suppliers = Supplier::where('country', $userCountry)->get();
        } else {
            // Set defaults or handle the case where admin relationship is null
            $totalSuppliers = 0;
            $totalDistributors = 0;
            $totalSalesAgents = 0;
            $distributors = collect();
            $salesAgents = collect();
            $suppliers = collect();
        }

        return view('admin.dashboard', compact('pageTitle', 'totalSuppliers', 'totalDistributors', 'totalSalesAgents', 'distributors', 'salesAgents', 'suppliers'));
    }



    public function edit()
    {
        $pageTitle = 'Profile';
        $user = Auth::user();
        return view('admin.profile', compact('user', 'pageTitle'));
    }


    public function update(Request $request, $id)
    {
        // Validate the incoming request
        $validated = $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|email',
            'phone' => 'required',
            'country' => 'required',
            'city' => 'required',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048'
        ]);

        // Find or create the associated User record
        $user = User::updateOrCreate(
            ['id' => $id],
            [
                // 'name' => $validated['name'],
                // 'email' => $validated['email'],
                // 'phone' => $validated['phone']
            ]
        );

        // Find or create the associated Admin record
        $admin = Admin::updateOrCreate(
            ['user_id' => $user->id],
            [
                'country' => $validated['country'],
                'city' => $validated['city']
            ]
        );

        // Handle image upload if provided
        if ($request->hasFile('image')) {
            // Delete old image if exists
            if ($admin->image && file_exists(public_path('uploads/' . $admin->image))) {
                unlink(public_path('uploads/' . $admin->image));
            }

            // Upload new image
            $image = $request->file('image');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('uploads'), $filename);
            $admin->image = $filename;
            $admin->save();
        }

        return redirect()->route('admin.profile.edit')->with('success', 'Profile updated successfully');
    }



    public function distributors(Request $request)
    {
        $query = Distributor::query();

        // Filter by country
        if ($request->has('country') && !empty($request->country)) {
            $query->where('country', $request->country);
        }

        // Filter by industries
        if ($request->has('industries') && !empty($request->industries)) {
            $query->whereJsonContains('industries', $request->industries);
        }
        // Filter by account type
        if ($request->has('account_type') && !empty($request->account_type)) {
            $query->where('account_type', $request->account_type);
        }

        // Search by company name
        if ($request->has('search') && !empty($request->search)) {
            $query->where('company', 'like', '%' . $request->search . '%');
        }

        $query->latest('created_at');
        // Retrieve filtered distributors with associated user data
        $distributors = $query->with('user')->get();

        // If the request is AJAX, return the filtered data
        if ($request->ajax()) {
            $view = view('admin.distributors.distributor_filtering', compact('distributors'))->render();
            return response()->json(['html' => $view]);
        }

        $pageTitle = 'Distributors';
        return view('admin.distributors.view', compact('pageTitle', 'distributors'));
    }


    public function add_distributors()
    {
        $pageTitle = 'Add | Distributors';
        return view('admin.distributors.add', compact('pageTitle'));
    }


    public function store_distributors(Request $request)
    {
        $request->validate([
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'phone' => 'required|string|max:25',
            'password' => 'required|string|min:8|confirmed',
            'role' => 'required',
        ]);

        // Role-specific validation rules
        $roleValidationRules = [
            '4' => [  // Distributor
                'company' => 'required|string|max:255',
                'location' => 'required|string|max:255',
                'country' => 'required|string|max:255',
                'business_category' => 'required|string|max:255',
                'company_website' => 'required|string|max:255',
            ],
        ];

        // Apply role-specific validation rules
        if (array_key_exists($request->role, $roleValidationRules)) {
            $request->validate($roleValidationRules[$request->role]);
        }
        $hashedPassword = Hash::make($request->password);

        $user = new User();
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->password = $hashedPassword;
        $user->role = $request->role;
        $user->save();


        // if ($request->hasFile('company_registration')) {
        //     $companyRegistrationPaper = $request->file('company_registration');
        //     $companyRegistrationPaperName = time() . '_' . $companyRegistrationPaper->getClientOriginalName();
        //     $companyRegistrationPaper->move('assets/uploads/company_registration_paper', $companyRegistrationPaperName);
        //     // $validated['company_registration_paper'] = 'assets/uploads/company_registration_paper/' . $companyRegistrationPaperName;
        //     $validated['company_registration_paper'] = $companyRegistrationPaperName;
        // }

        // if ($request->hasFile('shop_photos')) {
        //     $shopPhotos = $request->file('shop_photos');
        //     $shopPhotosName = time() . '_' . $shopPhotos->getClientOriginalName();
        //     $shopPhotos->move('assets/uploads/shop_photos', $shopPhotosName);
        //     // $validated['shop_photos'] = 'assets/uploads/shop_photos/' . $shopPhotosName;
        //     $validated['shop_photos'] = $shopPhotosName;
        // }

        // if ($request->hasFile('inspection_report')) {
        //     $inspectionReport = $request->file('inspection_report');
        //     $inspectionReportName = time() . '_' . $inspectionReport->getClientOriginalName();
        //     $inspectionReport->move('assets/uploads/inspection_reports', $inspectionReportName);
        //     // $validated['inspection_report'] = 'assets/uploads/inspection_reports/' . $inspectionReportName;
        //     $validated['inspection_report'] = $inspectionReportName;
        // }

        // if ($request->hasFile('authorization_letter')) {
        //     $authorizationLetter = $request->file('authorization_letter');
        //     $authorizationLetterName = time() . '_' . $authorizationLetter->getClientOriginalName();
        //     $authorizationLetter->move('assets/uploads/authorization_letters', $authorizationLetterName); 
        Distributor::create([
            'user_id' => $user->id,
            'distributor_type' => $request->input('distributor_type'),
            'company' => $request->input('company'),
            'location' => $request->input('location'),
            'country' => $request->input('country'),
            'business_category' => $request->input('business_category'),
            'company_website' => $request->input('company_website'),
        ]);


        return redirect()->route('admin.distributor')->with('success', 'Distributor created successfully.');
    }


    public function edit_distributors($id)
    {
        $pageTitle = 'Edit | Distributors';
        $distributor = Distributor::with('user')->where('user_id', $id)->first();
        return view('admin.distributors.edit', compact('pageTitle', 'distributor'));
    }


    public function update_distributors(Request $request, $id)
    {

        $request->validate([
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
        ]);


        // Find the distributor by ID
        $distributor = Distributor::findOrFail($id);
        $user = User::findOrFail($distributor->user_id);


        // Handle company registration paper upload if it exists
        if ($request->hasFile('company_registration_paper')) {
            // Delete old company registration paper
            if ($distributor->company_registration_paper) {
                $oldInspectionReportPath = public_path('uploads/company_registration_paper/' . $distributor->company_registration_paper);
                if (file_exists($oldInspectionReportPath)) {
                    unlink($oldInspectionReportPath);
                }
            }

            // Save new company registration paper
            $companyRegistrationPaper = $request->file('company_registration_paper');
            $companyRegistrationPaperName = time() . '_' . $companyRegistrationPaper->getClientOriginalName();
            $companyRegistrationPaper->move(public_path('uploads/company_registration_paper'), $companyRegistrationPaperName);
            $validated['company_registration_paper'] = $companyRegistrationPaperName;
        } else {
            // Retain the old company registration paper if no new file is uploaded
            $validated['company_registration_paper'] = $distributor->company_registration_paper;
        }

        // Handle shop photos upload if it exists
        if ($request->hasFile('shop_photos')) {
            // Delete old shop photos
            if ($distributor->shop_photos) {
                $oldShopPhotosPath = public_path('uploads/shop_photos/' . $distributor->shop_photos);
                if (file_exists($oldShopPhotosPath)) {
                    unlink($oldShopPhotosPath);
                }
            }

            // Save new shop photos
            $shopPhotos = $request->file('shop_photos');
            $shopPhotosName = time() . '_' . $shopPhotos->getClientOriginalName();
            $shopPhotos->move(public_path('uploads/shop_photos'), $shopPhotosName);
            $validated['shop_photos'] = $shopPhotosName;
        } else {
            // Retain the old shop photos if no new file is uploaded
            $validated['shop_photos'] = $distributor->shop_photos;
        }



        // Update the distributor
        $distributor->update([
            'distributor_type' => $request->input('distributor_type'),
            'company' => $request->input('company'),
            'location' => $request->input('location'),
            'country' => $request->input('country'),
            'business_category' => $request->input('business_category'),
            'company_website' => $request->input('company_website')
        ]);

        // Redirect back with success message
        return redirect()->route('admin.distributor')->with('success', 'Distributor Updated Successfully');
    }



    public function delete_distributors($id)
    {
        // Find the distributor and associated user
        $distributor = Distributor::findOrFail($id);
        $user = User::findOrFail($distributor->user_id);



        // Delete the distributor and user
        $distributorDeleted = $distributor->delete();
        $userDeleted = $user->delete();

        if ($distributorDeleted && $userDeleted) {
            return redirect()->back()->with('status', 'Distributor Deleted Successfully');
        } else {
            return redirect()->back()->with('status', 'Error Deleting Distributor');
        }
    }


    public function suppliers(Request $request)
    {
        $query = Supplier::query();

        // Filter by country
        if ($request->has('country') && !empty($request->country)) {
            $query->where('country', $request->country);
        }

        // Filter by industries
        if ($request->has('industries') && !empty($request->industries)) {
            $query->whereJsonContains('industries', $request->industries);
        }
        // Filter by account type
        if ($request->has('account_type') && !empty($request->account_type)) {
            $query->where('account_type', $request->account_type);
        }

        // Search by company name
        if ($request->has('search') && !empty($request->search)) {
            $query->where('company', 'like', '%' . $request->search . '%');
        }
        $query->latest('created_at');

        // Retrieve filtered distributors with associated user data
        $suppliers = $query->with('user')->get();

        // If the request is AJAX, return the filtered data
        if ($request->ajax()) {
            $view = view('admin.suppliers.suppliers_filtering', compact('suppliers'))->render();
            return response()->json(['html' => $view]);
        }

        $pageTitle = 'Suppliers';
        return view('admin.suppliers.view', compact('pageTitle', 'suppliers'));
    }



    public function add_suppliers()
    {
        $pageTitle = 'Add | Suppliers';
        return view('admin.suppliers.add', compact('pageTitle'));
    }

 
    public function store_suppliers(Request $request)
    {
        $request->validate([
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'phone' => 'required|string|max:25',
            'password' => 'required|string|min:8|confirmed',
            'role' => 'required',
        ]);

        // Role-specific validation rules
        $roleValidationRules = [
            '3' => [  // Supplier
                'company' => 'required|string|max:255',
                'location' => 'required|string|max:255',
                'country' => 'required|string|max:255',
                'business_category' => 'required|string|max:255',
                'company_website' => 'required|string|max:255',
            ],
        ];

        // Apply role-specific validation rules
        if (array_key_exists($request->role, $roleValidationRules)) {
            $request->validate($roleValidationRules[$request->role]);
        }
        $hashedPassword = Hash::make($request->password);

        $user = new User();
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->password = $hashedPassword;
        $user->role = $request->role;
        $user->save();

        // if ($request->hasFile('company_registration')) {
        //     $companyRegistrationPaper = $request->file('company_registration');
        //     $companyRegistrationPaperName = time() . '_' . $companyRegistrationPaper->getClientOriginalName();
        //     $companyRegistrationPaper->move('assets/uploads/company_registration_paper', $companyRegistrationPaperName);
        //     // $validated['company_registration_paper'] = 'assets/uploads/company_registration_paper/' . $companyRegistrationPaperName;
        //     $validated['company_registration_paper'] = $companyRegistrationPaperName;
        // }

        // if ($request->hasFile('shop_photos')) {
        //     $shopPhotos = $request->file('shop_photos');
        //     $shopPhotosName = time() . '_' . $shopPhotos->getClientOriginalName();
        //     $shopPhotos->move('assets/uploads/shop_photos', $shopPhotosName);
        //     // $validated['shop_photos'] = 'assets/uploads/shop_photos/' . $shopPhotosName;
        //     $validated['shop_photos'] = $shopPhotosName;
        // }

        // if ($request->hasFile('inspection_report')) {
        //     $inspectionReport = $request->file('inspection_report');
        //     $inspectionReportName = time() . '_' . $inspectionReport->getClientOriginalName();
        //     $inspectionReport->move('assets/uploads/inspection_reports', $inspectionReportName);
        //     // $validated['inspection_report'] = 'assets/uploads/inspection_reports/' . $inspectionReportName;
        //     $validated['inspection_report'] = $inspectionReportName;
        // }

        // if ($request->hasFile('authorization_letter')) {
        //     $authorizationLetter = $request->file('authorization_letter');
        //     $authorizationLetterName = time() . '_' . $authorizationLetter->getClientOriginalName();
        //     $authorizationLetter->move('assets/uploads/authorization_letters', $authorizationLetterName);
        //     // $validated['authorization_letter'] = 'assets/uploads/authorization_letters/' . $authorizationLetterName;
        //     $validated['authorization_letter'] = $authorizationLetterName;
        // }

        Supplier::create([
            'user_id' => $user->id,
            'company' => $request->input('company'),
            'location' => $request->input('location'),
            'country' => $request->input('country'),
            'business_category' => $request->input('business_category'),
            'company_website' => $request->input('company_website'),
        ]);

        return redirect()->route('admin.suppliers')->with('success', 'Supplier added successfully.');
    }


    public function edit_suppliers($id)
    {
        $supplier = Supplier::with('user')->where('user_id', $id)->first();

        $pageTitle = 'Edit | Suppliers';

        return view('admin.suppliers.edit', compact('supplier', 'pageTitle'));
    }


    public function update_suppliers(Request $request, $id)
    {
        $request->validate([
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'company' => 'required|string|max:255',
            'location' => 'required|string|max:255',
            'country' => 'required|string|max:255',
            'business_category' => 'required|string|max:255',
            'company_website' => 'required|string|max:255'
        ]);


        $supplier = Supplier::findOrFail($id);

        // Handle file uploads if they exist
        if ($request->hasFile('inspection_report')) {
            // Delete old inspection report
            if ($supplier->inspection_report) {
                $oldInspectionReportPath = public_path('uploads/inspection_reports/' . $supplier->inspection_report);
                if (file_exists($oldInspectionReportPath)) {
                    unlink($oldInspectionReportPath);
                }
            }

            // Save new inspection report
            $inspectionReport = $request->file('inspection_report');
            $inspectionReportName = time() . '_' . $inspectionReport->getClientOriginalName();
            $inspectionReport->move(public_path('uploads/inspection_reports'), $inspectionReportName);
        } else {
            // Retain the old inspection report if no new file is uploaded
            $inspectionReportName = $supplier->inspection_report;
        }

        if ($request->hasFile('letter_of_authorization')) {
            // Delete old letter of authorization
            if ($supplier->letter_of_authorization) {
                $oldLetterOfAuthorizationPath = public_path('uploads/authorization_letters/' . $supplier->letter_of_authorization);
                if (file_exists($oldLetterOfAuthorizationPath)) {
                    unlink($oldLetterOfAuthorizationPath);
                }
            }

            // Save new letter of authorization
            $letterOfAuthorization = $request->file('letter_of_authorization');
            $letterOfAuthorizationName = time() . '_' . $letterOfAuthorization->getClientOriginalName();
            $letterOfAuthorization->move(public_path('uploads/authorization_letters'), $letterOfAuthorizationName);
        } else {
            // Retain the old letter of authorization if no new file is uploaded
            $letterOfAuthorizationName = $supplier->letter_of_authorization;
        }

        // Update the supplier with validated data
        $supplier->update([
            'company' => $request->input('company'),
            'location' => $request->input('location'),
            'country' => $request->input('country'),
            'business_category' => $request->input('business_category'),
            'company_website' => $request->input('company_website'),
        ]);

        return redirect()->route('admin.suppliers')->with('success', 'Supplier updated successfully.');
    }


    public function destroy($id)
    {
        // Find the supplier and associated user
        $supplier = Supplier::findOrFail($id);
        $user = User::findOrFail($supplier->user_id);

        // Delete the supplier and user
        $supplierDeleted = $supplier->delete();
        $userDeleted = $user->delete();

        if ($supplierDeleted && $userDeleted) {
            return redirect()->route('admin.suppliers')->with('success', 'Supplier deleted successfully.');
        } else {
            return redirect()->back()->with('status', 'Error Deleting Supplier');
        }
    }




    public function salesAgent(Request $request)
    {
        $query = SalesAgent::query();

        // Filter by country
        if ($request->has('country') && !empty($request->country)) {
            $query->where('country', $request->country);
        }

        // Filter by industries
        if ($request->has('industries') && !empty($request->industries)) {
            $query->whereJsonContains('industries', $request->industries);
        }
        // Filter by account type
        if ($request->has('account_type') && !empty($request->account_type)) {
            $query->where('account_type', $request->account_type);
        }
        // Search functionality
        if ($request->has('search') && !empty($request->search)) {
            $search = $request->search;
            $query->where(function ($query) use ($search) {
                $query->where('company', 'like', '%' . $search . '%')
                    ->orWhere('first_name', 'like', '%' . $search . '%')
                    ->orWhere('last_name', 'like', '%' . $search . '%');
            });
        }

        $query->latest('created_at');

        // Retrieve filtered distributors with associated user data
        $salesAgents = $query->with('user')->get();

        // If the request is AJAX, return the filtered data
        if ($request->ajax()) {
            $view = view('admin.salesagent.salesAgent_filtering', compact('salesAgents'))->render();
            return response()->json(['html' => $view]);
        }

        $pageTitle = 'Sales Agent';
        return view('admin.salesagent.view', compact('pageTitle', 'salesAgents'));
    }




    public function add_salesAgent()
    {
        $pageTitle = 'Add | Sales Agent';
        return view('admin.salesagent.add', compact('pageTitle'));
    }

    public function store_salesAgent(Request $request)
    {
        $validated = $request->validate([
            'first_name' => 'nullable|string|max:255',
            'last_name' => 'nullable|string|max:255',
            'company' => 'nullable|string|max:255',
            'date_of_birth' => 'nullable|date',
            'phone' => 'required|string|max:25',
            'email' => 'required|email|max:255',
            'country' => 'required|string|max:255',
            'city' => 'required|string|max:255',
            'industries' => 'required|array',
            'introduction' => 'required|string',
        ]);


        // Verify phone OTP
        if (!OTPService::verifyOTP('phone_otp', $validated['phone_otp'])) {
            return redirect()->back()->withErrors(['phone_otp' => 'Invalid or expired OTP for phone.'])->withInput();
        }

        // Verify email OTP
        if (!OTPService::verifyOTP('email_otp', $validated['email_otp'])) {
            return redirect()->back()->withErrors(['email_otp' => 'Invalid or expired OTP for email.'])->withInput();
        }
        // Generate a random password and hash it
        $randomPassword = Str::random(10);
        $password = Hash::make($randomPassword);

        // Create a new user
        $user = User::create([
            'name' => $request->input('first_name') . ' ' . $request->input('last_name'),
            'email' => $validated['email'],
            'phone' => $validated['phone'],
            'password' => $password,
            'role' => 5,
        ]);

        $validated['user_id'] = $user->id;

        SalesAgent::create([
           
            'user_id' => $user->id,
            'first_name' => $validated['first_name'],
            'last_name' => $validated['last_name'],
            'company' => $validated['company'],
            'date_of_birth' => $validated['date_of_birth'],
            'country' => $validated['country'],
            'city' => $validated['city'],
            'industries' => $validated['industries'],
            'introduction' => $validated['introduction'],

        ]);

        return redirect()->route('admin.sales-agent')->with('success', 'Sales Agent created successfully.');
    }


    public function edit_salesAgent($id)
    {
        $pageTitle = 'Edit | Sales-agent';
        $salesAgent = SalesAgent::with('user')->where('user_id', $id)->first();

        return view('admin.salesagent.edit', compact('pageTitle', 'salesAgent'));
    }


    public function update_salesAgent(Request $request,  $id)
    {
        // dd($request->all());
        $validated = $request->validate([
            'first_name' => 'nullable|string|max:255',
            'last_name' => 'nullable|string|max:255',
            'company' => 'nullable|string|max:255',
            'date_of_birth' => 'nullable|date',
            'phone' => 'required|string|max:25',
            'email' => 'required|email|max:255',
            'country' => 'required|string|max:255',
            'city' => 'required|string|max:255',
            'industries' => 'required|array',
            'introduction' => 'required|string',
        ]);

        $salesAgent = SalesAgent::findOrFail($id);
        $user = User::findOrFail($salesAgent->user_id);
        // Update user email and phone if they have changed
        if ($user->email !== $validated['email'] || $user->phone !== $validated['phone']) {
            $user->update([
                'email' => $validated['email'],
                'phone' => $validated['phone'],
            ]);
        }

        $salesAgent->update([
            // 'user_id' => Auth::id(),
            'user_id' => $user->id,

            'first_name' => $validated['first_name'],
            'last_name' => $validated['last_name'],
            'company' => $validated['company'],
            'date_of_birth' => $validated['date_of_birth'],
            'country' => $validated['country'],
            'city' => $validated['city'],
            'industries' => $validated['industries'],
            'introduction' => $validated['introduction'],

        ]);

        return redirect()->route('admin.sales-agent')->with('success', 'Sales Agent updated successfully.');
    }

    public function destroySalesAgent($id)
    {


        // Find the salesAgent and associated user
        $salesAgent = SalesAgent::findOrFail($id);
        $user = User::findOrFail($salesAgent->user_id);

        // dd($salesAgent);
        // Delete the salesAgent and user
        $salesAgentDeleted = $salesAgent->delete();
        $userDeleted = $user->delete();

        if ($salesAgentDeleted && $userDeleted) {
            return redirect()->back()->with('success', 'SalesAgent Deleted successfully.');
        } else {
            return redirect()->back()->with('status', 'Error Deleting SalesAgent');
        }
    }

    public function news_approval()
    {
        $pageTitle = 'News Approval';
        return view('admin.news_approval.view', compact('pageTitle'));
    }



    public function approval_status(Request $request)
    {
        $input = $request->all();
        $id = $input['id'];

        $user = User::where('id', $id)->first();

        // print_r($user);
        if ($user) {
            if ($user->role == 2) {
                $admin = Admin::where('user_id', $id)->update(['status' => 1]);
                if ($admin) {
                    return response()->json(['status' => 'User has been Approved']);
                }
            } elseif ($user->role == 3) {
                $supplier = Supplier::where('user_id', $id)->update(['status' => 1]);
                if ($supplier) {
                    return response()->json(['status' => 'User has been Approved']);
                }
            } elseif ($user->role == 4) {
                $distributor = Distributor::where('user_id', $id)->update(['status' => 1]);
                if ($distributor) {
                    return response()->json(['status' => 'User has been Approved']);
                }
            } elseif ($user->role == 5) {
                $salesAgent = SalesAgent::where('user_id', $id)->update(['status' => 1]);
                if ($salesAgent) {
                    return response()->json(['status' => 'User has been Approved']);
                }
            } else {
                return response()->json(['error' => 'User role is not valid']);
            }
        } else {
            return response()->json(['error' => 'User not found']);
        }

        return response()->json(['error' => 'Something went wrong']);
    }
}
