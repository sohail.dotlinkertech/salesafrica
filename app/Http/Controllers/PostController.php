<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\Quote;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    //

    public function salesagentMyPost()
    {
        $posts = Post::where('user_id', Auth::id())->get();
        return view('frontend.salesagent.my-posts-sales-agent', compact('posts'));
    }

    public function create()
    {
        return view('frontend.salesagent.add-post-sales-agent');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'title' => 'required|string|max:255',
            'description' => 'required|string',
        ]);

        Post::create([
            'user_id' => Auth::id(),
            'title' => $request->title,
            'description' => $request->description,
        ]);

        return redirect()->route('salesagent.myPost')->with('success', 'Post added successfully.');
    }



}
