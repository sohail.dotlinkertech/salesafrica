<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EmailController extends Controller
{
    /**
     * Constructor to apply middleware for authentication.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Mark the user's email as verified.
     */
    public function verifyEmail(Request $request)
    {
        $request->user()->markEmailAsVerified();
        return redirect('/home'); // Redirect to wherever you need after verification
    }

    /**
     * Send another email verification notification.
     */
    public function resendVerificationEmail(Request $request)
    {
        $request->user()->sendEmailVerificationNotification();
        return back()->with('message', 'Verification link sent!');
    }
}
