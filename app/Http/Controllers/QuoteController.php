<?php

namespace App\Http\Controllers;


use App\Models\Post;
use App\Models\Quote;
use App\Models\QuoteReply;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class QuoteController extends Controller
{

    public function create()
    {
        return view('frontend.distributor.add-po-dstistributor');
    }


    
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'title' => 'required|string|max:255',
            'description' => 'required|string',
        ]);

        Quote::create([
            'user_id' => Auth::id(),
            'title' => $request->title,
            'description' => $request->description,
        ]);

        return redirect()->back()->with('success', 'Quote added successfully.');
    }



    public function quoteReply(Request $request, $id)
    {
        // Validate the request
        $request->validate([
            'title' => 'required',
            'reply_text' => 'required',
            'attachments' => 'nullable|file|mimes:jpg,jpeg,png,pdf,doc,docx|max:2048'
        ]);
        
      
        $attachments = null;
    
        // Handle file upload
        if ($request->hasFile('attachments')) {
            $file = $request->file('attachments');
            $attachments = time() . '_' . $file->getClientOriginalName();
            $file->move(public_path('uploads/attachments'), $attachments);
        }
    
        // Create a new QuoteReply
        QuoteReply::create([
            'quote_id' => $id,
            'title' => $request->title,
            'sender_id'=>Auth::id(),
            'reply_text' => $request->reply_text,
            'attachments' => $attachments
        ]);
    
        // Redirect back with success message
        return redirect()->back()->with('success', 'Successfully Replied.');
    }


    
}
