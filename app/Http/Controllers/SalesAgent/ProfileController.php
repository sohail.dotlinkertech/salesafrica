<?php

namespace App\Http\Controllers\SalesAgent;

use App\Http\Controllers\Controller;
use App\Models\SalesAgent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    public function editSalesagent()
    {
        $user = Auth::user();

        $salesagent = SalesAgent::where('user_id', $user->id)->firstOrFail();
        return view('frontend.salesagent.edit-sales-agent-profile',compact('salesagent')); 
    }

    public function update(Request $request, $id)
    {
        // dd($request->all());
        // Validate the incoming request data
        $request->validate([
            'company' => 'nullable|string|max:255',
            'location' => 'nullable|string|max:255',
            'country' => 'required|string|max:255',
            'business_category' => 'nullable|string|max:255',
            'company_website' => 'nullable|max:255',
            'inspection_report' => 'nullable|image|file|mimes:pdf,doc,docx,png,jpg,gif,svg|max:2048',
            'letter_of_authorization' => 'nullable|image|file|mimes:pdf,doc,docx,png,jpg,gif,svg|max:2048',
            // 'first_name' => 'nullable|string|max:255',
            // 'last_name' => 'nullable|string|max:255',
            // 'email' => 'nullable|email|max:255',
            // 'phone' => 'nullable|string|max:25',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
    
        
        $salesagent = SalesAgent::findOrFail($id);
        $user = $salesagent->user;

   
    
        if ($request->hasFile('image')) {
            // Delete old image
            if ($salesagent->image) {
                $oldImagePath = public_path('uploads' . $salesagent->image);
                if (file_exists($oldImagePath)) {
                    unlink($oldImagePath);
                }
            }
    
            // Save new image
            $image = $request->file('image');
            $imageName = time() . '_' . $image->getClientOriginalName();
            $image->move(public_path('uploads'), $imageName);
            $salesagent->image = $imageName;
        }
    
        // Update the salesagent's data
        $salesagent->update([
            'company' => $request->input('company'),
            'location' => $request->input('location'),
            'country' => $request->input('country'),
            'business_category' => $request->input('business_category'),
            'company_website' => $request->input('company_website'),
            // 'first_name' => $request->input('first_name'),
            // 'last_name' => $request->input('last_name'),
            // 'email' => $request->input('email'),
            // 'phone' => $request->input('phone'),
            'image' => $salesagent->image ?? null,
        ]);

        $user->update([
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'email' => $request->input('email'),
            'phone' => $request->input('phone'),
            'image' => $salesagent->image ?? null,

        ]);
    
        // Redirect back with success message
        return redirect()->route('salesagent.edit', $salesagent->id)->with('success', 'Profile updated successfully.');
    }
}
