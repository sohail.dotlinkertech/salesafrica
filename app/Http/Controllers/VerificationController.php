<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\EmailVerificationOTP;
use App\Services\OTPService;
class VerificationController extends Controller
{
    //

    public function sendPhoneOTP(Request $request)
    {
        $phone = $request->input('phone');
        $otp = OTPService::generateOTP();
        // Save OTP to session or database
        session(['phone_otp' => $otp]);
        // Send OTP to phone (use a service like Twilio)
        OTPService::sendOTPToPhone($phone, $otp);
        return response()->json(['message' => 'OTP sent to your phone.']);
    }

    public function sendEmailVerification(Request $request)
    {
        $email = $request->input('email');
        $otp = OTPService::generateOTP();
        // Save OTP to session or database
        session(['email_otp' => $otp]);
        // Send OTP to email
        Mail::to($email)->send(new EmailVerificationOTP($otp));
        return response()->json(['message' => 'Verification email sent.']);
    }

    public function verifyOTP(Request $request)
    {
        $inputOtp = $request->input('phone_otp');
        $sessionOtp = session('phone_otp');
        if ($inputOtp == $sessionOtp) {
            return response()->json(['message' => 'Phone number verified.']);
        }
        return response()->json(['message' => 'Invalid OTP.'], 422);
    }

    public function verifyEmailOTP(Request $request)
    {
        $inputOtp = $request->input('email_otp');
        $sessionOtp = session('email_otp');
        if ($inputOtp == $sessionOtp) {
            return response()->json(['message' => 'Email verified.']);
        }
        return response()->json(['message' => 'Invalid OTP.'], 422);
    }
}
