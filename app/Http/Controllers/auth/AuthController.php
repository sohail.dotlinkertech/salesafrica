<?php

namespace App\Http\Controllers\auth;

use App\Http\Controllers\Controller;
use App\Models\Distributor;
use App\Models\SalesAgent;
use App\Models\Supplier;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{

    public function register_view(Request $request)
    {
        return view('signup');
    }

    public function login_view()
    {
        return view('login');
    }


    public function adminLogin()
    {
        return view('auth.login');
    }
    public function adminRegister()
    {
        return view('auth.register');
    }


    public function signupPost(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'phone' => 'required|string|max:25',
            'password' => 'required|string|min:8|confirmed',
            'role' => 'required',
            'company' => 'nullable|string|max:255',
            'location' => 'nullable|string|max:255',
            'country' => 'nullable|string|max:255',
            'business_category' => 'nullable|string|max:255',
            'company_website' => 'nullable|url|max:255',
        ]);

        $hashedPassword = Hash::make($request->password);

        $user = new User();
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->password = $hashedPassword;
        $user->role = $request->role;
        $user->save();

        auth()->login($user);

        $individual = $request->has('individual') ? 1 : 0; // Check if the individual checkbox is selected

        if ($request->role == 4) {
            Distributor::create([
                'user_id' => $user->id,
                'company' => $request->input('company'),
                'location' => $request->input('location'),
                'country' => $request->input('country'),
                'business_category' => $request->input('business_category'),
                'company_website' => $request->input('company_website'),
                'individual' => $individual,
            ]);
        } elseif ($request->role == 3) {
            Supplier::create([
                'user_id' => $user->id,
                'company' => $request->input('company'),
                'location' => $request->input('location'),
                'country' => $request->input('country'),
                'business_category' => $request->input('business_category'),
                'company_website' => $request->input('company_website'),
                // 'individual' => $individual,
            ]);
        } elseif ($request->role == 5) {
            SalesAgent::create([
                'user_id' => $user->id,
                'company' => $request->input('company'),
                'location' => $request->input('location'),
                'country' => $request->input('country'),
                'business_category' => $request->input('business_category'),
                'company_website' => $request->input('company_website'),
                'individual' => $individual,
            ]);
        }

        return redirect()->route('login')->with('success', 'Account created successfully.');
    }


    public function loginPost(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ], [
            'email.required' => 'Please enter your Username.',
            'email.email' => 'Please enter a valid Username.',
            'password.required' => 'Please enter your password.',
        ]);

        $user = User::where('email', $request->email)->first();

        if (!$user) {
            return back()->withErrors([
                'email' => 'Username is not registered.',
            ]);
        }

        // If user exists, verify the password using Hash::check()
        if (Hash::check($request->password, $user->password)) {
            Auth::login($user, $request->has('remember'));
            return redirect($this->redirectDash());
        } else {
            return back()->withErrors([
                'password' => 'Password is incorrect.',
            ]);
        }
    }

    private function redirectDash()
    {
        if (Auth::user() && Auth::user()->role == 1) {
            return '/superadmin/dashboard';
        } elseif (Auth::user() && Auth::user()->role == 2) {
            return '/admin/dashboard';
        } elseif (Auth::user() && Auth::user()->role == 3) {
            return route('supplier.opportunites');
        } elseif (Auth::user() && Auth::user()->role == 4) {
            return route('distributor.dashboard');
        } else {
            return route('salesagent.dashboard');
        }
    }

    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }




    public function adminRegisterPost(Request $request)
    {
        // dd($request->all());
        // Validate the form data
        $request->validate([
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|string|min:8|confirmed',
            'role' => 'required',
        ]);

        // Create a new user instance
        $user = new User();
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->role = $request->role;

        // Save the user to the database
        $user->save();


        // Redirect to a success page or login page
        return redirect()->route('admin.login')->with('success', 'Registration successful! Please log in.');
    }
}
