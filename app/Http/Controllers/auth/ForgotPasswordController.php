<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Stringable;
use Illuminate\Support\Str;

class ForgotPasswordController extends Controller
{


    public function forgotPassword(Request $request)
    {
        // dd($request->all());
        $request->validate(['email' => 'required|email|exists:users']);

        $token = Str::random(64);


        // Check if token already exists for the email
        $passwordReset = DB::table('password_reset_tokens')->where('email', $request->email)->first();

       if ($passwordReset) {
            // Update existing token
            DB::table('password_reset_tokens')->where('email', $request->email)->update([
                'token' => $token,
                'created_at' => Carbon::now(),
            ]);
        } else {
            // Insert new token
            DB::table('password_reset_tokens')->insert([
                'email' => $request->email,
                'token' => $token,
                'created_at' => Carbon::now(),
            ]);
        }

        Mail::send('auth.forgot-password-mail', ['token' => $token], function ($message) use ($request) {
            $message->to($request->email);
            $message->subject('Password Reset');
        });


        return back()->with('success', 'A password reset link has been sent to your email');
    }

    public function resetPassword($token)
    {
        return view('auth.reset-password', compact('token'));
    }

    public function resetPasswordPost(Request $request)
    {
        $request->validate([
            'email' => 'required|email|exists:users',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required',
        ]);

        $updatePassword = DB::table('password_reset_tokens')
            ->where([
                'email' => $request->email,
                'token' => $request->token,
            ])
            ->first();

        if (!$updatePassword) {
            return back()->withInput()->withErrors(['email' => 'Invalid token!']);
        }

        User::where('email', $request->email)->update([
            'password' => Hash::make($request->password)
        ]);

        DB::table('password_reset_tokens')->where(['email' => $request->email])->delete();

        return redirect('/lohin')->with('success', 'Your password has been changed!');
    }
}
