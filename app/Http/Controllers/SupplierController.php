<?php

namespace App\Http\Controllers;

use App\Models\Distributor;
use App\Models\News;
use App\Models\SalesAgent;
use App\Models\Supplier;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SupplierController extends Controller
{
    


    public function opportunites()
    {
        return view('frontend.supplier.opportunites');
    }



    public function myAccountSupplier()
    {
        return view('frontend.supplier.my-account-supplier');
    }


    public function profileSupplier()
    {
        return view('frontend.supplier.supplier-profile');
    }


    public function viewSupplier()
    {
        return view('frontend.supplier.supplier-view');
    }

    public function transactionInvoicerSupplier()
    {
        return view('frontend.supplier.supplier-view');
    }

  

    public function myRfqSupplier()
    {
        return view('frontend.supplier.my-rfq-supplier');
    }

    public function quotationSupplier()
    {
        return view('frontend.supplier.quotation');
    }

    public function shortlistedDistributorSupplier()
    {
        // $distributors = User::where('role', 4)->get();
        $distributors = Distributor::with('user')->get();

        // Pass the fetched distributors to the view
        return view('frontend.supplier.shortlisted-distributor', compact('distributors'));
    }

    public function shortlistedSalesagentSupplier()
    {
        $salesagents = SalesAgent::with('user')->get();

        return view('frontend.supplier.shortlisted-sales-agents', compact('salesagents'));
    }

    public function brandnewsSupplier()
    {
        $news = News::all();
        return view('frontend.supplier.brand-news', compact('news'));
    }

    public function salesagentSupplier()
    {
        $salesagents = SalesAgent::with('user')->get();

        return view('frontend.supplier.sales-agent', compact('salesagents'));
    }

    public function viewSalesagent($id)
    {
        $salesagent = SalesAgent::with('user')->findOrFail($id); // Assuming you have a SalesAgent model
        return view('frontend.supplier.sales-agent-view', compact('salesagent'));
    }



    public function distributorSupplier()
    {
        $distributors = Distributor::with('user')->get();

        return view('frontend.supplier.distributor', compact('distributors'));
    }

    public function viewDistributor($id)
    {
        $distributor = Distributor::with('user')->findOrFail($id); // Assuming you have a SalesAgent model
        return view('frontend.supplier.distributor-view', compact('distributor'));
    }


    public function suppliersSupplier()
    {
        $suppliers = Supplier::with('user')->get();

        return view('frontend.supplier.supplier', compact('suppliers'));
    }

    public function showSupplier($id)
    {
        $supplier = Supplier::with('user')->findOrFail($id); // Assuming you have a SalesAgent model
        return view('frontend.supplier.supplier-view', compact('supplier'));
    }


    public function subscription()
    {
        return view('frontend.supplier.subscription');
    }
}
