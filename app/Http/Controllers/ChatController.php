<?php

namespace App\Http\Controllers;

use App\Models\Conversation;
use App\Models\Distributor;
use App\Models\Message;
use App\Models\Supplier;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ChatController extends Controller
{
    public function messageSupplier()
    {
        $supplierId = Auth::id(); // Get current supplier's user ID
        // $conversations = Conversation::where('sender_id', $supplierId)
        //     ->orWhere('receiver_id', $supplierId)
        //     ->with(['messages', 'sender', 'receiver'])
        //     ->get();

        $conversations = Conversation::where(function ($query) use ($supplierId) {
            $query->where('sender_id', $supplierId)
                ->orWhere('receiver_id', $supplierId);
        })
            ->with(['messages', 'sender', 'receiver'])
            ->get();

        // Check if $conversations is empty
        if ($conversations->isEmpty()) {
            // Handle the case where no conversations are found, perhaps redirect or show a message
            return redirect()->back()->with('error', 'No conversations found.');
        }

        return view('frontend.supplier.message', compact('conversations'));
    }

    public function messageDistributor()
    {
        $distributorId = Auth::id(); // Get the current distributor's user ID

        // Fetch conversations where the distributor is either the sender or receiver
        $conversations = Conversation::where(function ($query) use ($distributorId) {
            $query->where('sender_id', $distributorId)
                ->orWhere('receiver_id', $distributorId);
        })
            ->with(['messages', 'sender', 'receiver'])
            ->get();

        // Check if $conversations is empty
        if ($conversations->isEmpty()) {
            // Handle the case where no conversations are found, perhaps redirect or show a message
            return redirect()->back()->with('error', 'No conversations found.');
        }

        return view('frontend.distributor.message', compact('conversations'));
    }



    public function messagePage($userId)
    {
        // Fetch the user based on the $userId
        $user = User::with(['distributor', 'salesagent'])->findOrFail($userId);

        // Example logic to fetch conversations/messages (replace with your actual logic)
        $conversations = []; // Replace with actual logic to fetch conversations/messages

        return view('frontend.supplier.onetoone-message', [
            'user' => $user,
            'conversations' => $conversations,
        ]);
    }




    public function messageSalesAgent()
    {
        $salesAgentId = Auth::id(); // Get the current sales agent's user ID
        $conversations = Conversation::where('sender_id', $salesAgentId)
            ->orWhere('receiver_id', $salesAgentId)
            ->with(['messages', 'sender', 'receiver'])
            ->get();

        return view('frontend.salesagent.message', compact('conversations'));
    }



    // public function sendMessage(Request $request)
    // {

    //     $conversation = Conversation::firstOrCreate([
    //         'sender_id' => Auth::id(),
    //         'receiver_id' => $request->receiver_id
    //     ]);

    //     // Create a new message in the conversation
    //     $message = new Message();
    //     $message->conversation_id = $conversation->id;
    //     $message->sender_id = Auth::id();
    //     $message->receiver_id = $request->receiver_id;
    //     $message->message = $request->message;
    //     $message->is_image = false; // Set is_image to false for text messages

    //     $message->save();


    // }

    public function sendMessage(Request $request)
    {
        $senderId = Auth::id();
        $receiverId = $request->receiver_id;

        // Check if a conversation already exists between sender and receiver
        $conversation = Conversation::where(function ($query) use ($senderId, $receiverId) {
            $query->where('sender_id', $senderId)
                ->where('receiver_id', $receiverId);
        })->orWhere(function ($query) use ($senderId, $receiverId) {
            $query->where('sender_id', $receiverId)
                ->where('receiver_id', $senderId);
        })->first();

        // If conversation doesn't exist, create a new one
        if (!$conversation) {
            $conversation = Conversation::create([
                'sender_id' => $senderId,
                'receiver_id' => $receiverId,
            ]);
        }

        // Create a new message in the conversation
        $message = new Message();
        $message->conversation_id = $conversation->id;
        $message->sender_id = $senderId;
        $message->receiver_id = $receiverId;
        $message->message = $request->message;
        $message->is_image = false; // Set is_image to false for text messages

        $message->save();

        // You can return a response or perform any other logic as needed
    }



    public function fetchMessagesWithReceiver(Request $request)
    {
        $messages = Message::where(function ($query) use ($request) {
            $query->where('sender_id', Auth::id())
                ->where('receiver_id', $request->receiver_id);
        })->orWhere(function ($query) use ($request) {
            $query->where('sender_id', $request->receiver_id)
                ->where('receiver_id', Auth::id());
        })->get();

        return response()->json($messages);
    }
}
