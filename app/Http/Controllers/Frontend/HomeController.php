<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Distributor;
use App\Models\SalesAgent;
use App\Models\Supplier;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $salesagents = SalesAgent::all();

        $distributors = Distributor::all();
        $suppliers = Supplier::all();

        return view('landingpage.index', compact('distributors', 'salesagents', 'suppliers'));
    }

    public function contactUs()
    {
        return view('landingpage.contact-us');
    }


    public function fullDescription()
    {
        return view('landingpage.contact-us');
    }

    
    
    public function viewAllSuppliers(Request $request)
    {
        $query = Supplier::query();

        // Filtering logic (if any)
        if ($request->has('category')) {
            $query->where('business_category', $request->category);
        }

        // Pagination
        $suppliers = $query->paginate(10); // Adjust the number of items per page as needed

        return view('landingpage.viewall.suppliers-view-all', compact('suppliers'));
    } 
    
    public function viewAllSalesagents(Request $request)
    {
        $query = SalesAgent::query();

        // Filtering logic (if any)
        if ($request->has('category')) {
            $query->where('business_category', $request->category);
        }

        // Pagination
        $salesagents = $query->paginate(10); // Adjust the number of items per page as needed

        return view('landingpage.viewall.salesagents-view-all', compact('salesagents'));
    }
    public function viewAllDistributors(Request $request)
    {
        $query = Distributor::query();

        // Filtering logic (if any)
        if ($request->has('category')) {
            $query->where('business_category', $request->category);
        }

        // Pagination
        $distributors = $query->paginate(10); // Adjust the number of items per page as needed

        return view('landingpage.viewall.distributors-view-all', compact('distributors'));
    }


    public function blogSingle()
    {
        return view('landingpage.viewall.blog-single');
    }

    public function showSalesAgentHome($id)
    {
        $salesagent = SalesAgent::with('user')->findOrFail($id); // Assuming you have a SalesAgent model
        return view('landingpage.profile-details.sales-agent-view', compact('salesagent'));
    }




    public function viewDistributorHome($id)
    {
        $distributor = Distributor::with('user')->findOrFail($id); // Assuming you have a SalesAgent model
        return view('landingpage.profile-details.distributor-view', compact('distributor'));
    }



    public function showSupplierHome($id)
    {
        $supplier = Supplier::with('user')->findOrFail($id); // Assuming you have a SalesAgent model
        return view('landingpage.profile-details.supplier-view', compact('supplier'));
    }

}
