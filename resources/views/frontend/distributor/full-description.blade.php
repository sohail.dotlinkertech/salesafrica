<!DOCTYPE html>
<html lang="en">
<head>

<!-- meta tags -->
<meta charset="utf-8">
<meta name="keywords" content="bootstrap 5, premium, multipurpose, sass, scss, saas, eCommerce, Shop, Fashion" />
<meta name="description" content="Bootstrap 5 Landing Page Template" />
<meta name="author" content="www.themeht.com" />
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Title -->
<title>SalesAfrica</title>

<!-- Favicon Icon -->
<link rel="shortcut icon" href="assets/images/favicon.ico" />

<!-- inject css start -->

<link href="assets/css/theme-plugin.css" rel="stylesheet" />
<link href="assets/css/theme.min.css" rel="stylesheet" />
<link href="assets/css/custom.css" rel="stylesheet" />

<!-- inject css end -->

</head>

<body>

<!-- page wrapper start -->

<div class="page-wrapper">
  
<!-- preloader start -->

<div id="ht-preloader">
  <div class="loader clear-loader">
    <img class="img-fluid" src="assets/images/loader.gif" alt="">
  </div>
</div>

<!-- preloader end -->


<!--header start-->

<header class="site-header">
    <div id="header-wrap" class="shadow-sm py-md-5 py-3 fixed-top">
      <div class="container">
        <div class="row">
          <!--menu start-->
          <div class="col">
            <nav class="navbar navbar-expand-lg navbar-light">
              <!-- <a class="navbar-brand logo d-lg-none" href="index.html">
                <img class="img-fluid" src="assets/images/logo.png" alt="">
              </a> -->
              <a class="navbar-brand logo fs25 me-4" href="index.html">
                Sales<span class="text-primary">Africa</span>
              </a>
              <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav">
                  <li class="nav-item"> <a class="nav-link" href="index.html">Home</a>
                  </li>
                  <li class="nav-item"> <a class="nav-link" href="#">About</a>
                  </li>
                  <li class="nav-item"> <a class="nav-link" href="contact-us.html">Contact</a>
                  </li>
                </ul>
              </div>
              <div class="right-nav align-items-center d-flex justify-content-end"> 
                <span class="me-2">
                  <a href="">Join Now</a>
                </span>
              </div>
            </nav>
          </div>
          <!--menu end-->
        </div>
      </div>
    </div>
  </header>

<!--header end-->


<!--body content start-->

<div class="page-content">

<!--product details start-->

<section>
  <div class="container">
    <div class="row">
        <div class="col-lg-7">
           <div class="form-group">
             <strong><h4 for="">Job Description:</h4></strong>
             <p class="text-black">
              Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada
             </p>
           </div>
           <div class="form-group">
            <strong><h4 for="">Requirements:</h4></strong>
            <p class="text-black">
              Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada
             </p>
           </div>
           <div class="form-group">
             <strong><h4 for="">What we offer:</h4></strong>
            <p class="text-black">
              Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada
             </p>
           </div>
           <div class="form-group">
            <strong><h4 for="">Zone of interest:</h4></strong>
             <p class="text-black">
              Nigeria, South Africa, Zambia
             </p>
           </div>
           <div class="form-group">
            <label for="">Send message to the company:</label>
             <textarea class="form-control textarea"></textarea>
           </div>
           <div class="form-group">
            <label for="">Upload attachments:</label>
             <input type="file" class="form-control py-3">
           </div>
           <div class="form-group">
            <div class="remember-checkbox clearfix mb-5">
                <div class="">
                    <label class="form-check-label" for="customCheck1">You need to
                       <a href="login.html" class="line text-black me-0">login</a> to send message to the company.</label>
                     <div>
                       <a href="signup.html" class="line text-black">Create an account now</a>
                     </div>  
                </div>
              </div>
           </div>
           <div class="form-group">
            <div class="">
                <button class="btn btn-primary">Submit</button>
              </div>
           </div>
        </div>
      <div class="col-lg-5 col-12 mt-5 mt-lg-0">
        <div class="product-details">
          <div class="d-flex align-items-center">
            <div class="c-img-small me-3">
             <img src="assets/images/furniture-company.jpg" alt="">
            </div>
            <h3 class="mb-0">
              <a href="#" class="text-black">
                Xihnong Furntire Ltd
              </a>
            </h3>
         </div>
          <div>
            <div class="row">
              <div class="col-lg-6">
               <ul class="list-unstyled my-4">
                 <li>Location: <span class="text-muted"> china</span> 
                 </li>
                 <li>Staff Number :<span class="text-muted"> AB9999</span>
                 </li>
                 <li>Business Category :<span class="text-muted"> Furniture</span>
                 </li>
               </ul>
              </div>
              <div class="col-lg-6">
               <ul class="list-unstyled my-4">
                 <li>Established Year: <span class="text-muted"> 1999</span> 
                 </li>
                 <li>Staff Number :<span class="text-muted"> Unavailable</span>
                 </li>
                 <li>Sold in Africa :<span class="text-muted"> Nigeria, China, South Africa</span>
                 </li>
               </ul>
              </div>
            </div>
            <div class="text-end">
              <a href="company.html"><button class="btn btn-sm btn-primary">Visit supplier homepage</button></a>
            </div>
          </div>
          <div class="about mt-5">
             <h3>About Us</h3>
             <p class="mb-4">Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada</p>
          </div>
        </div>
        <div class="others">
            <div class="mb-5">
                <div class="d-flex mb-3 justify-content-center">
                  <button class="btn btn-primary me-2">Inspection Report</button>
                  <button class="btn btn-primary">Product Catalogue</button>
                </div>
            </div>
            <div class="video">
                <h5>Supplier Video</h5>
                <iframe class="w-100" height="315" src="https://www.youtube.com/embed/ABTdTTnnEU8?si=ttIOs4TlPcMtho5e" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
            </div>
            <div class=" brand-news shadow mt-3 p-5">
              <h4>Brand News</h4>
              <div class="row">
                <div class="col-8">
                  <p>1. Global Markets Rally Amid Economic</p>
                  <p>2. New Tech Innovations Unveiled at Major</p>
                  <p>3. Climate Change Summit Highlights</p>
                  <p>4. Breakthrough in Medical Research</p>
                  <p>5. Historic Peace Agreement Signed in Conflict Zone</p>
                </div>
                <div class="col-4 text-end">
                  <img src="assets/images/furniture-company.jpg" class="news-img" alt="">
                  <img src="assets/images/furniture-company.jpg" class="news-img" alt="">
                </div> 
              </div>
            </div>
            <!-- <div class="heart text-center">
                <span><i class="lar la-heart text-primary me-1"></i><a href="">Save as favourite</a></span>
            </div> -->
        </div>
      </div>
    </div>
  </div>
</section>

<!--product details end-->
</div>

<!--body content end--> 


<!--footer start-->

<footer class="py-11 bg-dark">
    <div class="container">
      <div class="row">
        <div class="col-12 col-lg-3"> <a class="footer-logo text-white h2 mb-0" href="index.html">
                Sales<span class="text-primary">Africa</span>
              </a>
          <p class="my-3 text-muted">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Asperiores, suscipit iusto! Sequi recusandae perspiciatis sit incidunt? Iure doloribus iste natus?.</p>
          <ul class="list-inline mb-0">
            <li class="list-inline-item"><a class="text-light ic-2x" href="#"><i class="la la-facebook"></i></a>
            </li>
            <li class="list-inline-item"><a class="text-light ic-2x" href="#"><i class="la la-dribbble"></i></a>
            </li>
            <li class="list-inline-item"><a class="text-light ic-2x" href="#"><i class="la la-instagram"></i></a>
            </li>
            <li class="list-inline-item"><a class="text-light ic-2x" href="#"><i class="la la-twitter"></i></a>
            </li>
            <li class="list-inline-item"><a class="text-light ic-2x" href="#"><i class="la la-linkedin"></i></a>
            </li>
          </ul>
        </div>
        <div class="col-12 col-lg-6 mt-6 mt-lg-0">
          <div class="row">
            <div class="col-12 col-sm-4 navbar-dark">
              <h5 class="mb-4 text-white">Quick Links</h5>
              <ul class="navbar-nav list-unstyled mb-0">
                <li class="mb-3 nav-item"><a class="nav-link" href="index.html">Home</a>
                </li>
                <li class="mb-3 nav-item"><a class="nav-link" href="about-us-1.html">About</a>
                </li>
                <li class="mb-3 nav-item"><a class="nav-link" href="faq.html">Faq</a>
                </li>
                <li class="nav-item"><a class="nav-link" href="contact-us.html">Contact Us</a>
                </li>
              </ul>
            </div>
            <div class="col-12 col-sm-4 mt-6 mt-sm-0 navbar-dark">
              <h5 class="mb-4 text-white">Top Products</h5>
              <ul class="navbar-nav list-unstyled mb-0">
                <li class="mb-3 nav-item"><a class="nav-link" href="#">T-Shirts</a>
                </li>
                <li class="mb-3 nav-item"><a class="nav-link" href="#">Sneakers & Athletic</a>
                </li>
                <li class="mb-3 nav-item"><a class="nav-link" href="#">Shirts & Tops</a>
                </li>
                <li class="mb-3 nav-item"><a class="nav-link" href="#">Sunglasses</a>
                </li>
                <li class="mb-3 nav-item"><a class="nav-link" href="#">Bags & Wallets</a>
                </li>
                <li class="mb-3 nav-item"><a class="nav-link" href="#">Accessories</a>
                </li>
                <li class="nav-item"><a class="nav-link" href="#">Shoes</a>
                </li>
              </ul>
            </div>
            <div class="col-12 col-sm-4 mt-6 mt-sm-0 navbar-dark">
              <h5 class="mb-4 text-white">Features</h5>
              <ul class="navbar-nav list-unstyled mb-0">
                <li class="mb-3 nav-item"><a class="nav-link" href="terms-and-conditions.html">Term Of Service</a>
                </li>
                <li class="mb-3 nav-item"><a class="nav-link" href="privacy-policy.html">Privacy Policy</a>
                </li>
                <li class="mb-3 nav-item"><a class="nav-link" href="#">Support</a>
                </li>
                <li class="mb-3 nav-item"><a class="nav-link" href="#">Shipping & Returns</a>
                </li>
                <li class="mb-3 nav-item"><a class="nav-link" href="#">Careers</a>
                </li>
                <li class="nav-item"><a class="nav-link" href="#">Our Story</a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-12 col-lg-3 mt-6 mt-lg-0">
          <div class="d-flex mb-3">
            <div class="me-2"> <i class="las la-map ic-2x text-primary"></i>
            </div>
            <div>
              <h6 class="mb-1 text-light">Store address</h6>
              <p class="mb-0 text-muted">423B, Road Wordwide Country, Africa</p>
            </div>
          </div>
          <div class="d-flex mb-3">
            <div class="me-2"> <i class="las la-envelope ic-2x text-primary"></i>
            </div>
            <div>
              <h6 class="mb-1 text-light">Email Us</h6>
              <a class="text-muted" href="mailto:themeht23@gmail.com"> themeht23@gmail.com</a>
            </div>
          </div>
          <div class="d-flex mb-3">
            <div class="me-2"> <i class="las la-mobile ic-2x text-primary"></i>
            </div>
            <div>
              <h6 class="mb-1 text-light">Phone Number</h6>
              <a class="text-muted" href="tel:+912345678900">+91-234-567-8900</a>
            </div>
          </div>
          <div class="d-flex">
            <div class="me-2"> <i class="las la-clock ic-2x text-primary"></i>
            </div>
            <div>
              <h6 class="mb-1 text-light">Working Hours</h6>
              <span class="text-muted">Mon - Fri: 10AM - 7PM</span>
            </div>
          </div>
        </div>
      </div>
      <hr class="my-8">
      <div class="row text-muted align-items-center">
        <div class="col-md-7">Copyright ©2024 All rights reserved</u>
        </div>
        <div class="col-md-5 text-md-end mt-3 mt-md-0">
          <ul class="list-inline mb-0">
            <li class="list-inline-item">
              <a href="#">
                <img class="img-fluid" src="assets/images/pay-icon/01.png" alt="">
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <img class="img-fluid" src="assets/images/pay-icon/02.png" alt="">
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <img class="img-fluid" src="assets/images/pay-icon/03.png" alt="">
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <img class="img-fluid" src="assets/images/pay-icon/04.png" alt="">
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </footer>

<!--footer end-->

</div>

<!-- page wrapper end -->

 
<!--back-to-top start-->

<div class="scroll-top"><a class="smoothscroll" href="#top"><i class="las la-angle-up"></i></a></div>

<!--back-to-top end-->

<!-- inject js start -->

<script src="assets/js/theme-plugin.js"></script>
<script src="assets/js/theme-script.js"></script>

<!-- inject js end -->

</body>
</html>
