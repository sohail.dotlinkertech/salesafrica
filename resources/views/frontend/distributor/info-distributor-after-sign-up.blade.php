<!DOCTYPE html>
<html lang="en">
<head>

<!-- meta tags -->
<meta charset="utf-8">
<meta name="keywords" content="bootstrap 5, premium, multipurpose, sass, scss, saas, eCommerce, Shop, Fashion" />
<meta name="description" content="Bootstrap 5 Landing Page Template" />
<meta name="author" content="www.themeht.com" />
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Title -->
<title>Ekocart - Multipurpose eCommerce HTML5 Template</title>

<!-- Favicon Icon -->
<link rel="shortcut icon" href="assets/images/favicon.ico" />

<!-- inject css start -->

<link href="assets/css/theme-plugin.css" rel="stylesheet" />
<link href="assets/css/theme.min.css" rel="stylesheet" />
<link href="assets/css/custom.css" rel="stylesheet" />

<!-- inject css end -->

</head>

<body>

<!-- page wrapper start -->

<div class="page-wrapper">

<!--body content start-->

<div class="page-content">

<!--login start-->

<section class="register">
  <div class="container">
     <div class="row">
      <div class="col-lg-8 col-md-12  mx-auto">
        <div class="mb-6">
          <p class="lead text-black">Congratulations, you have registered distributor account at Sales.Africa</p>
          <p class="lead text-black">Now, upload the documents and once they are approved, your account will be activated,
            then you can start communicating with suppliers.
            </p>
        </div>
        </div>
        </div>
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <div class="register-form">
          <form id="contact-form">
            <div class="row">
               <div class="col-12">
                 <div class="row align-items-center">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">Company Registration Paper</label>
                            <input type="file" class="form-control py-3">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <small class="mb-0 text-black">If you are an individual distributor, you don’t have to upload this.</small>
                    </div>
                    <div class="col-md-6" >
                        <div class="form-group">
                        <label for="">Photo of Shop</label>
                        <input type="file" class="form-control py-3">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <small class="mb-0 text-black">Please upload photos of your shop or office with your company logo, if you are running an online
                            shop, you can provide an image of your online storefront.</small>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                        <label for="">Business Card</label>
                        <input type="text" class="form-control py-3">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <small class="mb-0 text-black">This is optional. But it may help us approve your account earlier.</small>
                    </div>
                 </div>
               </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <a href="#" class="btn btn-primary">Save</a>
                <a href="#" class="btn btn-primary">Submit</a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>

<!--login end-->

</div>

<!--body content end--> 

</div>

<!-- page wrapper end -->

<!-- inject js start -->

<script src="assets/js/theme-plugin.js"></script>
<script src="assets/js/theme-script.js"></script>
<!-- inject js end -->

</body>
</html>
