@extends('frontend.layouts.distributorMain')

@section('title', 'Track order')

@section('main-content')
    <div class="col-lg-10 content-right">
        <div class="d-flex justify-content-between mb-3">
            <h4>Transatcions Invoice</h4>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <table class="table table-bordered">
                    <thead style="background-color: aliceblue;">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Purchase Date</th>
                            <th scope="col">Expiry Date</th>
                            <th scope="col">Amount</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">1</th>
                            <td>12-06-2024</td>
                            <td>31-12-2025</td>
                            <td>$899</td>
                        </tr>
                        <tr>
                            <th scope="row">1</th>
                            <td>12-06-2024</td>
                            <td>31-12-2025</td>
                            <td>$899</td>
                        </tr>
                        <tr>
                            <th scope="row">1</th>
                            <td>12-06-2024</td>
                            <td>31-12-2025</td>
                            <td>$899</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
