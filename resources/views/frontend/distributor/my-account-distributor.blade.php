@extends('frontend.layouts.distributorMain')

@section('title', 'Track order')

@section('main-content')
    <div class="col-lg-10 content-right">
        <div class="row  mb-3">
            <div class="col-12">
                <div class="d-flex justify-content-between align-items-center">
                    <div>
                        <h4 class="mb-0">My Account</h4>
                        <p>Change email and password</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12 mx-auto">
                <div class="register-form">
                    <form id="contact-form">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Email Verification Code</label>
                                    <input id="form_email" type="email" class="form-control"
                                        placeholder="Verification Code">
                                    <button type="button" class="btn btn-sm btn-secondary mt-2">Send Verification
                                        Code</button>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">New Password</label>
                                    <input type="password" class="form-control" placeholder="New Password">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Confirm New Password</label>
                                    <input type="password" class="form-control" placeholder= "Confirm New Password">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="#" class="btn btn-primary">Submit</a>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
