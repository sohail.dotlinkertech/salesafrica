@extends('frontend.layouts.distributorMain')

@section('title', 'Track order')

@section('main-content')
    <div class="col-lg-10 content-right">
        <div class="d-flex justify-content-between mb-4">
            <h3>My RFQ （Request for Quotation）</h3>
        </div>
        @foreach ($quotes as $item)
        <div class="quotation mb-3">
            <h4>{{ $item->title }}</h4>
           
            <div class="d-flex">
                <p class="text-black me-5"><strong>From:</strong> {{ $item->related_user->company }} </p>
                <div><img src="{{ url('assets/images/china-flag.jpg') }}" class="me-2" style="width: 25px;" alt="">Sales Agent</div>
               </div>
               <p class="text-black"><strong>Category:</strong>{{ $item->related_user->buisness_category }}</p>
            @foreach ($item->replies as $reply)
                <div class="mt-3">
                    <i
                        class="line"><small>{{ Auth::id() == $reply->sender_id ? 'You replied' : 'Response sent to you' }}</small></i>
                </div>
                <h5>{{ $reply->title }}</h5>
                <p class="mb-4 mt-2">{{ $reply->reply_text }}</p>
            @endforeach

            @if ($item->replies->isEmpty())
                <p class="mb-4 mt-2">No replies yet.</p>
            @endif
            <div class="text-end">
                <button class="btn btn-primary btn-sm" data-toggle="modal"
                    data-target="#respondModal{{ $item->id }}">Respond</button>
            </div>
        </div>

        <!-- The Modal -->
        <div class="modal fade" id="respondModal{{ $item->id }}" tabindex="-1" role="dialog"
            aria-labelledby="respondModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="respondModalLabel">Respond</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{ route('quote.reply', $item->id) }}" method="POST"
                            enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <label for="">Attachments</label>
                                    <input type="file" name="attachments" id="" class="form-control">
                                </div>
                                <div class="col-md-12 mt-5">
                                    <label for="">Title <span style="color: red">*</span></label>
                                    <input type="text" name="title" required value="{{ old('title') }}"
                                        class="form-control">
                                </div>
                                <div class="col-md-12 mt-5">
                                    <label for="">Message <span style="color: red">*</span></label>
                                    <textarea name="reply_text" required class="form-control">{{ old('reply_text') }}</textarea>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Reply</button>
                            </div>
                        </form>

                    </div>

                </div>
            </div>
        </div>
    @endforeach


    </div>

@endsection
