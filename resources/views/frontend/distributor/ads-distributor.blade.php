@extends('frontend.layouts.distributorMain')

@section('title', 'Track order')

@section('main-content')

    <div class="col-lg-10 content-right">
       <div class="d-flex justify-content-between align-items-center">
        <h4>Ads</h4>
        <a href="add-ads-distributor.html">
            <button class="btn btn-primary">Add Ads</button>
        </a>
       </div>
      <div class="row">
        <div class="col-12 my-4">
          <div class="position-relative rounded overflow-hidden">
            <!-- Background -->
            <img class="img-fluid hover-zoom ad-img" src="assets/images/product-ad/07.jpg" alt="">
            <!-- Body -->
            <div class="position-absolute top-50 translate-middle-y ps-5">
              <h6 class="text-dark">2024 Collection</h6>
              <!-- Heading -->
              <h3>New Stylish Trend<br>Running Shoe</h3>
              <!-- Link --> <a class="btn btn-sm btn-primary btn-animated" href="#">See who has viewed
                  </a>
            </div>
          </div>
        </div>
      </div>
    </div>
 @endsection