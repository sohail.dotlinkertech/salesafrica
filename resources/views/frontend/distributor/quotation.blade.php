@extends('frontend.layouts.distributorMain')

@section('title', 'Track order')

@section('main-content')
    <div class="col-lg-10 content-right">
        <div class="d-flex justify-content-between mb-4">
            <h3>RFQ Received （Request for Quotation）</h3>
        </div>
        @foreach ($quotes as $item)
        <div class="col-lg-8">
            <h4 class="mb-4">
              <label for="">New Opportunities</label>
            </h4>
            <div class="p-box mb-3">
              <div class="d-md-flex product-details">
                <div class="d-flex justify-content-center me-3">
                     <div class="c-img mb-3">
                         <img src="{{ url('assets/images/furniture-company.jpg') }}" alt="">
                         <div class="vip">
                             <img src="{{ url('assets/images/luxury-vip-badge.webp') }}" alt="">
                         </div>
                     </div>
                </div>
               <div class="d-flex">
                 <div class="me-4">
                    <h5 class="link-title mb-0">{{ $item->title }}</h5>
                    <p class="link-title mb-0">Opportunities: Sales agent, full-time markter, distributor</p>
                    <p class="link-title mb-0">Business Category: Furniture, Building Material</p>
                    <p class="link-title mb-0">Products: Sofa wooden, Bed, Wardrobe, Dining table</p>
                    <p class="link-title mb-0">Zone of interest: Nigeria, Ghana, south africa</p>
                    <div class="mt-3">
                      <h6 class="link-title mb-0">
                        <a href="" class="text-black">Supplier: {{ $item->related_user->company }}</a>
                      </h6>
                       <p class="link-title mb-0">Location: <img src="{{ url('assets/images/china-flag.jpg') }}" style="width: 30px;" alt=""> {{ $item->related_user->country }}</p>
                    </div>
                 </div>
               </div>
             </div>
              <div class="d-flex justify-content-end align-items-center">
                  <span data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="Add to shortlist">
                    <i class="lar la-heart text-primary me-1 cursor" style="font-size: 27px;"></i>
                  </span>
                 <a href="{{  }}" class="me-2">
                  <button class="btn btn-sm btn-primary">
                    View Full description
                   </button>
                 </a>
                 <a href="#">
                  <button class="btn btn-sm btn-primary">
                    Apply
                   </button>
                 </a>
              </div>
            </div>
         
    
            <div class="text-center">
              <a href="#">Check More </a>
            </div>
          </div>
        @endforeach


    </div>
@endsection
