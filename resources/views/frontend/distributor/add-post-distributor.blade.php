@extends('frontend.layouts.distributorMain')

@section('title', 'Distributor- Create Posts')

@section('main-content')
    <div class="col-lg-10 content-right">
        <div class="row  mb-3">
            <div class="col-12">
                <div class="d-flex justify-content-between align-items-center">
                    <div>
                        <h4 class="mb-0">Add post</h4>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12 mx-auto">
                <div class="register-form">
                    <form action="{{ route('distributor.posts.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">Title</label>
                                    <input type="text" name="title" value="{{ old('title') }}" class="form-control" placeholder="Title">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="">Description</label>
                                    <textarea name="description" id="" class="form-control textarea">{{ old('description') }}</textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                             
                                 <button type="submit" class="btn btn-primary" style="background-color:rgb(236, 51, 51)">Submit</button>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
