@extends('frontend.layouts.distributorMain')

@section('title', 'Distributor - My Posts')

@section('main-content')

    <style>
        .post-item {
            /* Example CSS for post item container */
        }

        .post-item p {
            /* Example CSS for paragraphs inside post item */
            word-wrap: break-word;
            /* Ensure text wraps if it exceeds container width */
            /* Additional styles as needed */
        }
    </style>
    <div class="col-lg-10 content-right">
        <div class="d-flex justify-content-between">
            <h4 class="mb-4">
                <label for="">My Posts</label>
            </h4>
            <div>
                <a href="{{ route('distributor.posts.create') }}"><button class="btn btn-primary">Add post</button></a>
            </div>
        </div>

        @foreach ($quotes as $item)
            <div class="comp-response mt-5">

                <h4>{{ $item->title }}</h4>

                @foreach ($item->replies as $reply)
                    <i class="line"> <small>{{ $reply->sender_id == Auth::id() ? "Your Response" : 'Sent you this reponse' }}</small></i>
                    <h6>{{ $reply->title }}</h6>

                    <p class="mb-4">{{ $reply->reply_text }}</p>
                @endforeach

                @if ($item->replies->isEmpty())
                    No Reponse Found
                @endif
                <div class="text-end">
                    <button class="btn btn-primary btn-sm" data-toggle="modal"
                        data-target="#respondModal{{ $item->id }}">Respond</button>
                </div>
            </div>
            <!-- The Modal -->
            <div class="modal fade" id="respondModal{{ $item->id }}" tabindex="-1" role="dialog"
                aria-labelledby="respondModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="respondModalLabel">Respond</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form action="{{ route('quote.reply', $item->id) }}" method="POST"
                                enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="">Attachments</label>
                                        <input type="file" name="attachments" id="" class="form-control">
                                    </div>
                                    <div class="col-md-12 mt-5">
                                        <label for="">Title <span style="color: red">*</span></label>
                                        <input type="text" name="title" required value="{{ old('title') }}"
                                            class="form-control">
                                    </div>
                                    <div class="col-md-12 mt-5">
                                        <label for="">Message <span style="color: red">*</span></label>
                                        <textarea name="reply_text" required class="form-control">{{ old('reply_text') }}</textarea>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary">Reply</button>
                                </div>
                            </form>

                        </div>

                    </div>
                </div>
            </div>
        @endforeach


    </div>

@endsection
