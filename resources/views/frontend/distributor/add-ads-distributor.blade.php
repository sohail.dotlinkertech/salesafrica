@extends('frontend.layouts.distributorMain')

@section('title', 'Track order')

@section('main-content')
    <div class="col-lg-7 content-right">
        <div class="mb-4">
            <h4>Add Ad</h4>
        </div>
        <div class="form-group">
            <label for="">Image:</label>
            <input type="file" name="name" class="form-control py-3">
        </div>
        <div class="form-group">
            <label for="">Ad Title:</label>
            <input type="text" name="name" class="form-control" placeholder="Office name">
        </div>
        <div class="form-group">
            <label for="">Ad Description:</label>
            <textarea class="form-control textarea"></textarea>
        </div>
        <div class="form-group">
            <div class="">
                <button class="btn btn-primary">Submit</button>
            </div>
        </div>
    </div>
@endsection
