@extends('frontend.layouts.distributorMain')

@section('title', 'Track order')

@section('main-content')
<div class="col-lg-10 content-right">
    <h4>Chat with Distributors</h4>
    <div id="frame">
        <div id="sidepanel">
            <div id="profile">
                <div class="wrap">
                    <div class="d-flex">
                        <div>
                            <img id="profile-img" src="http://emilcarlsson.se/assets/mikeross.png" class="online" alt="" />
                        </div>
                        <div>
                            <p class="mb-0 float-none">Mike Ross</p>
                            <p class="mb-0 float-none">Online</p>
                        </div>
                    </div>
                </div>
            </div>
            <div id="search">
                <label for=""><i class="fa fa-search" aria-hidden="true"></i></label>
                <input type="text" placeholder="Search contacts..." />
            </div>
            <div id="contacts">
                <ul id="user-list">
                    @foreach($distributors as $distributor)
                        <li class="contact" data-user-id="{{ $distributor->user_id }}">
                            <div class="wrap">
                                <span class="contact-status online"></span>
                                <img src="{{ $distributor->image }}" alt="" />
                                <div class="meta">
                                    <p class="name">{{ $distributor->user->first_name }}</p>
                                    <p class="preview">{{ Str::limit($distributor->introduction, 30) }}</p>
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="content">
            <div class="contact-profile">
                <img src="http://emilcarlsson.se/assets/harveyspecter.png" alt="" />
                <p class="mb-0 float-none">Harvey Specter</p>
                <p class="mb-0 float-none">Online</p>
            </div>
            <div class="messages chat-body" id="chat-body">
                <ul>
                    {{-- Messages will be dynamically loaded here --}}
                </ul>
            </div>
            <div class="message-input">
                <div class="wrap">
                    <input type="text" class="form-control" id="message-input" placeholder="Write your message...">
                    <i class="fa fa-paperclip attachment" aria-hidden="true"></i>
                    <button class="submit" id="send-button"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/@joeattardi/emoji-button@3.0.3/dist/index.min.js"></script>
<script src="https://code.jquery.com/jquery-3.7.1.min.js"></script>
<script>
    $(document).ready(function() {
        const sendMessage = () => {
            const message = $('#message-input').val();
            const receiverId = $('#send-button').data('receiver-id');
            if (message.trim() !== '') {
                $.post('{{ route('chat.sendMessage') }}', {
                    message: message,
                    receiver_id: receiverId,
                    _token: '{{ csrf_token() }}'
                }, function(data) {
                    $('#message-input').val('');
                    fetchMessagesWithReceiver(receiverId);
                }).fail(function(xhr, status, error) {
                    console.log("Error: " + xhr.responseText);
                });
            }
        };

        $('#send-button').click(sendMessage);
        $('#message-input').keypress(function(e) {
            if (e.which == 13) {
                sendMessage();
            }
        });

        const fetchMessagesWithReceiver = (receiverId) => {
            $.get('{{ route('chat.fetchMessagesWithReceiver') }}', { receiver_id: receiverId }, function(data) {
                $('#chat-body ul').empty();
                data.forEach(message => {
                    const msgHtml = `
                        <li class="${message.sender_id === {{ Auth::id() }} ? 'sent' : 'replies'}">
                            <img src="${message.sender_id === {{ Auth::id() }} ? message.sender_profile_url : message.receiver_profile_url}" alt="" />
                            <p>${message.message}</p>
                        </li>`;
                    $('#chat-body ul').append(msgHtml);
                });
                $('#chat-body').scrollTop($('#chat-body')[0].scrollHeight);
            });
        };

        $('#user-list').on('click', '.contact', function() {
            const receiverId = $(this).data('user-id');
            fetchMessagesWithReceiver(receiverId);
            $('#send-button').data('receiver-id', receiverId);
        });

        const initialReceiverId = {{ $distributors->first()->user_id ?? 'null' }}; // Replace with the actual receiver ID
        if (initialReceiverId) {
            fetchMessagesWithReceiver(initialReceiverId);
            $('#send-button').data('receiver-id', initialReceiverId);
        }
    });
</script>
@endsection
