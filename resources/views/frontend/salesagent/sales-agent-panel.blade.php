@extends('frontend.layouts.salesagentMain')

@section('title', 'Track order')

@section('main-content')
    <div class="col-lg-10 mt-5 mt-lg-0 content-right">
        <div class="d-flex justify-content-between mb-4">
            <h3>Response Received</h3>
        </div>
        <div class="comp-response mt-5">
            <h4>Chinese Office furniture manufacturer seeking distributor in africa</h4>
            <i> <small>Sent you this reponse</small></i>
            <p class="mb-4">Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec eleifend
                Pellentesque eu quam sem, ac malesuada Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula
                suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada Nulla eget sem vitae eros pharetra
                viverra Nam vitae luctus ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada Nulla
                eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec eleifend Pellentesque eu
                quam sem, ac malesuada Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec
                eleifend Pellentesque eu quam sem, ac malesuada</p>
        </div>
    </div>
@endsection
