
@extends('frontend.layouts.salesagentMain')

@section('title', 'Track order')

@section('main-content')
  
  <div class="col-lg-10 content-right">
          <div class="d-flex justify-content-between">
            <h4 class="mb-4">
                <label for="">Shortlisted Opportunities</label>
              </h4>
          </div>
          <div class="p-box mb-3">
            <div class="d-md-flex product-details">
              <div class="d-flex justify-content-center me-3">
                   <div class="c-img mb-3">
                       <img src="assets/images/furniture-company.jpg" alt="">
                       <div class="vip">
                           <img src="assets/images/luxury-vip-badge.webp" alt="">
                       </div>
                   </div>
              </div>
             <div class="d-flex">
               <div class="me-4">
                  <h5 class="link-title mb-0">Chinese Office furniture manufacturer seeking distributor in africa</h5>
                  <p class="link-title mb-0">Opportunities: Sales agent, full-time markter, distributor</p>
                  <p class="link-title mb-0">Business Category: Furniture, Building Material</p>
                  <p class="link-title mb-0">Products: Sofa wooden, Bed, Wardrobe, Dining table</p>
                  <p class="link-title mb-0">Zone of interest: Nigeria, Ghana, south africa</p>
                  <div class="mt-3">
                    <h6 class="link-title mb-0">
                      <a href="company.html" class="text-black">Supplier: Shandong xaion xinhua office furniture Co., Ltd.</a>
                    </h6>
                     <p class="link-title mb-0">Location: <img src="assets/images/china-flag.jpg" style="width: 30px;" alt=""> China</p>
                  </div>
               </div>
             </div>
           </div>
            <!-- <div class="d-flex justify-content-end align-items-center">
               <a href="#">
                <button class="btn btn-sm btn-primary">
                  Edit opportunity
                 </button>
               </a>
            </div> -->
          </div>
          <div class="p-box mb-3">
            <div class="d-md-flex product-details">
              <div class="d-flex justify-content-center me-3">
                   <div class="c-img mb-3">
                       <img src="assets/images/furniture-company.jpg" alt="">
                       <div class="vip">
                           <img src="assets/images/luxury-vip-badge.webp" alt="">
                       </div>
                   </div>
              </div>
             <div class="d-flex">
               <div class="me-4">
                  <h5 class="link-title mb-0">Chinese Office furniture manufacturer seeking distributor in africa</h5>
                  <p class="link-title mb-0">Opportunities: Sales agent, full-time markter, distributor</p>
                  <p class="link-title mb-0">Business Category: Furniture, Building Material</p>
                  <p class="link-title mb-0">Products: Sofa wooden, Bed, Wardrobe, Dining table</p>
                  <p class="link-title mb-0">Zone of interest: Nigeria, Ghana, south africa</p>
                  <div class="mt-3">
                    <h6 class="link-title mb-0">
                      <a href="company.html" class="text-black">Supplier: Shandong xaion xinhua office furniture Co., Ltd.</a>
                    </h6>
                     <p class="link-title mb-0">Location: <img src="assets/images/china-flag.jpg" style="width: 30px;" alt=""> China</p>
                  </div>
               </div>
             </div>
           </div>
           <!-- <div class="d-flex justify-content-end align-items-center">
            <a href="#">
             <button class="btn btn-sm btn-primary">
               Edit opportunity
              </button>
            </a>
           </div> -->
          </div>
  
          <!-- <div class="text-center">
            <a href="#">Check More </a>
          </div> -->
        </div>
   @endsection