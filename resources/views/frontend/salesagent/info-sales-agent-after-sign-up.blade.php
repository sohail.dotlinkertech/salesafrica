<!DOCTYPE html>
<html lang="en">
<head>

<!-- meta tags -->
<meta charset="utf-8">
<meta name="keywords" content="bootstrap 5, premium, multipurpose, sass, scss, saas, eCommerce, Shop, Fashion" />
<meta name="description" content="Bootstrap 5 Landing Page Template" />
<meta name="author" content="www.themeht.com" />
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Title -->
<title>Ekocart - Multipurpose eCommerce HTML5 Template</title>

<!-- Favicon Icon -->
<link rel="shortcut icon" href="assets/images/favicon.ico" />

<!-- inject css start -->

<link href="assets/css/theme-plugin.css" rel="stylesheet" />
<link href="assets/css/theme.min.css" rel="stylesheet" />
<link href="assets/css/custom.css" rel="stylesheet" />

<!-- inject css end -->

</head>

<body>

<!-- page wrapper start -->

<div class="page-wrapper">

<!--body content start-->

<div class="page-content">

<!--login start-->

<section class="register">
  <div class="container">
     <div class="row">
      <div class="col-lg-8 col-md-12  mx-auto">
        <div class="mb-6">
          <p class="lead text-black">Congratulations, you have registered sales agent account at Sales.Africa.</p>
          <p class="lead text-black">Now, you can start communication with suppliers that are looking for sales agents in Africa.</p>
        </div>
        </div>
        </div>
  </div>
</section>

<!--login end-->

</div>

<!--body content end--> 

</div>

<!-- page wrapper end -->

<!-- inject js start -->

<script src="assets/js/theme-plugin.js"></script>
<script src="assets/js/theme-script.js"></script>
<!-- inject js end -->

</body>
</html>
