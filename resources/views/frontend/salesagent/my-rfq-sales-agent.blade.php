@extends('frontend.layouts.salesagentMain')

@section('title', 'Track order')

@section('main-content')
    <div class="col-lg-10 content-right">
        <div class="d-flex justify-content-between mb-4">
            <h3>My RFQ （Request for Quotation）</h3>
        </div>
        <div class="quotation mb-3">
            <h4>I need quotes on a 40ft container of ceiling lights to Nigeria</h4>
            <div class="d-flex">
                <div><img src="assets/images/china-flag.jpg" class="me-2" style="width: 25px;" alt="">Sales Agent
                </div>
            </div>
            <p class="text-black"><strong>Category:</strong> Building materia</p>
            <div>
                <div class="mt-3">
                    <i class="line"><small>You replied</small></i>
                </div>
                <h5>I have recieved your Response it was good</h5>
                <p class="mb-4 mt-2">Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec
                    eleifend Pellentesque eu quam sem, ac malesuada Nulla eget sem vitae eros pharetra viverra Nam vitae
                    luctus ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada</p>
            </div>
            <div>
                <div class="mt-3 d-flex justify-content-between">
                    <i class="line"><small>You got response</small></i>
                    <small>12-8-2024 12PM</small>
                </div>
                <h5>I have recieved your Response it was good</h5>
                <p class="mb-4 mt-2">Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec
                    eleifend Pellentesque eu quam sem, ac malesuada Nulla eget sem vitae eros pharetra viverra Nam vitae
                    luctus ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada</p>
            </div>
        </div>
        <div class="quotation mb-3">
            <h4>I need quotes on a 40ft container of ceiling lights to Nigeria</h4>
            <div class="d-flex">
                <div><img src="assets/images/china-flag.jpg" class="me-2" style="width: 25px;" alt="">Sales Agent
                </div>
            </div>
            <p class="text-black"><strong>Category:</strong> Building materia</p>
            <div>
                <div class="mt-3">
                    <i class="line"><small>You replied</small></i>
                </div>
                <h5>I have recieved your Response it was good</h5>
                <p class="mb-4 mt-2">Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec
                    eleifend Pellentesque eu quam sem, ac malesuada Nulla eget sem vitae eros pharetra viverra Nam vitae
                    luctus ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada</p>
            </div>
            <div>
                <div class="mt-3 d-flex justify-content-between">
                    <i class="line"><small>You got response</small></i>
                    <small>12-8-2024 12PM</small>
                </div>
                <h5>I have recieved your Response it was good</h5>
                <p class="mb-4 mt-2">Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec
                    eleifend Pellentesque eu quam sem, ac malesuada Nulla eget sem vitae eros pharetra viverra Nam vitae
                    luctus ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada</p>
            </div>
        </div>
        <div class="quotation mb-3">
            <h4>I need quotes on a 40ft container of ceiling lights to Nigeria</h4>
            <div class="d-flex">
                <div><img src="assets/images/china-flag.jpg" class="me-2" style="width: 25px;" alt="">Sales Agent
                </div>
            </div>
            <p class="text-black"><strong>Category:</strong> Building materia</p>
            <div>
                <div class="mt-3">
                    <i class="line"><small>You replied</small></i>
                </div>
                <h5>I have recieved your Response it was good</h5>
                <p class="mb-4 mt-2">Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec
                    eleifend Pellentesque eu quam sem, ac malesuada Nulla eget sem vitae eros pharetra viverra Nam vitae
                    luctus ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada</p>
            </div>
            <div>
                <div class="mt-3 d-flex justify-content-between">
                    <i class="line"><small>You got response</small></i>
                    <small>12-8-2024 12PM</small>
                </div>
                <h5>I have recieved your Response it was good</h5>
                <p class="mb-4 mt-2">Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec
                    eleifend Pellentesque eu quam sem, ac malesuada Nulla eget sem vitae eros pharetra viverra Nam vitae
                    luctus ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada</p>
            </div>
        </div>
    </div>

@endsection
