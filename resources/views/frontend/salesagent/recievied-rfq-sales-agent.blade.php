@extends('frontend.layouts.salesagentMain')

@section('title', 'Track order')

@section('main-content')
    <div class="col-lg-10 content-right">
        <div class="d-flex justify-content-between mb-4">
            <h3>RFQ Received （Request for Quotation）</h3>
        </div>
        <div class="quotation mb-3">
            <h4>I need quotes on a 40ft container of ceiling lights to Nigeria</h4>
            <div class="d-flex">
                <div><img src="assets/images/china-flag.jpg" class="me-2" style="width: 25px;" alt="">Sales Agent
                </div>
            </div>
            <p class="text-black"><strong>Category:</strong> Building materia</p>
            <div class="mt-3">
                <i class="line"><small>Response sent to you</small></i>
            </div>
            <h5>I have recieved your Response it was good</h5>
            <p class="mb-4 mt-2">Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec
                eleifend Pellentesque eu quam sem, ac malesuada Nulla eget sem vitae eros pharetra viverra Nam vitae luctus
                ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada Nulla eget sem vitae eros pharetra
                viverra Nam vitae luctus ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada Nulla
                eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec eleifend Pellentesque eu
                quam sem, ac malesuada Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec
                eleifend Pellentesque eu quam sem, ac malesuada</p>
            <div class="text-end">
                <a href="quote-respond.html"> <button class="btn btn-primary btn-sm">Respond</button></a>
            </div>
        </div>
        <div class="quotation mb-3">
            <h4>I need quotes on a 40ft container of ceiling lights to Nigeria</h4>
            <div class="d-flex">
                <div><img src="assets/images/china-flag.jpg" class="me-2" style="width: 25px;" alt="">Sales Agent
                </div>
            </div>
            <p class="text-black"><strong>Category:</strong> Building materia</p>
            <div class="mt-3">
                <i class="line"><small>Response sent to you</small></i>
            </div>
            <h5>I have recieved your Response it was good</h5>
            <p class="mb-4 mt-2">Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec
                eleifend Pellentesque eu quam sem, ac malesuada Nulla eget sem vitae eros pharetra viverra Nam vitae luctus
                ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada Nulla eget sem vitae eros pharetra
                viverra Nam vitae luctus ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada Nulla
                eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec eleifend Pellentesque eu
                quam sem, ac malesuada Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec
                eleifend Pellentesque eu quam sem, ac malesuada</p>
            <div class="text-end">
                <a href="quote-respond.html"> <button class="btn btn-primary btn-sm">Respond</button></a>
            </div>
        </div>
        <div class="quotation mb-3">
            <h4>I need quotes on a 40ft container of ceiling lights to Nigeria</h4>
            <div class="d-flex">
                <div><img src="assets/images/china-flag.jpg" class="me-2" style="width: 25px;" alt="">Sales Agent
                </div>
            </div>
            <p class="text-black"><strong>Category:</strong> Building materia</p>
            <div class="mt-3">
                <i class="line"><small>Response sent to you</small></i>
            </div>
            <h5>I have recieved your Response it was good</h5>
            <p class="mb-4 mt-2">Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec
                eleifend Pellentesque eu quam sem, ac malesuada Nulla eget sem vitae eros pharetra viverra Nam vitae luctus
                ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada Nulla eget sem vitae eros pharetra
                viverra Nam vitae luctus ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada Nulla
                eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec eleifend Pellentesque eu
                quam sem, ac malesuada Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec
                eleifend Pellentesque eu quam sem, ac malesuada</p>
            <div class="text-end">
                <a href="quote-respond.html"> <button class="btn btn-primary btn-sm">Respond</button></a>
            </div>
        </div>
    </div>
@endsection
