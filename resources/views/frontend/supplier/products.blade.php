@extends('frontend.layouts.supplierMain')

@section('title', 'Track order')

@section('main-content')
    <div class="col-lg-10 content-right">
        <div class="row justify-content-center text-center">
            <div class="col-12">
                <div class="d-flex align-items-center justify-content-between mb-5">
                    <h3 class="mb-0">Products</h3>
                    <div>
                        <a href="{{route('supplier.productsAdd')}}">
                            <button class="btn btn-primary">Add Product</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach($products as $product)
                <div class="col-lg-3">
                    <div class="card product-card">
                        <a href="#">
                            <img class="card-img-top card-img-front" src="{{ asset('uploads/product/' . $product->image) }}" alt="Product Image">
                        </a>
                        <div class="card-info">
                            <div class="card-body">
                                <div class="product-title"><a class="link-title" href="#">{{ $product->product_name }}</a></div>
                                <div class="product-category">Category: {{ $product->category }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

@endsection
