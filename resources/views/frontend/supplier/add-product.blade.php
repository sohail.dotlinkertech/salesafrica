@extends('frontend.layouts.supplierMain')

{{-- @section('title', 'Track order') --}}
@section('main-content')
    <div class="col-lg-10 content-right">
        <div class="mb-4">
            <h4>Add Product</h4>
        </div>
        <form action="{{ route('supplier.products.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="">Image:</label>
                <input type="file" name="image" class="form-control py-3">
                @error('image')
                <div class="text-danger">{{ $message }}</div>
            @enderror
            </div>

            <div class="form-group">
                <label for="">Product Name:</label>
                <input type="text" name="product_name" class="form-control" placeholder="Product name">
                @error('product_name')
                <div class="text-danger">{{ $message }}</div>
            @enderror
            </div>
            <div class="form-group">
                <label for="">Product Category:</label>
                <input type="text" name="category" class="form-control" placeholder="Product category">
                @error('category')
                <div class="text-danger">{{ $message }}</div>
            @enderror
            </div>
            <div class="form-group">
                <div class="">
                    {{-- <button class="btn btn-primary me-2">Add Another</button> --}}
                    <button type="submit" class="btn btn-primary">Add Product</button>
                </div>
            </div>
        </form>
    </div>
@endsection
