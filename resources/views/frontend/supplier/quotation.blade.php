@extends('frontend.layouts.supplierMain')

@section('title', 'Track order')

@section('main-content')
      <div class="col-lg-10 content-right">
        <div class="d-flex justify-content-between mb-4">
            <h3>RFQ （Request for Quotation）</h3>
         </div>
        <div class="quotation mb-3">
            <h4>I need quotes on a 40ft container of ceiling lights to Nigeria</h4>
             <div class="d-flex">
              <p class="text-black me-5"><strong>From:</strong> Sami Building Material Nigeria Limited </p>
              <div><img src="assets/images/china-flag.jpg" class="me-2" style="width: 25px;" alt="">Sales Agent</div>
             </div>
            <p class="text-black"><strong>Category:</strong> Building materia</p>
           <p class="mb-4 mt-2">Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada</p>
           <div class="text-end">
            <a href="quote-respond.html"> <button class="btn btn-primary btn-sm">Respond</button></a>
           </div>
        </div>
        <div class="quotation mb-3">
            <h4>I need quotes on a 40ft container of ceiling lights to Nigeria</h4>
             <div class="d-flex">
              <p class="text-black me-5"><strong>From:</strong> Sami Building Material Nigeria Limited </p>
              <div><img src="assets/images/china-flag.jpg" class="me-2" style="width: 25px;" alt="">Sales Agent</div>
             </div>
            <p class="text-black"><strong>Category:</strong> Building materia</p>
           <p class="mb-4 mt-2">Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada</p>
           <div class="text-end">
            <a href="quote-respond.html"> <button class="btn btn-primary btn-sm">Respond</button></a>
           </div>
        </div>
        <div class="quotation mb-3">
            <h4>I need quotes on a 40ft container of ceiling lights to Nigeria</h4>
             <div class="d-flex">
              <p class="text-black me-5"><strong>From:</strong> Sami Building Material Nigeria Limited </p>
              <div><img src="assets/images/china-flag.jpg" class="me-2" style="width: 25px;" alt="">Sales Agent</div>
             </div>
            <p class="text-black"><strong>Category:</strong> Building materia</p>
           <p class="mb-4 mt-2">Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada</p>
           <div class="text-end">
            <a href="quote-respond.html"> <button class="btn btn-primary btn-sm">Respond</button></a>
           </div>
        </div>
    </div>

@endsection