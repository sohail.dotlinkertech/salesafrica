@extends('frontend.layouts.supplierMain')

{{-- @section('title', 'Track order') --}}

@section('main-content')

    <style>
        .containerr {
            width: 80%;
            margin: auto;
            background-color: white;
            border-radius: 10px;
        }

        .containerr .top {
            padding: 3rem;
            width: 100%;
            box-shadow: 0px -2px 14px 4px #f0f0f0;
        }

        .top h2 {
            font-size: 1.5rem;
            color: #2b79b4;
        }

        .top h5 {
            padding-bottom: .5rem;
            font-size: 1rem;
            color: #fe4c1c;
        }

        .top p {
            font-size: 1rem;
            color: rgb(211, 205, 205);
            line-height: 1rem;
        }

        .lower {
            display: flex;
        }

        .lower .box1 {
            padding: 3rem;
            width: 50%;
            border-bottom-left-radius: 10px;
            background-color: #2b79b4;
        }

        .box1 h3 {
            font-size: 2rem;
            color: white;
            padding-bottom: 1rem;
        }

        .price {
            display: flex;
            align-items: center;
        }

        .price strong {
            font-size: 1.5rem;
            color: white;
            padding-right: 10px;
            line-height: 10px;
        }

        .price span {
            font-size: 0.75rem;
            color: rgb(226, 220, 220);
        }

        .box1 p {
            font-size: 1rem;
            color: white;
            padding-top: 1rem;
            padding-bottom: 1rem;
        }

        .box1 button {
            padding: 1rem 8rem;
            color: white;
            background-color: rgb(95, 196, 95);
            border: none;
            outline: none;
        }

        .lower .box2 {
            background-color: #0a9ee8;
            padding: 2rem;
            width: 50%;
            border-bottom-right-radius: 10px;
        }

        .box2 h3 {
            font-size: 2rem;
            color: white;
            padding-bottom: 1.5rem;
        }

        .box2 ul li {
            list-style: none;
            color: white;
            font-size: 1rem;
        }

        @media screen and (max-width: 1150px) {
            html {
                font-size: 55%;
            }
        }

        @media screen and (max-width: 1050px) {
            html {
                font-size: 50%;
            }
        }

        @media screen and (max-width: 1070px) {
            html {
                font-size: 45%;
            }
        }

        @media screen and (max-width: 870px) {
            html {
                font-size: 49%;
            }

            .containerr {
                width: 80%;
                margin-top: 6rem;
            }

            main {
                align-items: flex-start;

            }

            .lower {
                flex-direction: column;
            }

            .lower .box1 {
                width: 100%;
                border-bottom-left-radius: 0px;
            }

            .lower .box1 button {
                padding: 1rem 5rem;

            }

            .lower .box2 {
                width: 100%;
                border-bottom-left-radius: 10px;
            }
        }
    </style>
    <div class="col-lg-10 content-right">
        <div class="containerr mt-5">
            <div class="top">
                <h2>See the responses after payment</h2>
                <h5>30-day, hassle-free money, back-guarantee</h5>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint deleniti
                    voluptatibus laboriosam a consequatur odio similique.</p>
            </div>
            <div class="lower">
                <div class="box1">
                    <h3>Response wise payment</h3>
                    <div class="price">
                        <strong>$29</strong>
                        <span>per response</span>
                    </div>
                    <p>Full Access for less than 1$ a day</p>
                    <button>Pay</button>
                </div>
                <div class="box2">
                    <h3>Why Us</h3>
                    <ul>
                        <li>Tutorials by industry Experts</li>
                        <li>Peer and experts code review</li>
                        <li>Coding Exercises</li>
                        <li>Access to our github Pages</li>
                        <li>Community Forum</li>
                        <li>Flashcard Checks</li>
                        <li>New videos every week</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

@endsection
