@extends('frontend.layouts.supplierMain')

@section('title', 'Track order')

@section('main-content')

    <div class="col-lg-10 content-right">
        <div class="row  mb-3">
            <div class="col-12">
                <div class="d-flex justify-content-between align-items-center">
                    <div>
                        <h4 class="mb-0">Edit Profile</h4>
                    </div>
                </div>
            </div>
        </div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        
        <div class="row">
            <div class="col-12 mx-auto">
                <div class="register-form">
                    <form id="edit-supplier-form" method="post" action="{{ route('supplier.update', $supplier->id) }}"
                        enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="row mb-2" id="sales-agentt">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="company">Company Name:</label>
                                        <input type="text" class="form-control" name="company" placeholder="Company name"
                                            value="{{ old('company', $supplier->company) }}">
                                        @error('company')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="location">Location:</label>
                                        <input type="text" class="form-control" name="location" placeholder="Location"
                                            value="{{ old('location', $supplier->location) }}">
                                        @error('location')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    @php
                                        $countries = [
                                            'Algeria',
                                            'Angola',
                                            'Benin',
                                            'Botswana',
                                            'Burkina Faso',
                                            'Burundi',
                                            'Cabo Verde',
                                            'Cameroon',
                                            'Central African Republic',
                                            'Chad',
                                            'Comoros',
                                            'Congo (Brazzaville)',
                                            'Congo (Kinshasa)',
                                            'Djibouti',
                                            'Egypt',
                                            'Equatorial Guinea',
                                            'Eritrea',
                                            'Eswatini',
                                            'Ethiopia',
                                            'Gabon',
                                            'Gambia',
                                            'Ghana',
                                            'Guinea',
                                            'Guinea-Bissau',
                                            'Ivory Coast',
                                            'Kenya',
                                            'Lesotho',
                                            'Liberia',
                                            'Libya',
                                            'Madagascar',
                                            'Malawi',
                                            'Mali',
                                            'Mauritania',
                                            'Mauritius',
                                            'Morocco',
                                            'Mozambique',
                                            'Namibia',
                                            'Niger',
                                            'Nigeria',
                                            'Rwanda',
                                            'Sao Tome and Principe',
                                            'Senegal',
                                            'Seychelles',
                                            'Sierra Leone',
                                            'Somalia',
                                            'South Africa',
                                            'South Sudan',
                                            'Sudan',
                                            'Tanzania',
                                            'Togo',
                                            'Tunisia',
                                            'Uganda',
                                            'Zambia',
                                            'Zimbabwe',
                                        ];
                                    @endphp

                                    <div class="form-group">
                                        <label for="country">Country:</label>
                                        <select class="form-control form-select" name="country">
                                            <option value="">Select Country</option>
                                            @foreach ($countries as $country)
                                                <option value="{{ $country }}"
                                                    {{ old('country', $supplier['country'] ?? '') == $country ? 'selected' : '' }}>
                                                    {{ $country }}
                                                </option>
                                            @endforeach
                                        </select>
                                        @error('country')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="business_category">Business Category:</label>
                                        <input type="text" class="form-control" name="business_category"
                                            placeholder="Business Category"
                                            value="{{ old('business_category', $supplier->business_category) }}">
                                        @error('business_category')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="company_website">Company Website:</label>
                                        <input type="text" class="form-control" name="company_website"
                                            placeholder="Company Website"value="{{ old('company_website', $supplier->company_website) }}">
                                        @error('company_website')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>


                                <div class="col-md-6 ">
                                    <div class="form-group ">
                                        <label for="">Inspection Report</label>
                                        <input type="file" name="inspection_report" class="form-control">
                                        @if ($supplier->inspection_report)
                                            <a href="{{ asset('uploads/inspection_reports/' . $supplier->inspection_report) }}"
                                                target="_blank">View current file</a>
                                        @endif

                                        @error('inspection_report')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror

                                    </div>
                                </div>

                                <div class="col-md-6 col-form-label">
                                    <div class="form-group ">
                                        <label for="">Letter of Authorization</label>
                                        <input type="file" name="letter_of_authorization" class="form-control">
                                        @if ($supplier->letter_of_authorization)
                                            <a href="{{ asset('uploads/authorization_letters/' . $supplier->letter_of_authorization) }}"
                                                target="_blank">View current file</a>
                                        @endif

                                        @error('letter_of_authorization')
                                            <span class="text-danger">{{ $message }}</span>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <h6>Information of Contact Person</h6>
                            <div class="col-md-6">

                                <div class="form-group">
                                    <label for="form_name">First Name</label>
                                    <input id="form_name" type="text" name="first_name" class="form-control"
                                        value="{{ old('company', $supplier->user->first_name) }}" placeholder="First name"
                                        required="required" data-error="Firstname is required.">
                                    <div class="help-block with-errors"></div>
                                </div>

                                <div class="form-group">
                                    <label for="image">Image</label>
                                    <input id="image" type="file" name="image" class="form-control">
                                    @if ($supplier->image)
                                        <img src="{{  $supplier->image }}" alt="Current Photo"
                                            style="max-width: 100px; max-height: 100px; margin-bottom: 10px;">
                                    @endif
                                    <div class="help-block with-errors"></div>
                                    @error('image')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_lastname">Last Name</label>
                                    <input id="form_lastname" type="text" name="last_name" class="form-control"
                                        value="{{ old('company', $supplier->user->last_name) }}" placeholder="Last name"
                                        required="required" data-error="Lastname is required.">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_email">Email</label>
                                    <input id="form_email" type="email" name="email" class="form-control"
                                        value="{{ old('company', $supplier->user->email) }}" placeholder="Email"
                                        required="required" data-error="Valid email is required.">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="form_phone">Phone</label>
                                    <input id="form_phone" type="tel" name="phone" class="form-control"
                                        value="{{ old('company', $supplier->user->phone) }}" placeholder="Phone"
                                        required="required" data-error="Phone is required">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
