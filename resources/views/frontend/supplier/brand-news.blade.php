@extends('frontend.layouts.supplierMain')

@section('title', 'Track order')
@section('main-content')

    <div class="col-lg-10 content-right">
        <div class="d-flex justify-content-between">
            <h4>Brand News</h4>
            <div>
                <a href="{{ route('supplier.news.create') }}">
                    <button class="btn btn-primary">
                        Add News
                    </button>
                </a>
            </div>
        </div>

        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif

        <div class="row mt-5">
            @foreach ($news as $item)
                <div class="col-lg-5">
                    <div class="brand-news shadow mt-3 p-5">
                        <div class="row">
                            <div class="col-8">
                                <p>{{ $loop->iteration }}. {{ $item->title }}</p>
                                <p>{{ $item->description }}</p>
                            </div>
                            {{-- <div class="col-4 text-end">
                          @if ($item->image)
                              <img src="{{ asset('upload/news' . $item->image) }}" class="news-img" alt="">
                          @else
                              <img src="{{ asset('assets/images/furniture-company.jpg') }}" class="news-img" alt="">
                          @endif
                          <img src="{{ asset('assets/images/furniture-company.jpg') }}" class="news-img" alt="">
                      </div> --}}

                            <div class="col-4 text-end">
                                @if ($item->type === 'image')
                                    <!-- Display image -->
                                    <img src="{{ asset('storage/' . $item->media) }}" class="news-img" alt="Image">
                                @elseif ($item->type === 'video')
                                    <!-- Display video -->
                                    <video controls>
                                        <source src="{{ asset('storage/' . $item->media) }}" type="video/mp4">
                                        Your browser does not support the video tag.
                                    </video>
                                @elseif ($item->type === 'link')
                                    <!-- Display YouTube link -->
                                    <iframe width="100%" height="auto" src="{{ $item->media }}" frameborder="0"
                                        allowfullscreen></iframe>
                                @else
                                    <!-- Placeholder if no media is available -->
                                    <img src="{{ asset('assets/images/furniture-company.jpg') }}" class="news-img"
                                        alt="Placeholder Image">
                                @endif
                            </div>

                        </div>
                        <div class="text-end mt-3">
                            <a href="{{ route('supplier.news.edit', $item->id) }}" class="btn btn-success btn-sm">
                                <i class="fa-solid fa-pen-to-square"></i> Edit
                            </a>
                            <form action="{{ route('supplier.news.destroy', $item->id) }}" method="POST" class="d-inline">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger btn-sm"
                                    onclick="return confirm('Are you sure you want to delete this news item?')">
                                    <i class="fa-solid fa-trash"></i> Delete
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
