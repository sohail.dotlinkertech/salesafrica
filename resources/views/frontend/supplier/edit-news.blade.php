@extends('frontend.layouts.supplierMain')

{{-- @section('title', 'Track order') --}}
@section('main-content')

    <div class="col-lg-10 content-right">
        <div class="mb-4">
            <h4>Add News</h4>
        </div>

        <form action="{{ route('supplier.news.update', ['id' => $news->id]) }}" method="POST">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="title">Brand News Title:</label>
                <input type="text" name="title" class="form-control" placeholder="Title" value="{{ $news->title }}" >
            </div>
            <div class="form-group">
                <label for="description">Brand News Description:</label>
                <textarea name="description" class="form-control textarea" placeholder="Description" >{{ $news->description }}</textarea>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
@endsection
