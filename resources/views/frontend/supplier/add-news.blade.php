@extends('frontend.layouts.supplierMain')

{{-- @section('title', 'Track order') --}}
@section('main-content')
    <div class="col-lg-10 content-right">
        <div class="mb-4">
            <h4>Add News</h4>
        </div>

        {{-- <form action="{{ route('supplier.news.store') }}" method="POST">
            @csrf
            <div class="form-group">
                <label for="title">Brand News Title:</label>
                <input type="text" name="title" class="form-control" placeholder="Title" >
            </div>
            <div class="form-group">
                <label for="description">Brand News Description:</label>
                <textarea name="description" class="form-control textarea" placeholder="Description" ></textarea>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form> --}}
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

        <form action="{{ route('supplier.news.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="title">Brand News Title:</label>
                <input type="text" name="title" class="form-control" placeholder="Title" required>
            </div>
            <div class="form-group">
                <label for="description">Brand News Description:</label>
                <textarea name="description" class="form-control textarea" placeholder="Description" required></textarea>
            </div>
            <div class="form-group">
                <label for="media">Media (Image, Video):</label>
                <input type="file" name="media" class="form-control" accept="image/*, video/*">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>

    </div>
@endsection
