@extends('frontend.layouts.supplierMain')

{{-- @section('title', 'Track order') --}}

@section('main-content')
    <div class="col-lg-10 content-right">
        <div class="row  mb-3">
            <div class="col-12">
                <div class="d-flex justify-content-between align-items-center">
                    <div>
                        <h4 class="mb-0">Suppliers</h4>
                    </div>
                    <div class="text-end">
                        <div class="input-group mb-3 border rounded">
                            <input class="form-control border-0 border-start" type="search" placeholder="Enter Your Keyword"
                                aria-label="Search">
                            <button class="btn btn-primary text-white" type="submit"><i class="las la-search"></i>
                            </button>
                        </div>
                        <div class="line">
                            <a href="#">Advanced search</a>
                            <div class="dropdown mt-3">
                                <button class="btn btn-outline-secondary btn-sm dropdown-toggle" type="button"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                    Filters
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="#">A-Z</a></li>
                                    <li><a class="dropdown-item" href="#">Newest First</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <p class="text-black">Popular Country:
                        <span class="line"><a href="#">Nigeria</a></span>
                        <span class="line"><a href="#"> South Africa</a></span>
                        <span class="line"><a href="#"> Kenya</a></span>
                        <span class="line"><a href="#"> Egypt</a></span>
                        <span class="line"><a href="#"> Tanzania</a></span>
                        <span class="line"><a href="#"> Ghana</a></span>
                        <span class="line"><a href="#"> Angola</a></span>
                        <span class="line"><a href="#"> Zimbabwe</a></span>
                        <span class="line"><a href="#"> Uganda</a></span>
                    </p>
                    <p class="text-black">Popular Category:
                        <span class="line"><a href="#">Building material</a></span>
                        <span class="line"><a href="#"> Clothes</a></span>
                        <span class="line"><a href="#"> Shoes</a></span>
                        <span class="line"><a href="#"> Fashion products</a></span>
                        <span class="line"><a href="#"> Computer accessories</a></span>
                        <span class="line"><a href="#"> Bags</a></span>
                        <span class="line"><a href="#"> Agricultural equipment</a></span>
                        <span class="line"><a href="#"> Furniture</a></span>
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="d-flex flex-wrap align-items-center text-center bg-white shadow">

                    @forelse($suppliers as $supplier)
                        <div class="clients-logo">
                            <a href="{{ route('supplier.supplier.show', $supplier->id) }}">
                                <img class="img-fluid" src="{{  $supplier->image }}" alt="">
                            </a>
                            <div class="text mt-3">
                                <a href="{{ route('supplier.supplier.show', $supplier->id) }}">
                                    <p class="mb-0"><img src="{{ asset('assets/images/china-flag.jpg') }}"
                                            style="width: 25px;" alt="">
                                        {{ $supplier->company ?? $supplier->user->name }} </p>
                                </a>

                                <div class="d-flex justify-content-center mt-3 categ">
                                    {{-- @foreach ($supplier->industries as $industry) --}}
                                    <span class="mb-0">{{ $supplier->business_category }}</span>
                                    {{-- @endforeach --}}
                                </div>
                                <a href="{{ route('supplier.personalMessage', ['userId' => $supplier->user_id]) }}">
                                    <button class="btn btn-sm btn-success">Chat</button>
                                </a>
                            </div>
                        </div>
                    @empty
                        <p>No suppliers found.</p>
                    @endforelse

                </div>
            </div>
        </div>
        <div class="">
            <nav aria-label="Page navigation" class="mt-8">
                <div class="d-flex justify-content-center align-items-center">
                    <ul class="pagination justify-content-center">
                        <li class="page-item"><a class="page-link" href="#">Previous</a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">1</a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">2</a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">3</a>
                        </li>
                        <li class="page-item"><a class="page-link" href="#">Next</a>
                        </li>
                    </ul>
                    <div class="ms-3">
                        <div class="d-flex align-items-center">Go to <input type="text"
                                class="form-control go-to p-0 ps-2 mx-2"> page</div>
                    </div>
                </div>
            </nav>
        </div>
    </div>
@endsection
