{{-- @extends('frontend.layouts.supplierMain')

@section('title', 'Track order')

@section('main-content')
<style>
    .messages ul {
        list-style-type: none;
        padding: 0;
    }

    .messages li {
        display: flex;
        align-items: center;
        margin-bottom: 10px;
    }

    .messages li.sent {
        justify-content: flex-end;
    }

    .messages li.replies {
        justify-content: flex-start;
    }

    .messages li img {
        width: 30px;
        height: 30px;
        border-radius: 50%;
        margin: 0 10px;
    }

    .messages li.sent img {
        order: 2;
        margin-left: 10px;
    }

    .messages li.replies img {
        /* order: 1; */
        margin-right: 10px;
    }

    .messages li p {
        background: #f1f0f0;
        border-radius: 10px;
        padding: 10px;
        max-width: 60%;
        word-wrap: break-word;
    }

    .messages li.sent p {
        background: #007bff;
        color: white;
        border-radius: 10px 10px 0 10px;
    }

    .messages li.replies p {
        background: #e5e5ea;
        color: black;
        border-radius: 10px 10px 10px 0;
    }
</style>
    <div class="col-lg-10 content-right">


        <div class="chat">
            <div id="frame">
                <div id="sidepanel">
                    <div id="profile">
                        <div class="wrap">
                            <div class="d-flex">
                                <div>
                                    <img id="profile-img" src="{{ asset('uploads/' . Auth::user()->image) }}" class="online profile" alt="Profile Image" />
                                </div>
                                <div>
                                    <p class="mb-0 float-none">{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</p>
                                    <p class="mb-0 float-none">Online</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content">
                    <div class="contact-profile">
                        <img id="selected-user-image" src="{{ $distributor->image ? asset('uploads/' . $distributor->image) : asset('assets/images/thumbnail/02.jpg') }}" alt="Selected User" />
                        <p id="selected-username" class="mb-0 float-none">{{ $distributor->user->first_name }} {{ $distributor->user->last_name }}</p>
                        <p class="mb-0 float-none">Online</p>
                    </div>
                    <div class="messages chat-body" id="chat-body">
                        <ul>
                        </ul>
                    </div>
                    <div class="message-input">
                        <div class="wrap">
                            <input type="text" class="form-control" id="message-input" placeholder="Write your message...">
                            <i class="fa fa-paperclip attachment" aria-hidden="true"></i>
                            <button class="submit" id="send-button" data-receiver-id="{{ $distributor->user_id }}"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/@joeattardi/emoji-button@3.0.3/dist/index.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.7.1.min.js"></script>
    <script>
        $(document).ready(function() {
            const sendMessage = () => {
                const message = $('#message-input').val();
                const receiverId = $('#send-button').data('receiver-id');
                if (message.trim() !== '') {
                    $.post('{{ route('chat.sendMessage') }}', {
                        message: message,
                        receiver_id: receiverId,
                        _token: '{{ csrf_token() }}'
                    }, function(data) {
                        $('#message-input').val('');
                        fetchMessagesWithReceiver(receiverId);
                    }).fail(function(xhr, status, error) {
                        console.log("Error: " + xhr.responseText);
                    });
                }
            };

            $('#send-button').click(sendMessage);
            $('#message-input').keypress(function(e) {
                if (e.which == 13) {
                    sendMessage();
                }
            });

            const fetchMessagesWithReceiver = (receiverId) => {
                $.get('{{ route('chat.fetchMessagesWithReceiver') }}', {
                    receiver_id: receiverId
                }, function(data) {
                    $('#chat-body ul').empty();
                    data.forEach(message => {
                        const isSentByUser = message.sender_id === {{ Auth::id() }};
                        const senderProfileUrl =
                            '{{ asset('uploads/' . Auth::user()->image) }}';

                        const receiverProfileUrl =
                            '{{ $distributor->image ? asset('uploads/' . $distributor->image) : asset('assets/images/thumbnail/02.jpg') }}';
                        const msgHtml = `
                        <li class="${isSentByUser ? 'sent' : 'replies'}">
                            <img src="${isSentByUser ? senderProfileUrl : receiverProfileUrl}" alt="Profile Image" />
                            <p>${message.message}</p>
                        </li>`;
                        $('#chat-body ul').append(msgHtml);
                    });
                    $('#chat-body').scrollTop($('#chat-body')[0].scrollHeight);
                });
            };

            // Fetch messages for the initial conversation
            fetchMessagesWithReceiver('{{ $distributor->user_id }}');
        });
    </script>
@endsection --}}


@extends('frontend.layouts.supplierMain')

@section('title', 'Chat')

@section('main-content')
    <style>
        .messages ul {
            list-style-type: none;
            padding: 0;
        }

        .messages li {
            display: flex;
            align-items: center;
            margin-bottom: 10px;
        }

        .messages li.sent {
            justify-content: flex-end;
        }

        .messages li.replies {
            justify-content: flex-start;
        }

        .messages li img {
            width: 30px;
            height: 30px;
            border-radius: 50%;
            margin: 0 10px;
        }

        .messages li.sent img {
            order: 2;
            margin-left: 10px;
        }

        .messages li.replies img {
            margin-right: 10px;
        }

        .messages li p {
            background: #f1f0f0;
            border-radius: 10px;
            padding: 10px;
            max-width: 60%;
            word-wrap: break-word;
        }

        .messages li.sent p {
            background: #007bff;
            color: white;
            border-radius: 10px 10px 0 10px;
        }

        .messages li.replies p {
            background: #e5e5ea;
            color: black;
            border-radius: 10px 10px 10px 0;
        }
    </style>
    <div class="col-lg-10 content-right">
        <div class="chat">
            <div id="frame">
                <div id="sidepanel">
                    <div id="profile">
                        <div class="wrap">
                            <div class="d-flex">
                                <div>
                                    <img id="profile-img" src="{{  Auth::user()->image }}"
                                        class="online profile" alt="Profile Image" />
                                </div>
                                <div>
                                    <p class="mb-0 float-none">{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}
                                    </p>
                                    <p class="mb-0 float-none">Online</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content">
                    <div class="contact-profile">
                        <img id="selected-user-image"
                            src="{{ $user->role == 4 ?  $user->distributor->image :  $user->salesagent->image }}"
                            alt="Selected User" />
                        <p id="selected-username" class="mb-0 float-none">{{ $user->first_name }} {{ $user->last_name }}</p>
                        <p class="mb-0 float-none">Online</p>
                    </div>
                    <div class="messages chat-body" id="chat-body">
                        <ul>
                            {{-- Messages will be dynamically loaded here --}}
                        </ul>
                    </div>
                    <div class="message-input">
                        <div class="wrap">
                            <input type="text" class="form-control" id="message-input"
                                placeholder="Write your message...">
                            <i class="fa fa-paperclip attachment" aria-hidden="true"></i>
                            <button class="submit" id="send-button" data-receiver-id="{{ $user->id }}"><i
                                    class="fa fa-paper-plane" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/@joeattardi/emoji-button@3.0.3/dist/index.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.7.1.min.js"></script>
    <script>
        $(document).ready(function() {
            const sendMessage = () => {
                const message = $('#message-input').val();
                const receiverId = $('#send-button').data('receiver-id');
                if (message.trim() !== '') {
                    $.post('{{ route('chat.sendMessage') }}', {
                        message: message,
                        receiver_id: receiverId,
                        _token: '{{ csrf_token() }}'
                    }, function(data) {
                        $('#message-input').val('');
                        fetchMessagesWithReceiver(receiverId);
                    }).fail(function(xhr, status, error) {
                        console.log("Error: " + xhr.responseText);
                    });
                }
            };

            $('#send-button').click(sendMessage);
            $('#message-input').keypress(function(e) {
                if (e.which == 13) {
                    sendMessage();
                }
            });

            const fetchMessagesWithReceiver = (receiverId) => {
                $.get('{{ route('chat.fetchMessagesWithReceiver') }}', {
                    receiver_id: receiverId
                }, function(data) {
                    $('#chat-body ul').empty();
                    data.forEach(message => {
                        const isSentByUser = message.sender_id === {{ Auth::id() }};
                        const senderProfileUrl =
                            '{{  Auth::user()->image }}';
                        const receiverProfileUrl =
                            '{{ $user->role == 4 ?  $user->distributor->image :  $user->salesagent->image }}';
                        const msgHtml = `
                    <li class="${isSentByUser ? 'sent' : 'replies'}">
                        <img src="${isSentByUser ? senderProfileUrl : receiverProfileUrl}" alt="Profile Image" />
                        <p>${message.message}</p>
                    </li>`;
                        $('#chat-body ul').append(msgHtml);
                    });
                    $('#chat-body').scrollTop($('#chat-body')[0].scrollHeight);
                });
            };

            // Fetch messages for the initial conversation
            fetchMessagesWithReceiver('{{ $user->id }}');
        });
    </script>
@endsection
