@extends('frontend.layouts.supplierMain')

@section('title', 'Track order')

@section('main-content')
<style>
    .messages ul {
        list-style-type: none;
        padding: 0;
    }

    .messages li {
        display: flex;
        align-items: center;
        margin-bottom: 10px;
    }

    .messages li.sent {
        justify-content: flex-end;
    }

    .messages li.replies {
        justify-content: flex-start;
    }

    .messages li img {
        width: 30px;
        height: 30px;
        border-radius: 50%;
        margin: 0 10px;
    }

    .messages li.sent img {
        order: 2;
        margin-left: 10px;
    }

    .messages li.replies img {
        /* order: 1; */
        margin-right: 10px;
    }

    .messages li p {
        background: #f1f0f0;
        border-radius: 10px;
        padding: 10px;
        max-width: 60%;
        word-wrap: break-word;
    }

    .messages li.sent p {
        background: #007bff;
        color: white;
        border-radius: 10px 10px 0 10px;
    }

    .messages li.replies p {
        background: #e5e5ea;
        color: black;
        border-radius: 10px 10px 10px 0;
    }
</style>

    <div class="col-lg-10 content-right">
        <h4>Chat </h4>
        <div id="frame">
            <div id="sidepanel">
                <div id="profile">
                    <div class="wrap">
                        <div class="d-flex">
                            <div>
                                <img id="profile-img" src="{{ asset('uploads/' . Auth::user()->supplier->image) }}" class="online" alt="Profile Image" />
                            </div>
                            <div>
                                <p class="mb-0 float-none">{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</p>
                                <p class="mb-0 float-none">Online</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="search">
                    <label for=""><i class="fa fa-search" aria-hidden="true"></i></label>
                    <input type="text" placeholder="Search contacts..." />
                </div>
            

                {{-- <div id="contacts">
                    <ul id="user-list">
                        @foreach ($conversations as $conversation)
                        @php
                            // Determine the other user based on current user's role in the conversation
                            $otherUser = $conversation->sender_id === auth()->id() ? $conversation->receiver : $conversation->sender;
                            $supplier = $otherUser->supplier;
                        @endphp
                        <li class="contact" data-user-id="{{ $otherUser->id }}">
                            <div class="wrap">
                                <span class="contact-status online"></span>
                                <img src="{{ $supplier ? asset('uploads/' . $supplier->image) : asset('assets/images/icons/default-profile.jpg') }}" alt="Profile Image" />
                                <div class="meta">
                                    <p class="name">{{ $otherUser->first_name }} {{ $otherUser->last_name }}</p>
                                    <p class="preview">{{ $supplier ? Str::limit($supplier->introduction, 30) : '' }}</p>
                                </div>
                            </div>
                        </li>
                    @endforeach
                    </ul>
                </div> --}}

                <div id="contacts">
                    <ul id="user-list">
                        @foreach ($conversations as $conversation)
                            @php
                                // Determine the other user based on the current user's role in the conversation
                                $otherUser = $conversation->sender_id === auth()->id() ? $conversation->receiver : $conversation->sender;
                                $role = $otherUser->role;
                
                                // Determine the profile based on user role
                                $profile = null;
                                if ($role == 4) {
                                    $profile = $otherUser->distributor;
                                } elseif ($role == 3) {
                                    $profile = $otherUser->supplier;
                                }
                            @endphp
                            <li class="contact" data-user-id="{{ $otherUser->id }}">
                                <div class="wrap">
                                    <span class="contact-status online"></span>
                                    <img src="{{ $profile && $profile->image ? asset('uploads/' . $profile->image) : asset('assets/images/icons/default-profile.jpg') }}" alt="Profile Image" />
                                    <div class="meta">
                                        <p class="name">{{ $otherUser->first_name }} {{ $otherUser->last_name }}</p>
                                        <p class="preview">{{ $profile ? Str::limit($profile->introduction, 30) : '' }}</p>
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
                
            </div>
            <div class="content">
                <div class="contact-profile">
                    <img id="selected-user-image" src="" alt="" />
                    <p id="selected-username" class="mb-0 float-none">Harvey Specter</p>
                    <p class="mb-0 float-none">Online</p>
                </div>
                <div class="messages chat-body" id="chat-body">
                    <ul>
                        {{-- Messages will be dynamically loaded here --}}
                    </ul>
                </div>
                <div class="message-input">
                    <div class="wrap">
                        <input type="text" class="form-control" id="message-input" placeholder="Write your message...">
                        <i class="fa fa-paperclip attachment" aria-hidden="true"></i>
                        <button class="submit" id="send-button"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/@joeattardi/emoji-button@3.0.3/dist/index.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.7.1.min.js"></script>
    <script>
        $(document).ready(function() {
            const sendMessage = () => {
                const message = $('#message-input').val();
                const receiverId = $('#send-button').data('receiver-id');
                if (message.trim() !== '') {
                    $.post('{{ route('chat.sendMessage') }}', {
                        message: message,
                        receiver_id: receiverId,
                        _token: '{{ csrf_token() }}'
                    }, function(data) {
                        $('#message-input').val('');
                        fetchMessagesWithReceiver(receiverId);
                    }).fail(function(xhr, status, error) {
                        console.log("Error: " + xhr.responseText);
                    });
                }
            };

            $('#send-button').click(sendMessage);
            $('#message-input').keypress(function(e) {
                if (e.which == 13) {
                    sendMessage();
                }
            });

            const fetchMessagesWithReceiver = (receiverId) => {
                $.get('{{ route('chat.fetchMessagesWithReceiver') }}', {
                    receiver_id: receiverId
                }, function(data) {
                    $('#chat-body ul').empty();
                    data.forEach(message => {
                        const isSentByUser = message.sender_id === {{ Auth::id() }};
                        const senderProfileUrl = '{{ asset('uploads/' . auth()->user()->supplier->image) }}';
                        const receiverProfileUrl = ''; // You need to define this for the receiver
                        const msgHtml = `
                            <li class="${isSentByUser ? 'sent' : 'replies'}">
                                <img src="${isSentByUser ? senderProfileUrl : receiverProfileUrl}" alt="Profile Image" />
                                <p>${message.message}</p>
                            </li>`;
                        $('#chat-body ul').append(msgHtml);
                    });
                    $('#chat-body').scrollTop($('#chat-body')[0].scrollHeight);
                });
            };

            $('#user-list').on('click', '.contact', function() {
                const receiverId = $(this).data('user-id');
                const username = $(this).find('.name').text();
                const profileImage = $(this).find('img').attr('src');
                $('#selected-user-image').attr('src', profileImage);
                $('#selected-username').text(username);
                $('#user-list .contact').removeClass('active');
                $(this).addClass('active');
                fetchMessagesWithReceiver(receiverId);
                $('#send-button').data('receiver-id', receiverId);
            });

            if (initialReceiverId) {
                fetchMessagesWithReceiver(initialReceiverId);
                $('#send-button').data('receiver-id', initialReceiverId);
            }
        });
    </script>
@endsection
