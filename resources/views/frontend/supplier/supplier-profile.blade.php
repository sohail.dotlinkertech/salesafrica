@extends('frontend.layouts.supplierMain')

@section('title', 'Track order')

@section('main-content')

    <div class="col-lg-10 content-right">
        <div class="row  mb-3">
            <div class="col-12">
                <div class="d-flex justify-content-between align-items-center">
                    <div>
                        <h4 class="mb-0">Supplier Profile</h4>
                    </div>
                    <div class="">
                        <a href="{{ route('supplier.edit') }}">
                            <button class="btn btn-primary">
                                Edit profile
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="d-flex text mt-3">
                    <div class="me-3  profile-pic">
                        <div>
                            @if (auth()->user()->supplier && auth()->user()->supplier->image)
                                <img src="{{  auth()->user()->supplier->image }}" class="img-fluid img"
                                    alt="Profile Image">
                            @else
                                <img class="img-fluid img" src="{{ asset('assets/images/thumbnail/01.jpg') }}"
                                    alt="">
                            @endif
                        </div>

                        <img src="{{ asset('assets/images/vip-stamp-2.png') }}" class="vipp" alt="">
                    </div>
                    <div class="d-flex justify-content-center">
                        <div class="me-4">
                            <div>Company Name:</div>
                            <div>Name:</div>
                            <div>Email:</div>
                            <div>Phone:</div>
                            <div>Location:</div>
                            <div>Country:</div>
                            <div>Business category:</div>
                        </div>
                        <div>

                            <div>{{ auth()->user()->supplier->company ?? '-' }}</div>
                            <div>{{ auth()->user()->first_name }} {{ auth()->user()->last_name }}</div>
                            <div>{{ auth()->user()->email }}</div>
                            <div>{{ auth()->user()->phone }}</div>
                            <div>{{ auth()->user()->supplier->location ?? '-' }}</div>
                            <div>{{ auth()->user()->supplier->country ?? '-' }}</div>
                            <div>{{ auth()->user()->supplier->business_category ?? '-' }}</div>
                        </div>


                    </div>
                </div>
                <div class="mt-5">
                    <a href="{{ route('supplier.subscription') }}">
                        <button class="btn btn-success">
                            Upgrade to VIP
                        </button>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
