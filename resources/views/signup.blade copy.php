<!DOCTYPE html>
<html lang="en">

<head>

    <!-- meta tags -->
    <meta charset="utf-8">
    <meta name="keywords" content="bootstrap 5, premium, multipurpose, sass, scss, saas, eCommerce, Shop, Fashion" />
    <meta name="description" content="Bootstrap 5 Landing Page Template" />
    <meta name="author" content="www.themeht.com" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Title -->
    <title>Ekocart - Multipurpose eCommerce HTML5 Template</title>

    <!-- Favicon Icon -->
    <link rel="shortcut icon" href="assets/images/favicon.ico" />

    <!-- inject css start -->

    <link href="{{ asset('assets/css/theme-plugin.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/theme.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/custom.css') }}" rel="stylesheet" />
    <!-- inject css end -->

</head>

<body>

    <!-- page wrapper start -->

    <div class="page-wrapper">

        <!--body content start-->

        <div class="page-content">

            <!--login start-->

            <section class="register">
                <div class="container">
                    <div class="row justify-content-center text-center">
                        <div class="col-lg-8 col-md-12">
                            <div class="mb-6">
                                <h6 class="text-primary mb-1">
                                    Sign Up
                                </h6>
                                <h2>Simple And Easy To Sign Up</h2>
                                <p class="lead">Join the largest sales network in Africa, start communication with
                                    suppliers, sales agents or distributors.
                                    It is FREE of charge.</p>
                            </div>
                        </div>
                    </div>
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-lg-8 col-md-10 mx-auto">
                            <div class="register-form">
                                <form method="POST" action="{{ route('signup.post') }}">
                                    @csrf
                                    <input type="hidden" name="role" id="role" value="Supplier">
                                    <!-- Default value -->
                                    <div class="messages"></div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Select Role</label>
                                                <div class="d-flex my-2">
                                                    <div class="form-check me-2">
                                                        <input class="form-check-input roleSelect" type="radio"
                                                            value="3" name="role" id="supplier" >
                                                        <label class="form-check-label" for="supplier">Supplier</label>
                                                    </div>

                                                    <div class="form-check me-2">
                                                        <input class="form-check-input roleSelect" type="radio"
                                                            value="5" name="role" id="sales-agent">
                                                        <label class="form-check-label" for="sales-agent">Sales
                                                            Agent</label>
                                                    </div>
                                                    <div class="form-check me-2">
                                                        <input class="form-check-input roleSelect" type="radio"
                                                            value="4" name="role" id="distributor">
                                                        <label class="form-check-label"
                                                            for="distributor">Distributor</label>
                                                    </div>
                                                </div>
                                                <small><i>If you buy and resell, you are a distributor. If you promote
                                                        products for others and earn a commission, you are a sales
                                                        agent. </i></small>
                                            </div>
                                         
                                        </div>
                                    </div>
                                    {{-- <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Select Role</label>
                                                <div class="d-flex my-2">
                                                    <div class="form-check me-2">
                                                        <input class="form-check-input roleSelect" type="radio" value="3" name="role" id="supplier" checked>
                                                        <label class="form-check-label" for="supplier">Supplier</label>
                                                    </div>
                                                    <div class="form-check me-2">
                                                        <input class="form-check-input roleSelect" type="radio" value="5" name="role" id="sales-agent">
                                                        <label class="form-check-label" for="sales-agent">Sales Agent</label>
                                                    </div>
                                                    <div class="form-check me-2">
                                                        <input class="form-check-input roleSelect" type="radio" value="4" name="role" id="distributor">
                                                        <label class="form-check-label" for="distributor">Distributor</label>
                                                    </div>
                                                </div>
                                                <small><i>If you buy and resell, you are a distributor. If you promote products for others and earn a commission, you are a sales agent. </i></small>
                                            </div>
                                        </div>
                                    </div> --}}

                                    {{-- <div class="row mb-2" id="supplierr" style="display: none;"> --}}
                                    <div class="row mb-2" id="supplierr">


                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="company">Company Name:</label>
                                                <input type="text" class="form-control" name="company"
                                                    placeholder="Company name" value="{{ old('company') }}">
                                                @error('company')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                      

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="location">Location:</label>
                                                <input type="text" class="form-control" name="location"
                                                    placeholder="Location" value="{{ old('location') }}">
                                                @error('location')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>


                                        <div class="col-md-6">
                                            @php
                                                $countries = [
                                                    'Algeria',
                                                    'Angola',
                                                    'Benin',
                                                    'Botswana',
                                                    'Burkina Faso',
                                                    'Burundi',
                                                    'Cabo Verde',
                                                    'Cameroon',
                                                    'Central African Republic',
                                                    'Chad',
                                                    'Comoros',
                                                    'Congo (Brazzaville)',
                                                    'Congo (Kinshasa)',
                                                    'Djibouti',
                                                    'Egypt',
                                                    'Equatorial Guinea',
                                                    'Eritrea',
                                                    'Eswatini',
                                                    'Ethiopia',
                                                    'Gabon',
                                                    'Gambia',
                                                    'Ghana',
                                                    'Guinea',
                                                    'Guinea-Bissau',
                                                    'Ivory Coast',
                                                    'Kenya',
                                                    'Lesotho',
                                                    'Liberia',
                                                    'Libya',
                                                    'Madagascar',
                                                    'Malawi',
                                                    'Mali',
                                                    'Mauritania',
                                                    'Mauritius',
                                                    'Morocco',
                                                    'Mozambique',
                                                    'Namibia',
                                                    'Niger',
                                                    'Nigeria',
                                                    'Rwanda',
                                                    'Sao Tome and Principe',
                                                    'Senegal',
                                                    'Seychelles',
                                                    'Sierra Leone',
                                                    'Somalia',
                                                    'South Africa',
                                                    'South Sudan',
                                                    'Sudan',
                                                    'Tanzania',
                                                    'Togo',
                                                    'Tunisia',
                                                    'Uganda',
                                                    'Zambia',
                                                    'Zimbabwe',
                                                ];
                                            @endphp

                                            <div class="form-group">
                                                <label for="country">Country:</label>
                                                <select class="form-control form-select" name="country">
                                                    <option value="">Select Country</option>
                                                    @foreach ($countries as $country)
                                                        <option value="{{ $country }}"
                                                            {{ old('country') == $country ? 'selected' : '' }}>
                                                            {{ $country }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @error('country')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>


                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="business_category">Business Category:</label>
                                                <input type="text" class="form-control" name="business_category"
                                                    placeholder="Business Category"
                                                    value="{{ old('business_category') }}">
                                                @error('business_category')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="company_website">Company Website:</label>
                                                <input type="text" class="form-control" name="company_website"
                                                    placeholder="Company Website"
                                                    value="{{ old('company_website') }}">
                                                @error('company_website')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>

                                    </div>

                                    {{-- <div class="row mb-2" id="distributorr" style="display: none;"> --}}
                                    <div class="row mb-2 d-none" id="distributorr" >

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="company">Company Name:</label>
                                                <input type="text" class="form-control" name="company"
                                                    placeholder="Company name" value="{{ old('company') }}">
                                                @error('company')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="col-12">
                                            <div class="form-check me-2 mb-2">
                                                <input class="form-check-input float-none" type="checkbox"
                                                    name="customCheck1" id="customCheck1">
                                                <label class="form-check-label" for="customCheck1">I am an
                                                    individual distributor who distribute products</label>
                                            </div>
                                        </div>



                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="location">Location:</label>
                                                <input type="text" class="form-control" name="location"
                                                    placeholder="Location" value="{{ old('location') }}">
                                                @error('location')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            @php
                                                $countries = [
                                                    'Algeria',
                                                    'Angola',
                                                    'Benin',
                                                    'Botswana',
                                                    'Burkina Faso',
                                                    'Burundi',
                                                    'Cabo Verde',
                                                    'Cameroon',
                                                    'Central African Republic',
                                                    'Chad',
                                                    'Comoros',
                                                    'Congo (Brazzaville)',
                                                    'Congo (Kinshasa)',
                                                    'Djibouti',
                                                    'Egypt',
                                                    'Equatorial Guinea',
                                                    'Eritrea',
                                                    'Eswatini',
                                                    'Ethiopia',
                                                    'Gabon',
                                                    'Gambia',
                                                    'Ghana',
                                                    'Guinea',
                                                    'Guinea-Bissau',
                                                    'Ivory Coast',
                                                    'Kenya',
                                                    'Lesotho',
                                                    'Liberia',
                                                    'Libya',
                                                    'Madagascar',
                                                    'Malawi',
                                                    'Mali',
                                                    'Mauritania',
                                                    'Mauritius',
                                                    'Morocco',
                                                    'Mozambique',
                                                    'Namibia',
                                                    'Niger',
                                                    'Nigeria',
                                                    'Rwanda',
                                                    'Sao Tome and Principe',
                                                    'Senegal',
                                                    'Seychelles',
                                                    'Sierra Leone',
                                                    'Somalia',
                                                    'South Africa',
                                                    'South Sudan',
                                                    'Sudan',
                                                    'Tanzania',
                                                    'Togo',
                                                    'Tunisia',
                                                    'Uganda',
                                                    'Zambia',
                                                    'Zimbabwe',
                                                ];
                                            @endphp

                                            <div class="form-group">
                                                <label for="country">Country:</label>
                                                <select class="form-control form-select" name="country">
                                                    <option value="">Select Country</option>
                                                    @foreach ($countries as $country)
                                                        <option value="{{ $country }}"
                                                            {{ old('country') == $country ? 'selected' : '' }}>
                                                            {{ $country }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @error('country')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="business_category">Business Category:</label>
                                                <input type="text" class="form-control" name="business_category"
                                                    placeholder="Business Category"
                                                    value="{{ old('business_category') }}">
                                                @error('business_category')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="company_website">Company Website:</label>
                                                <input type="text" class="form-control" name="company_website"
                                                    placeholder="Company Website"
                                                    value="{{ old('company_website') }}">
                                                @error('company_website')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>

                                    {{-- <div class="row mb-2" id="sales-agentt" style="display: none;"> --}}
                                    <div class="row mb-2" id="sales-agentt" class="d-none">

                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="company">Company Name:</label>
                                                <input type="text" class="form-control" name="company"
                                                    placeholder="Company name" value="{{ old('company') }}">
                                                @error('company')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="col-12">
                                            <div class="form-check me-2 mb-2">
                                                <input class="form-check-input float-none" type="checkbox"
                                                    name="customCheck1" id="customCheck1">
                                                <label class="form-check-label" for="customCheck1">I am an
                                                    individual sales agent</label>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="location">Location:</label>
                                                <input type="text" class="form-control" name="location"
                                                    placeholder="Location" value="{{ old('location') }}">
                                                @error('location')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            @php
                                                $countries = [
                                                    'Algeria',
                                                    'Angola',
                                                    'Benin',
                                                    'Botswana',
                                                    'Burkina Faso',
                                                    'Burundi',
                                                    'Cabo Verde',
                                                    'Cameroon',
                                                    'Central African Republic',
                                                    'Chad',
                                                    'Comoros',
                                                    'Congo (Brazzaville)',
                                                    'Congo (Kinshasa)',
                                                    'Djibouti',
                                                    'Egypt',
                                                    'Equatorial Guinea',
                                                    'Eritrea',
                                                    'Eswatini',
                                                    'Ethiopia',
                                                    'Gabon',
                                                    'Gambia',
                                                    'Ghana',
                                                    'Guinea',
                                                    'Guinea-Bissau',
                                                    'Ivory Coast',
                                                    'Kenya',
                                                    'Lesotho',
                                                    'Liberia',
                                                    'Libya',
                                                    'Madagascar',
                                                    'Malawi',
                                                    'Mali',
                                                    'Mauritania',
                                                    'Mauritius',
                                                    'Morocco',
                                                    'Mozambique',
                                                    'Namibia',
                                                    'Niger',
                                                    'Nigeria',
                                                    'Rwanda',
                                                    'Sao Tome and Principe',
                                                    'Senegal',
                                                    'Seychelles',
                                                    'Sierra Leone',
                                                    'Somalia',
                                                    'South Africa',
                                                    'South Sudan',
                                                    'Sudan',
                                                    'Tanzania',
                                                    'Togo',
                                                    'Tunisia',
                                                    'Uganda',
                                                    'Zambia',
                                                    'Zimbabwe',
                                                ];
                                            @endphp

                                            <div class="form-group">
                                                <label for="country">Country:</label>
                                                <select class="form-control form-select" name="country">
                                                    <option value="">Select Country</option>
                                                    @foreach ($countries as $country)
                                                        <option value="{{ $country }}"
                                                            {{ old('country') == $country ? 'selected' : '' }}>
                                                            {{ $country }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                @error('country')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="business_category">Business Category:</label>
                                                <input type="text" class="form-control" name="business_category"
                                                    placeholder="Business Category"
                                                    value="{{ old('business_category') }}">
                                                @error('business_category')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="company_website">Company Website:</label>
                                                <input type="text" class="form-control" name="company_website"
                                                    placeholder="Company Website"
                                                    value="{{ old('company_website') }}">
                                                @error('company_website')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>



                                    <h4>Information of Contact Person</h4>



                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="form_name">First Name</label>
                                                <input id="form_name" type="text" name="first_name"
                                                    class="form-control" placeholder="First name" required="required"
                                                    data-error="Firstname is required.">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="form_lastname">Last Name</label>
                                                <input id="form_lastname" type="text" name="last_name"
                                                    class="form-control" placeholder="Last name" required="required"
                                                    data-error="Lastname is required.">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="form_email">Email</label>
                                                <input id="form_email" type="email" name="email"
                                                    class="form-control" placeholder="Email" required="required"
                                                    data-error="Valid email is required.">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="form_phone">Phone</label>
                                                <input id="form_phone" type="tel" name="phone"
                                                    class="form-control" placeholder="Phone" required="required"
                                                    data-error="Phone is required">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        {{-- <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="email_verification">Email Verification Code</label>
                                                    <input id="email_verification" type="text"
                                                        name="email_verification_code" class="form-control"
                                                        placeholder="Enter email verification code">
                                                    <button type="button" class="btn btn-sm btn-secondary mt-2"
                                                        onclick="sendEmailVerificationCode()">Send Verification
                                                        Code</button>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="phone_verification">Phone Verification Code</label>
                                                    <input id="phone_verification" type="text"
                                                        name="phone_verification_code" class="form-control"
                                                        placeholder="Enter phone verification code">
                                                    <button type="button" class="btn btn-sm btn-secondary mt-2"
                                                        onclick="sendPhoneVerificationCode()">Send Verification
                                                        Code</button>
                                                </div>
                                            </div> --}}
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="form_password">Password</label>
                                                <input id="form_password" type="password" name="password"
                                                    class="form-control" placeholder="Password" required="required"
                                                    data-error="Password is required.">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="form_password1">Confirm Password</label>
                                                <input id="form_password1" type="password"
                                                    name="password_confirmation" class="form-control"
                                                    placeholder="Confirm Password" required="required"
                                                    data-error="Confirm Password is required.">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mt-5">
                                        <div class="col-md-12">
                                            <div class="remember-checkbox clearfix mb-5">
                                                <div class="form-check">
                                                    <input class="form-check-input float-none" type="checkbox"
                                                        name="customCheck1" id="customCheck1" required>
                                                    <label class="form-check-label" for="customCheck1">I agree to
                                                        the term of use and privacy policy</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">


                                            <button type="submit" class="btn btn-primary">Create Account</button>
                                            <span class="mt-4 d-block">Have An Account? <a
                                                    href="{{ route('login') }}"><i>Sign In!</i></a></span>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!--login end-->

        </div>

        <!--body content end-->

    </div>

    <!-- page wrapper end -->

    <!-- inject js start -->
    <script src="{{ asset('assets/js/theme-plugin.js') }}"></script>
    <script src="{{ asset('assets/js/theme-script.js') }}"></script>

    {{-- <script>
        document.addEventListener('DOMContentLoaded', function() {
            const roleSelects = document.querySelectorAll('.roleSelect');
            const supplier = document.getElementById('supplierr');
            const distributor = document.getElementById('distributorr');
            const salesAgent = document.getElementById('sales-agentt');

            supplier.style.display = 'flex';
            roleSelects.forEach(roleSelect => {
                // roleSelect.addEventListener('change', function() {
                //     if (this.value === 'Supplier') {
                //         supplier.style.display = 'flex';
                //         distributor.style.display = 'none';
                //         salesAgent.style.display = 'none';
                //     } else if (this.value === 'SalesAgent') {
                //         supplier.style.display = 'none';
                //         distributor.style.display = 'none';
                //         salesAgent.style.display = 'flex';
                //     } else if (this.value === 'Distributor') {
                //         supplier.style.display = 'none';
                //         distributor.style.display = 'flex';
                //         salesAgent.style.display = 'none';
                //     } else {
                //         supplier.style.display = 'none';
                //         distributor.style.display = 'none';
                //         salesAgent.style.display = 'none';
                //     }
                // });
                roleSelect.addEventListener('change', function() {
                    if (this.value === '3') {
                        supplier.style.display = 'flex';
                        distributor.style.display = 'none';
                        salesAgent.style.display = 'none';
                    } else if (this.value === '5') {
                        supplier.style.display = 'none';
                        distributor.style.display = 'none';
                        salesAgent.style.display = 'flex';
                    } else if (this.value === '4') {
                        supplier.style.display = 'none';
                        distributor.style.display = 'flex';
                        salesAgent.style.display = 'none';
                    } else {
                        supplier.style.display = 'none';
                        distributor.style.display = 'none';
                        salesAgent.style.display = 'none';
                    }
                });

            });
        });

        function sendEmailVerificationCode() {
            // Implement email verification code sending logic here
            alert('Email verification code sent!');
        }

        function sendPhoneVerificationCode() {
            // Implement phone verification code sending logic here
            alert('Phone verification code sent');
        }
    </script> --}}

    
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            const roleSelects = document.querySelectorAll('.roleSelect');
            const supplier = document.getElementById('supplierr');
            const distributor = document.getElementById('distributorr');
            const salesAgent = document.getElementById('sales-agentt');

            // Function to show role fields based on selected role
            function showRoleFields(role) {
                supplier.classList.add('d-none');
                distributor.classList.add('d-none');
                salesAgent.classList.add('d-none');

                if (role === '3') {
                    supplier.classList.remove('d-none');
                } else if (role === '4') {
                    distributor.classList.remove('d-none');
                } else if (role === '5') {
                    salesAgent.classList.remove('d-none');
                }
            }

            // Initial state setup based on default selected role (Supplier)
            showRoleFields('3'); // '3' corresponds to the value of the Supplier role

            // Event listener for role selection changes
            roleSelects.forEach(roleSelect => {
                roleSelect.addEventListener('change', function() {
                    const selectedRole = this.value;
                    showRoleFields(selectedRole);
                });
            });
        });
    </script>

        <script>
    document.addEventListener('DOMContentLoaded', function() {
        const roleSelects = document.querySelectorAll('.roleSelect');
        const supplier = document.getElementById('supplierr');
        const distributor = document.getElementById('distributorr');
        const salesAgent = document.getElementById('sales-agentt');

        // Function to show role fields based on selected role
        function showRoleFields(role) {
            supplier.classList.add('d-none');
            distributor.classList.add('d-none');
            salesAgent.classList.add('d-none');

            if (role === '3') {
                supplier.classList.remove('d-none');
            } else if (role === '4') {
                distributor.classList.remove('d-none');
            } else if (role === '5') {
                salesAgent.classList.remove('d-none');
            }
        }

        // Initial state setup based on default selected role (Supplier)
        showRoleFields('3'); // '3' corresponds to the value of the Supplier role

        // Event listener for role selection changes
        roleSelects.forEach(roleSelect => {
            roleSelect.addEventListener('change', function() {
                const selectedRole = this.value;
                showRoleFields(selectedRole);
            });
        });
    });
</script>



 
    <!-- inject js end -->

</body>

</html>
