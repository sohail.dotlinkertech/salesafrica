<!DOCTYPE html>
<html lang="en">

<head>

    <!-- meta tags -->
    <meta charset="utf-8">
    <meta name="keywords" content="bootstrap 5, premium, multipurpose, sass, scss, saas, eCommerce, Shop, Fashion" />
    <meta name="description" content="Bootstrap 5 Landing Page Template" />
    <meta name="author" content="www.themeht.com" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Title -->
    <title>SalesAfrica</title>

    <!-- Favicon Icon -->
    <link rel="shortcut icon" href="{{ asset('assets/images/favicon.ico') }}" />

    <!-- inject css start -->
    <link href="{{ asset('assets/css/theme-plugin.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/theme.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/custom.css') }}" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/line-awesome/1.3.0/line-awesome/css/line-awesome.min.css
"
        rel="stylesheet" />
    <!-- inject css end -->

</head>

<body>

    <!-- page wrapper start -->

    <div class="page-wrapper">

        <!-- preloader start -->

        <div id="ht-preloader">
            <div class="loader clear-loader">
                <img class="img-fluid" src="{{ asset('assets/images/loader.gif') }}" alt="">
            </div>
        </div>

        <!-- preloader end -->


        <!--header start-->

        <header class="site-header">
            <div id="header-wrap" class="shadow-sm py-md-5 py-3 fixed-top">
                <div class="container">
                    <div class="row">
                        <!--menu start-->
                        <div class="col">
                            <nav class="navbar navbar-expand-lg navbar-light">
                                <!-- <a class="navbar-brand logo d-lg-none" href="index.html">
              <img class="img-fluid" src="assets/images/logo.png" alt="">
            </a> -->
                                <a class="navbar-brand logo fs25 me-4" href="{{ route('index') }}">
                                    Sales<span class="text-primary">Africa</span>
                                </a>
                                <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                                    <span class="navbar-toggler-icon"></span>
                                </button>
                                <div class="collapse navbar-collapse" id="navbarNav">
                                    <ul class="navbar-nav">
                                        <li class="nav-item"> <a class="nav-link" href="{{ route('index') }}">Home</a>
                                        </li>
                                        <li class="nav-item"> <a class="nav-link" href="#">About</a>
                                        </li>
                                        <li class="nav-item"> <a class="nav-link"
                                                href="{{ route('contactUs') }}">Contact</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="right-nav align-items-center d-flex justify-content-end">
                                    <span class="me-2">
                                        <a href="{{ route('login') }}">Join Now</a>
                                    </span>
                                </div>
                            </nav>
                        </div>
                        <!--menu end-->
                    </div>
                </div>
            </div>
        </header>

        @yield('main-content')


   


    

        <footer class="py-11 bg-dark">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-3"> <a class="footer-logo text-white h2 mb-0" href="index.html">
                            Sales<span class="text-primary">Africa</span>
                        </a>
                        <p class="my-3 text-muted">Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                            Asperiores, suscipit iusto! Sequi recusandae perspiciatis sit incidunt? Iure doloribus iste
                            natus?.</p>
                        <ul class="list-inline mb-0">
                            <li class="list-inline-item"><a class="text-light ic-2x" href="#"><i
                                        class="la la-facebook"></i></a>
                            </li>
                            <li class="list-inline-item"><a class="text-light ic-2x" href="#"><i
                                        class="la la-dribbble"></i></a>
                            </li>
                            <li class="list-inline-item"><a class="text-light ic-2x" href="#"><i
                                        class="la la-instagram"></i></a>
                            </li>
                            <li class="list-inline-item"><a class="text-light ic-2x" href="#"><i
                                        class="la la-twitter"></i></a>
                            </li>
                            <li class="list-inline-item"><a class="text-light ic-2x" href="#"><i
                                        class="la la-linkedin"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12 col-lg-6 mt-6 mt-lg-0">
                        <div class="row">
                            <div class="col-12 col-sm-4 navbar-dark">
                                <h5 class="mb-4 text-white">Quick Links</h5>
                                <ul class="navbar-nav list-unstyled mb-0">
                                    <li class="mb-3 nav-item"><a class="nav-link"
                                            href="{{ route('index') }}">Home</a>
                                    </li>
                                    <li class="mb-3 nav-item"><a class="nav-link"
                                            href="{{ route('index') }}">About</a>
                                    </li>
                                    <li class="mb-3 nav-item"><a class="nav-link" href="faq.html">Faq</a>
                                    </li>
                                    <li class="nav-item"><a class="nav-link" href="{{ route('contactUs') }}">Contact
                                            Us</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-4 mt-6 mt-sm-0 navbar-dark">
                                <h5 class="mb-4 text-white">Top Products</h5>
                                <ul class="navbar-nav list-unstyled mb-0">
                                    <li class="mb-3 nav-item"><a class="nav-link" href="#">T-Shirts</a>
                                    </li>
                                    <li class="mb-3 nav-item"><a class="nav-link" href="#">Sneakers &
                                            Athletic</a>
                                    </li>
                                    <li class="mb-3 nav-item"><a class="nav-link" href="#">Shirts & Tops</a>
                                    </li>
                                    <li class="mb-3 nav-item"><a class="nav-link" href="#">Sunglasses</a>
                                    </li>
                                    <li class="mb-3 nav-item"><a class="nav-link" href="#">Bags & Wallets</a>
                                    </li>
                                    <li class="mb-3 nav-item"><a class="nav-link" href="#">Accessories</a>
                                    </li>
                                    <li class="nav-item"><a class="nav-link" href="#">Shoes</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-4 mt-6 mt-sm-0 navbar-dark">
                                <h5 class="mb-4 text-white">Features</h5>
                                <ul class="navbar-nav list-unstyled mb-0">
                                    <li class="mb-3 nav-item"><a class="nav-link"
                                            href="terms-and-conditions.html">Term Of Service</a>
                                    </li>
                                    <li class="mb-3 nav-item"><a class="nav-link" href="privacy-policy.html">Privacy
                                            Policy</a>
                                    </li>
                                    <li class="mb-3 nav-item"><a class="nav-link" href="#">Support</a>
                                    </li>
                                    <li class="mb-3 nav-item"><a class="nav-link" href="#">Shipping &
                                            Returns</a>
                                    </li>
                                    <li class="mb-3 nav-item"><a class="nav-link" href="#">Careers</a>
                                    </li>
                                    <li class="nav-item"><a class="nav-link" href="#">Our Story</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-3 mt-6 mt-lg-0">
                        <div class="d-flex mb-3">
                            <div class="me-2"> <i class="las la-map ic-2x text-primary"></i>
                            </div>
                            <div>
                                <h6 class="mb-1 text-light">Store address</h6>
                                <p class="mb-0 text-muted">423B, Road Wordwide Country, Africa</p>
                            </div>
                        </div>
                        <div class="d-flex mb-3">
                            <div class="me-2"> <i class="las la-envelope ic-2x text-primary"></i>
                            </div>
                            <div>
                                <h6 class="mb-1 text-light">Email Us</h6>
                                <a class="text-muted" href="mailto:themeht23@gmail.com"> themeht23@gmail.com</a>
                            </div>
                        </div>
                        <div class="d-flex mb-3">
                            <div class="me-2"> <i class="las la-mobile ic-2x text-primary"></i>
                            </div>
                            <div>
                                <h6 class="mb-1 text-light">Phone Number</h6>
                                <a class="text-muted" href="tel:+912345678900">+91-234-567-8900</a>
                            </div>
                        </div>
                        <div class="d-flex">
                            <div class="me-2"> <i class="las la-clock ic-2x text-primary"></i>
                            </div>
                            <div>
                                <h6 class="mb-1 text-light">Working Hours</h6>
                                <span class="text-muted">Mon - Fri: 10AM - 7PM</span>
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="my-8">
                <div class="row text-muted align-items-center">
                    <div class="col-md-7">Copyright ©2024 All rights reserved</u>
                    </div>
                    <div class="col-md-5 text-md-end mt-3 mt-md-0">
                        <ul class="list-inline mb-0">
                            <li class="list-inline-item">
                                <a href="#">
                                    {{-- <img class="img-fluid" src="assets/images/pay-icon/01.png" alt=""> --}}
                                    <img class="img-fluid" src="{{ asset('assets/images/pay-icon/01.png') }}"
                                        alt="">

                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#">
                                    <img class="img-fluid" src="assets/images/pay-icon/02.png" alt="">
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#">
                                    <img class="img-fluid" src="assets/images/pay-icon/03.png" alt="">
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#">
                                    <img class="img-fluid" src="assets/images/pay-icon/04.png" alt="">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>

        <!--footer end-->

    </div>

    <!-- page wrapper end -->


    <!--back-to-top start-->

    <div class="scroll-top"><a class="smoothscroll" href="#top"><i class="las la-angle-up"></i></a></div>

    <!--back-to-top end-->

    <!-- inject js start -->

    {{-- <script src="assets/js/theme-plugin.js"></script>
<script src="assets/js/theme-script.js"></script> --}}

    <script src="{{ asset('assets/js/theme-plugin.js') }}"></script>
    <script src="{{ asset('assets/js/theme-script.js') }}"></script>
    <script>
        const tooltipTriggerList = document.querySelectorAll('[data-bs-toggle="tooltip"]')
        const tooltipList = [...tooltipTriggerList].map(tooltipTriggerEl => new bootstrap.Tooltip(tooltipTriggerEl))
    </script>

    <!-- inject js end -->

</body>

</html>
