@extends('landingpage.viewallMain') {{-- Use your main layout --}}


@section('main-content')
    <div class="col-lg-10 content-right">
        <div class="d-flex justify-content-between mb-4">
            <h3>Sales Agent Details</h3>
        </div>
        <div class="d-md-flex product-details">
            <div class="d-flex justify-content-center me-3">
                <div class="c-img mb-3">

                    <img src="{{ $salesagent->image ? asset('uploads/' . $salesagent->image) : asset('assets/images/thumbnail/02.jpg') }}" alt="">

                    <div class="vip">
                        <img src="{{ asset('assets/images/luxury-vip-badge.webp') }}" alt="">
                    </div>
                </div>
            </div>
            <div class="d-flex">
                <div class="me-4">
                    <h5 class="link-title mb-0">Name: {{ $salesagent->user->first_name }} {{ $salesagent->user->last_name }}
                    </h5>
                    <p class="link-title mb-0">Address: {{ $salesagent->location }}, {{ $salesagent->country }}</p>
                    <p class="link-title mb-0">Business Category: {{ $salesagent->business_category }}</p>
                    <p class="link-title mb-0">Contact: {{ $salesagent->user->phone }}</p>
                    <p class="link-title mb-0">Zone of interest: </p>
                    <div class="mt-3">
                        <p class="link-title mb-0">Location: <img src="{{ asset('assets/images/china-flag.jpg') }}"
                                style="width: 30px;" alt=""> China</p>
                    </div>
                </div>
            </div>
        </div>
        {{-- <div class="chat">
            <a href="{{ route('supplier.personalMessage', ['userId' => $salesagent->user_id]) }}">
                <button class="btn btn-success">Chat</button>
            </a>
        </div> --}}
        <div class="about mt-5">
            <h4>About Me</h4>
         
        </div>
    </div>
@endsection
