@extends('landingpage.viewallMain') {{-- Use your main layout --}}

@section('main-content')
    <section class="p-0 mt-5">
        <div class="container">
            <div class="row mb-3">
                <div class="col-12">
                    <div class="d-flex justify-content-between">
                        <div>
                            <h4 class="mb-0">Recommended salesagents</h4>
                        </div>
                    </div>
                </div>
            </div>


         
            <div class="col-12">
                <div class="d-flex flex-wrap align-items-center text-center bg-white shadow">
                    @foreach ($salesagents as $salesagent)
                        <div class="clients-logo">
                            <a href="{{ route('supplier.salesagent.showHome', $salesagent->id) }}">

                            <img class="img-fluid" src="{{ asset('uploads/' . $salesagent->image) }}"
                                alt="">
                            <div class="text mt-3">

                                <p class="mb-0">
                                    <img src="{{ asset('assets/images/china-flag.jpg') }}"
                                        style="width: 25px;" alt="">
                                    {{ $salesagent->company }}
                                </p>
                                </a>
                                <div class="d-flex justify-content-center mt-3 categ">

                                    <span class="mb-0"> {{ $salesagent->business_category }}</span>
                                </div>

                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        
            <div class="row">
                <div class="col-12">
                    {{ $salesagents->links() }} {{-- Pagination links --}}
                </div>
            </div>
        </div>
    </section>
@endsection
