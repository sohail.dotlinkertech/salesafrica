@extends('landingpage.viewallMain') {{-- Use your main layout --}}
@section('main-content')



        <!--body content start-->

        <div class="page-content">

            <!--blog start-->

            <section>
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <!-- Blog Card -->
                            <div class="card border-0 bg-transparent">
                                <div class="position-relative rounded overflow-hidden">
                                    <div
                                        class="position-absolute z-index-1 bottom-0 bg-white text-primary shadow-primary text-center py-1 px-2 rounded ms-3 mb-3">
                                        15 Mar</div>
                                    <img class="card-img-top hover-zoom" src="assets/images/blog/02.jpg" alt="Image">
                                </div>
                                <div class="card-body pt-5 px-0">
                                    <h2>
                                        Spring summer fashion trends
                                    </h2>
                                    <p>lights dominion divide years for fourth have don't stars is that saying.
                                        serspiciatis unde omnis iste natus error. The expert team at Brightscout
                                        specializes in building innovative technology solutions for enterprises. Our
                                        products and services unleash new levels of productivity, enhance collaboration
                                        amongst team members, and streamline large-scale communication. We utilize a
                                        powerful mix of exquisite design and innovative technology to resolve the issues
                                        that hold back many companies today.</p>
                                </div>
                                <p>Consequat dolor sit amet, consectetur adipiscing elit. Maecenas lobortis quam id
                                    lectus aliquet euismod. Ut sagittis et augue dui gravida Cras ultricies ligula sed
                                    magna dictum porta, Sed ut perspiciatis unde omnis iste natus error sit voluptat
                                    erci tation ullamco laboris nisi ut aliq uip.eiu smod tempor the incidi dunt ut
                                    labore et dolore magna aliqua. Phasellus eget purus id felis dignissim convallis
                                    Suspendisse et augue dui gravida Cras ultricies ligula sed magna dictum porta, Sed
                                    ut perspiciatis unde omnis iste natus error sit voluptat erci tation ullamco laboris
                                    nisi ut aliq uip.eiu smod tempor the incidi dunt ut labore et dolore magna aliqua.
                                    Ut atenim ad minim veniam, quis nostrud exerci tation abore et dolore magna aliqua.
                                    Uhbt atenim</p>
                            </div>
                            <!-- End Blog Card -->
                        </div>
                    </div>
                </div>
            </section>

            <!--blog end-->

        </div>

        <!--body content end-->


      @endsection