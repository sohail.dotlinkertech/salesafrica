@extends('landingpage.viewallMain') {{-- Use your main layout --}}

@section('main-content')
    <section class="p-0 mt-5">
        <div class="container">
            <div class="row mb-3">
                <div class="col-12">
                    <div class="d-flex justify-content-between">
                        <div>
                            <h4 class="mb-0">Recommended Suppliers</h4>
                        </div>
                    </div>
                </div>
            </div>
            {{-- <div class="row mb-4">
                <div class="col-12  d-flex justify-content-end">
                    <form method="GET" action="{{ route('suppliers.view_all') }}">
                        <div class="input-group">
                            <select name="category" class="form-control">
                                <option value="">All Categories</option>
                                <option value="Furniture">Furniture</option>
                                <option value="Building Material">Building Material</option>
                                <option value="Bed">Bed</option>
                            </select>
                            <button type="submit" class="btn btn-primary">Filter</button>
                        </div>
                    </form>
                </div>
            </div> --}}

            <div class="row">
                <div class="col-12">
                    <div class="d-flex flex-wrap align-items-center text-center bg-white shadow">
                        @foreach ($suppliers as $supplier)
                            <div class="clients-logo">
                                <a href="{{ route('supplier.supplier.showHome', $supplier->id) }}">

                                <img class="img-fluid" src="{{ asset('uploads/' . $supplier->image) }}" alt="">
                                <div class="text mt-3">

                                    <p class="mb-0">
                                        <img src="{{ asset('assets/images/china-flag.jpg') }}" style="width: 25px;"
                                            alt="">
                                        {{ $supplier->company }}
                                    </p>
                                    </a>
                                    <div class="d-flex justify-content-center mt-3 categ">

                                        <span class="mb-0"> {{ $supplier->business_category }}</span>
                                    </div>

                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    {{ $suppliers->links() }} {{-- Pagination links --}}
                </div>
            </div>
        </div>
    </section>
@endsection
