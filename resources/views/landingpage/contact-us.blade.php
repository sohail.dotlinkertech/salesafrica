<!DOCTYPE html>
<html lang="en">
<head>

<!-- meta tags -->
<meta charset="utf-8">
<meta name="keywords" content="bootstrap 5, premium, multipurpose, sass, scss, saas, eCommerce, Shop, Fashion" />
<meta name="description" content="Bootstrap 5 Landing Page Template" />
<meta name="author" content="www.themeht.com" />
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Title -->
<title>SalesAfrica</title>

<!-- Favicon Icon -->
<link rel="shortcut icon" href="{{ asset('assets/images/favicon.ico') }}" />

<!-- inject css start -->
<link href="{{ asset('assets/css/theme-plugin.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/css/theme.min.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/css/custom.css') }}" rel="stylesheet" />

<link href="https://cdnjs.cloudflare.com/ajax/libs/line-awesome/1.3.0/line-awesome/css/line-awesome.min.css
" rel="stylesheet" />
<!-- inject css end -->

</head>

<body>

<!-- page wrapper start -->

<div class="page-wrapper">
  
<!-- preloader start -->

<div id="ht-preloader">
  <div class="loader clear-loader">
    <img class="img-fluid" src="assets/images/loader.gif" alt="">
  </div>
</div>

<!-- preloader end -->


<!--header start-->

<header class="site-header">
  <div id="header-wrap" class="shadow-sm py-md-5 py-3 fixed-top">
    <div class="container">
      <div class="row">
        <!--menu start-->
        <div class="col">
          <nav class="navbar navbar-expand-lg navbar-light">
            <!-- <a class="navbar-brand logo d-lg-none" href="index.html">
              <img class="img-fluid" src="assets/images/logo.png" alt="">
            </a> -->
            <a class="navbar-brand logo fs25 me-4" href="{{ route('index') }}">
              Sales<span class="text-primary">Africa</span>
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
              <ul class="navbar-nav">
                <li class="nav-item"> <a class="nav-link" href="{{ route('index') }}">Home</a>
                </li>
                <li class="nav-item"> <a class="nav-link" href="#">About</a>
                </li>
                <li class="nav-item"> <a class="nav-link" href="{{route('contactUs')}}">Contact</a>
                </li>
              </ul>
            </div>
            <div class="right-nav align-items-center d-flex justify-content-end"> 
              <span class="me-2">
                <a href="{{ route('login') }}">Join Now</a>
              </span>
            </div>
          </nav>
        </div>
        <!--menu end-->
      </div>
    </div>
  </div>
</header>

<!--header end-->


<!--body content start-->

<div class="page-content">

<section>
  <div class="container">
    <div class="row mb-5">
      <div class="col-lg-8">
        <div class="mb-5"> 
          <h6 class="text-primary mb-1">— Contact US</h6>
          <h2 class="mb-0">We’d love to hear from you.</h2>
        </div>
        <form id="contact-form" class="row" method="post" action="https://themeht.com/template/ekocart/html/php/contact.php">
            <div class="messages"></div>
            <div class="form-group col-md-6">
              <label class="mb-1">First Name <span class="text-danger">*</span></label>
              <input id="form_name" type="text" name="name" class="form-control" placeholder="First Name" required="required" data-error="Name is required.">
              <div class="help-block with-errors"></div>
            </div>
            <div class="form-group col-md-6">
              <label class="mb-1">Last Name <span class="text-danger">*</span></label>
              <input id="form_name1" type="text" name="name" class="form-control" placeholder="Last Name" required="required" data-error="Name is required.">
              <div class="help-block with-errors"></div>
            </div>
            <div class="form-group col-md-6">
              <label class="mb-1">Email Address <span class="text-danger">*</span></label>
              <input id="form_email" type="email" name="email" class="form-control" placeholder="Email" required="required" data-error="Valid email is required.">
              <div class="help-block with-errors"></div>
            </div>
            <div class="form-group col-md-6">
              <label class="mb-1">Phone Number <span class="text-danger">*</span></label>
              <input id="form_phone" type="tel" name="phone" class="form-control" placeholder="Phone" required="required" data-error="Phone is required">
              <div class="help-block with-errors"></div>
            </div>
            <div class="form-group col-md-12">
              <label class="mb-1">Message <span class="text-danger">*</span></label>
              <textarea id="form_message" name="message" class="form-control h-auto" placeholder="Message" rows="4" required="required" data-error="Please,leave us a message."></textarea>
              <div class="help-block with-errors"></div>
            </div>
            <div class="col-md-12 mt-4">
              <button class="btn btn-primary btn-animated"><span>Send Messages</span>
              </button>
            </div>
          </form>
      </div>
      <div class="col-lg-4 mt-6 mt-lg-0">
        <div class="shadow-sm rounded p-5">
          <div class="mb-5"> 
          <h6 class="text-primary mb-1">— Contact Info</h6>
          <h4 class="mb-0">We Are here To help You</h4>
        </div>
        <div class="d-flex mb-3">
          <div class="me-2"> <i class="las la-map ic-2x text-primary"></i>
          </div>
          <div>
            <h6 class="mb-1 text-dark">Store address</h6>
            <p class="mb-0 text-muted">423B, Road Wordwide Country, USA</p>
          </div>
        </div>
        <div class="d-flex mb-3">
          <div class="me-2"> <i class="las la-envelope ic-2x text-primary"></i>
          </div>
          <div>
            <h6 class="mb-1 text-dark">Email Us</h6>
            <a class="text-muted" href="mailto:themeht23@gmail.com"> themeht23@gmail.com</a>
          </div>
        </div>
        <div class="d-flex mb-3">
          <div class="me-2"> <i class="las la-mobile ic-2x text-primary"></i>
          </div>
          <div>
            <h6 class="mb-1 text-dark">Phone Number</h6>
            <a class="text-muted" href="tel:+912345678900">+91-234-567-8900</a>
          </div>
        </div>
        <div class="d-flex mb-5">
          <div class="me-2"> <i class="las la-clock ic-2x text-primary"></i>
          </div>
          <div>
            <h6 class="mb-1 text-dark">Working Hours</h6>
            <span class="text-muted">Mon - Fri: 10AM - 7PM</span>
          </div>
        </div>
        <ul class="list-inline">
          <li class="list-inline-item"><a class="bg-white shadow-sm rounded p-2" href="#"><i class="la la-facebook"></i></a>
          </li>
          <li class="list-inline-item"><a class="bg-white shadow-sm rounded p-2" href="#"><i class="la la-dribbble"></i></a>
          </li>
          <li class="list-inline-item"><a class="bg-white shadow-sm rounded p-2" href="#"><i class="la la-instagram"></i></a>
          </li>
          <li class="list-inline-item"><a class="bg-white shadow-sm rounded p-2" href="#"><i class="la la-twitter"></i></a>
          </li>
          <li class="list-inline-item"><a class="bg-white shadow-sm rounded p-2" href="#"><i class="la la-linkedin"></i></a>
          </li>
        </ul>
        </div>
      </div>
    </div>
    
  </div>
</section>

<section class="pt-0">
  <div class="container">
    <hr class="mt-0 mb-10">
    <div class="row justify-content-center text-center mb-5">
      <div class="col-lg-8">        
        <div> 
          <h6 class="text-primary mb-1">— Easy to Find</h6>
          <h2 class="mb-0">Our Store Location</h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <div class="map" style="height: 500px;">
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3151.840108181602!2d144.95373631539215!3d-37.8172139797516!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6ad65d4c2b349649%3A0xb6899234e561db11!2sEnvato!5e0!3m2!1sen!2sin!4v1497005461921" allowfullscreen="" class="w-100 h-100 border-0"></iframe>
        </div>
      </div>
    </div>
  </div>
</section>

</div>

<!--body content end--> 


<!--footer start-->

<footer class="py-11 bg-dark">
  <div class="container">
    <div class="row">
      <div class="col-12 col-lg-3"> <a class="footer-logo text-white h2 mb-0" href="index.html">
              Sales<span class="text-primary">Africa</span>
            </a>
        <p class="my-3 text-muted">Lorem ipsum dolor sit amet consectetur adipisicing elit. Id dolores et quidem sit similique? Iste, ut! Mollitia vero laborum enim.</p>
        <ul class="list-inline mb-0">
          <li class="list-inline-item"><a class="text-light ic-2x" href="#"><i class="la la-facebook"></i></a>
          </li>
          <li class="list-inline-item"><a class="text-light ic-2x" href="#"><i class="la la-dribbble"></i></a>
          </li>
          <li class="list-inline-item"><a class="text-light ic-2x" href="#"><i class="la la-instagram"></i></a>
          </li>
          <li class="list-inline-item"><a class="text-light ic-2x" href="#"><i class="la la-twitter"></i></a>
          </li>
          <li class="list-inline-item"><a class="text-light ic-2x" href="#"><i class="la la-linkedin"></i></a>
          </li>
        </ul>
      </div>
      <div class="col-12 col-lg-6 mt-6 mt-lg-0">
        <div class="row">
          <div class="col-12 col-sm-4 navbar-dark">
            <h5 class="mb-4 text-white">Quick Links</h5>
            <ul class="navbar-nav list-unstyled mb-0">
              <li class="mb-3 nav-item"><a class="nav-link" href="index.html">Home</a>
              </li>
              <li class="mb-3 nav-item"><a class="nav-link" href="about-us-1.html">About</a>
              </li>
              <li class="mb-3 nav-item"><a class="nav-link" href="faq.html">Faq</a>
              </li>
              <li class="nav-item"><a class="nav-link" href="contact-us.html">Contact Us</a>
              </li>
            </ul>
          </div>
          <div class="col-12 col-sm-4 mt-6 mt-sm-0 navbar-dark">
            <h5 class="mb-4 text-white">Top Products</h5>
            <ul class="navbar-nav list-unstyled mb-0">
              <li class="mb-3 nav-item"><a class="nav-link" href="#">T-Shirts</a>
              </li>
              <li class="mb-3 nav-item"><a class="nav-link" href="#">Sneakers & Athletic</a>
              </li>
              <li class="mb-3 nav-item"><a class="nav-link" href="#">Shirts & Tops</a>
              </li>
              <li class="mb-3 nav-item"><a class="nav-link" href="#">Sunglasses</a>
              </li>
              <li class="mb-3 nav-item"><a class="nav-link" href="#">Bags & Wallets</a>
              </li>
              <li class="mb-3 nav-item"><a class="nav-link" href="#">Accessories</a>
              </li>
              <li class="nav-item"><a class="nav-link" href="#">Shoes</a>
              </li>
            </ul>
          </div>
          <div class="col-12 col-sm-4 mt-6 mt-sm-0 navbar-dark">
            <h5 class="mb-4 text-white">Features</h5>
            <ul class="navbar-nav list-unstyled mb-0">
              <li class="mb-3 nav-item"><a class="nav-link" href="terms-and-conditions.html">Term Of Service</a>
              </li>
              <li class="mb-3 nav-item"><a class="nav-link" href="privacy-policy.html">Privacy Policy</a>
              </li>
              <li class="mb-3 nav-item"><a class="nav-link" href="#">Support</a>
              </li>
              <li class="mb-3 nav-item"><a class="nav-link" href="#">Shipping & Returns</a>
              </li>
              <li class="mb-3 nav-item"><a class="nav-link" href="#">Careers</a>
              </li>
              <li class="nav-item"><a class="nav-link" href="#">Our Story</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-12 col-lg-3 mt-6 mt-lg-0">
        <div class="d-flex mb-3">
          <div class="me-2"> <i class="las la-map ic-2x text-primary"></i>
          </div>
          <div>
            <h6 class="mb-1 text-light">Store address</h6>
            <p class="mb-0 text-muted">423B, Road Wordwide Country, Africa</p>
          </div>
        </div>
        <div class="d-flex mb-3">
          <div class="me-2"> <i class="las la-envelope ic-2x text-primary"></i>
          </div>
          <div>
            <h6 class="mb-1 text-light">Email Us</h6>
            <a class="text-muted" href="mailto:themeht23@gmail.com"> themeht23@gmail.com</a>
          </div>
        </div>
        <div class="d-flex mb-3">
          <div class="me-2"> <i class="las la-mobile ic-2x text-primary"></i>
          </div>
          <div>
            <h6 class="mb-1 text-light">Phone Number</h6>
            <a class="text-muted" href="tel:+912345678900">+91-234-567-8900</a>
          </div>
        </div>
        <div class="d-flex">
          <div class="me-2"> <i class="las la-clock ic-2x text-primary"></i>
          </div>
          <div>
            <h6 class="mb-1 text-light">Working Hours</h6>
            <span class="text-muted">Mon - Fri: 10AM - 7PM</span>
          </div>
        </div>
      </div>
    </div>
    <hr class="my-8">
    <div class="row text-muted align-items-center">
      <div class="col-md-7">Copyright ©2024 All rights reserved</u>
      </div>
      <div class="col-md-5 text-md-end mt-3 mt-md-0">
        <ul class="list-inline mb-0">
          <li class="list-inline-item">
            <a href="#">
              <img class="img-fluid" src="assets/images/pay-icon/01.png" alt="">
            </a>
          </li>
          <li class="list-inline-item">
            <a href="#">
              <img class="img-fluid" src="assets/images/pay-icon/02.png" alt="">
            </a>
          </li>
          <li class="list-inline-item">
            <a href="#">
              <img class="img-fluid" src="assets/images/pay-icon/03.png" alt="">
            </a>
          </li>
          <li class="list-inline-item">
            <a href="#">
              <img class="img-fluid" src="assets/images/pay-icon/04.png" alt="">
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</footer>

<!--footer end-->

</div>

<!-- page wrapper end -->

 
<!--back-to-top start-->

<div class="scroll-top"><a class="smoothscroll" href="#top"><i class="las la-angle-up"></i></a></div>

<!--back-to-top end-->

<!-- inject js start -->
<script src="{{ asset('assets/js/theme-plugin.js') }}"></script>
<script src="{{ asset('assets/js/theme-script.js') }}"></script><script>

<!-- inject js end -->

</body>
</html>
