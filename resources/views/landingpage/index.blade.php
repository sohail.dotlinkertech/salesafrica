<!DOCTYPE html>
<html lang="en">

<head>

    <!-- meta tags -->
    <meta charset="utf-8">
    <meta name="keywords" content="bootstrap 5, premium, multipurpose, sass, scss, saas, eCommerce, Shop, Fashion" />
    <meta name="description" content="Bootstrap 5 Landing Page Template" />
    <meta name="author" content="www.themeht.com" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Title -->
    <title>SalesAfrica</title>

    <!-- Favicon Icon -->
    <link rel="shortcut icon" href="{{ asset('assets/images/favicon.ico') }}" />

    <!-- inject css start -->
    <link href="{{ asset('assets/css/theme-plugin.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/theme.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/custom.css') }}" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/line-awesome/1.3.0/line-awesome/css/line-awesome.min.css
"
        rel="stylesheet" />
    <!-- inject css end -->

</head>

<body>

    <!-- page wrapper start -->

    <div class="page-wrapper">

        <!-- preloader start -->

        <div id="ht-preloader">
            <div class="loader clear-loader">
                <img class="img-fluid" src="{{ asset('assets/images/loader.gif') }}" alt="">
            </div>
        </div>

        <!-- preloader end -->


        <!--header start-->

        <header class="site-header">
            <div id="header-wrap" class="shadow-sm py-md-5 py-3 fixed-top">
                <div class="container">
                    <div class="row">
                        <!--menu start-->
                        <div class="col">
                            <nav class="navbar navbar-expand-lg navbar-light">
                              
                                <a class="navbar-brand logo fs25 me-4" href="index.html">
                                    Sales<span class="text-primary">Africa</span>
                                </a>
                                <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                                    <span class="navbar-toggler-icon"></span>
                                </button>
                                <div class="collapse navbar-collapse" id="navbarNav">
                                    <ul class="navbar-nav">
                                        <li class="nav-item"> <a class="nav-link" href="{{ route('index') }}">Home</a>
                                        </li>
                                        <li class="nav-item"> <a class="nav-link" href="#">About</a>
                                        </li>
                                        <li class="nav-item"> <a class="nav-link"
                                                href="{{ route('contactUs') }}">Contact</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="right-nav align-items-center d-flex justify-content-end">
                                    <span class="me-2">
                                        <a href="{{ route('login') }}">Join Now</a>
                                    </span>
                                </div>
                            </nav>
                        </div>
                        <!--menu end-->
                    </div>
                </div>
            </div>
        </header>

        <!--header end-->


        <!--hero section start-->

        <section class="banner pos-r p-0">
            <div class="banner-slider owl-carousel no-pb owl-2" data-dots="false" data-nav="true">
                <div class="item bg-pos-rt" data-bg-img="assets/images/bg/01.jpg">
                    <div class="container h-100">
                        <div class="row h-100 align-items-center">
                            <div class="col-lg-7 col-md-12 custom-py-1 position-relative z-index-1">
                                <!-- <h6 class="font-w-6 text-primary animated3">Welcome Ekocart</h6> -->
                                <h1 class="mb-4 animated3">A New Online<br> Shop experience</h1>
                                <div class="animated3">
                                    <a class="btn btn-primary btn-animated" href="#">Shop Now</a>
                                </div>
                                <div class="hero-circle animated4"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item bg-pos-rt" data-bg-img="assets/images/bg/02.jpg">
                    <div class="container h-100">
                        <div class="row h-100 align-items-center">
                            <div class="col-lg-7 col-md-12 custom-py-1 position-relative z-index-1">
                                <h6 class="font-w-6 text-primary animated3">2020 Latest Style</h6>
                                <h1 class="mb-4 animated3">Trending Men's Collection</h1>
                                <div class="animated3">
                                    <a class="btn btn-primary btn-animated" href="#">View Collection</a>
                                </div>
                                <div class="hero-circle animated4"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!--hero section end-->


        <!--body content start-->

        <div class="page-content">


            <!--product start-->

            <section>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8">
                            <h4 class="mb-4">
                                <label for="">New Opportunities</label>
                            </h4>
                            <div class="p-box mb-3">
                                <div class="d-md-flex product-details">
                                    <div class="d-flex justify-content-center me-3">
                                        <div class="c-img mb-3">
                                            <img src="assets/images/furniture-company.jpg" alt="">
                                            <div class="vip">
                                                <img src="assets/images/luxury-vip-badge.webp" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex">
                                        <div class="me-4">
                                            <h5 class="link-title mb-0">Chinese Office furniture manufacturer seeking
                                                distributor in africa</h5>
                                            <p class="link-title mb-0">Opportunities: Sales agent, full-time markter,
                                                distributor</p>
                                            <p class="link-title mb-0">Business Category: Furniture, Building Material
                                            </p>
                                            <p class="link-title mb-0">Products: Sofa wooden, Bed, Wardrobe, Dining
                                                table</p>
                                            <p class="link-title mb-0">Zone of interest: Nigeria, Ghana, south africa
                                            </p>
                                            <div class="mt-3">
                                                <h6 class="link-title mb-0">
                                                    <a href="company-details.html" class="text-black">Supplier:
                                                        Shandong xaion xinhua office furniture Co., Ltd.</a>
                                                </h6>
                                                <p class="link-title mb-0">Location: <img
                                                        src="assets/images/china-flag.jpg" style="width: 30px;"
                                                        alt=""> China</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-end align-items-center">
                                    <span data-bs-toggle="tooltip" data-bs-placement="top"
                                        data-bs-title="Add to shortlist">
                                        <i class="lar la-heart text-primary me-1 cursor" style="font-size: 27px;"></i>
                                    </span>
                                    <a href="full-description.html" class="me-2">
                                        <button class="btn btn-sm btn-primary">
                                            View Full description
                                        </button>
                                    </a>
                                    <a href="#">
                                        <button class="btn btn-sm btn-primary">
                                            Apply
                                        </button>
                                    </a>
                                </div>
                            </div>
                            <div class="p-box mb-3">
                                <div class="d-md-flex product-details">
                                    <div class="d-flex justify-content-center me-3">
                                        <div class="c-img mb-3">
                                            <img src="assets/images/furniture-company.jpg" alt="">
                                            <div class="vip">
                                                <img src="assets/images/luxury-vip-badge.webp" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex">
                                        <div class="me-4">
                                            <h5 class="link-title mb-0">Chinese Office furniture manufacturer seeking
                                                distributor in africa</h5>
                                            <p class="link-title mb-0">Opportunities: Sales agent, full-time markter,
                                                distributor</p>
                                            <p class="link-title mb-0">Business Category: Furniture, Building Material
                                            </p>
                                            <p class="link-title mb-0">Products: Sofa wooden, Bed, Wardrobe, Dining
                                                table</p>
                                            <p class="link-title mb-0">Zone of interest: Nigeria, Ghana, south africa
                                            </p>
                                            <div class="mt-3">
                                                <h6 class="link-title mb-0">
                                                    <a href="company-details.html" class="text-black">Supplier:
                                                        Shandong xaion xinhua office furniture Co., Ltd.</a>
                                                </h6>
                                                <p class="link-title mb-0">Location: <img
                                                        src="assets/images/china-flag.jpg" style="width: 30px;"
                                                        alt=""> China</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-end align-items-center">
                                    <span data-bs-toggle="tooltip" data-bs-placement="top"
                                        data-bs-title="Add to shortlist">
                                        <i class="lar la-heart text-primary me-1 cursor" style="font-size: 27px;"></i>
                                    </span>
                                    <a href="full-description.html" class="me-2">
                                        <button class="btn btn-sm btn-primary">
                                            View Full description
                                        </button>
                                    </a>
                                    <a href="#">
                                        <button class="btn btn-sm btn-primary">
                                            Apply
                                        </button>
                                    </a>
                                </div>
                            </div>
                            <div class="p-box mb-3">
                                <div class="d-md-flex product-details">
                                    <div class="d-flex justify-content-center me-3">
                                        <div class="c-img mb-3">
                                            <img src="assets/images/furniture-company.jpg" alt="">
                                            <div class="vip">
                                                <img src="assets/images/luxury-vip-badge.webp" alt="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="d-flex">
                                        <div class="me-4">
                                            <h5 class="link-title mb-0">Chinese Office furniture manufacturer seeking
                                                distributor in africa</h5>
                                            <p class="link-title mb-0">Opportunities: Sales agent, full-time markter,
                                                distributor</p>
                                            <p class="link-title mb-0">Business Category: Furniture, Building Material
                                            </p>
                                            <p class="link-title mb-0">Products: Sofa wooden, Bed, Wardrobe, Dining
                                                table</p>
                                            <p class="link-title mb-0">Zone of interest: Nigeria, Ghana, south africa
                                            </p>
                                            <div class="mt-3">
                                                <h6 class="link-title mb-0">
                                                    <a href="company-details.html" class="text-black">Supplier:
                                                        Shandong xaion xinhua office furniture Co., Ltd.</a>
                                                </h6>
                                                <p class="link-title mb-0">Location: <img
                                                        src="assets/images/china-flag.jpg" style="width: 30px;"
                                                        alt=""> China</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-end align-items-center">
                                    <span data-bs-toggle="tooltip" data-bs-placement="top"
                                        data-bs-title="Add to shortlist">
                                        <i class="lar la-heart text-primary me-1 cursor" style="font-size: 27px;"></i>
                                    </span>
                                    <a href="full-description.html" class="me-2">
                                        <button class="btn btn-sm btn-primary">
                                            View Full description
                                        </button>
                                    </a>
                                    <a href="#">
                                        <button class="btn btn-sm btn-primary">
                                            Apply
                                        </button>
                                    </a>
                                </div>
                            </div>

                            <div class="text-center">
                                <a href="#">Check More </a>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <h4 class="mb-4">
                                <label for="">Search Opportunities</label>
                            </h4>
                            <div class="selects">
                                <div class="mb-3">
                                    <!-- <input type="text" class="form-control" placeholder="search here..."> -->
                                    <div class="input-group mb-3 border rounded">
                                        <input class="form-control border-0 border-start" type="search"
                                            placeholder="Enter Your Keyword" aria-label="Search">
                                        <button class="btn btn-primary text-white" type="submit"><i
                                                class="las la-search"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="mb-3">
                                    <label for="">Select country</label>
                                    <select class="form-select" aria-label="Default select example">
                                        <option value="1">China</option>
                                        <option value="2">Africa</option>
                                        <option value="3">USA</option>
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <label for="">Select Role</label>
                                    <select class="form-select" aria-label="Default select example">
                                        <option value="1">Furniture</option>
                                        <option value="2">Two</option>
                                        <option value="3">Three</option>
                                    </select>
                                </div>
                                <div class="mb-3">
                                    <label for="">Business category</label>
                                    <select class="form-select" aria-label="Default select example">
                                        <option value="1">One</option>
                                        <option value="2">Two</option>
                                        <option value="3">Three</option>
                                    </select>
                                </div>
                                <div class="text-center">
                                    <button class="btn btn-primary">
                                        Search
                                    </button>
                                </div>
                            </div>
                            <div class="forms shadow mt-5">
                                <div class="mb-3">
                                    <label for="" class="form-label">Account</label>
                                    <input type="email" class="form-control" placeholder="name@example.com">
                                </div>
                                <div class="mb-3">
                                    <label for="" class="form-label">Password</label>
                                    <input type="email" class="form-control" placeholder="Enter your password">
                                </div>
                                <div class="text-center">
                                    <button class="btn btn-primary">
                                        Sign In
                                    </button>
                                    <button class="btn btn-primary">
                                        Sign Up
                                    </button>
                                </div>
                            </div>
                            <div>
                                <h4 class="mb-0 mt-3">Brand News</h4>
                                <div class=" brand-news shadow mt-2 p-3">
                                    <div class="row">
                                        <div class="col-8">
                                            <p>1. Global Markets Rally Amid Economic</p>
                                            <p>2. New Tech Innovations Unveiled at Major</p>
                                            <p>3. Climate Change Summit Highlights</p>
                                            <p>4. Breakthrough in Medical Research</p>
                                            <p>5. Historic Peace Agreement Signed in Conflict Zone</p>
                                        </div>
                                        <div class="col-4 text-end">
                                            <img src="assets/images/furniture-company.jpg" class="news-img"
                                                alt="">
                                            <img src="assets/images/furniture-company.jpg" class="news-img"
                                                alt="">
                                        </div>
                                    </div>
                                    <div class="text-center">
                                        <small><a href="#">Read More</a></small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!--product end-->

            <!--product ad start-->

            <section class="p-0">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="position-relative rounded overflow-hidden">
                                <!-- Background -->
                                <img class="img-fluid hover-zoom ad-img" src="assets/images/product-ad/01.jpg"
                                    alt="">
                                <!-- Body -->
                                <div class="position-absolute top-50 translate-middle-y ps-5">
                                    <h6 class="text-dark">Woman Party Collection</h6>
                                    <!-- Heading -->
                                    <h3>#Trendy Arrived Item<br> <span class="text-primary font-w-7">Off 50%</span>
                                    </h3>
                                    <!-- Link --> <a class="btn btn-sm btn-primary btn-animated" href="#">Shop
                                        Now
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!--product ad end-->


            <!--multi sec start-->
            <section class="p-0 mt-5">
                <div class="container">
                    <div class="row  mb-3">
                        <div class="col-12">
                            <div class="d-flex justify-content-between">
                                <div>
                                    <h4 class="mb-0">Recommended Sales Agents</h4>
                                </div>
                                <div class="text-end">
                                    <a href="{{ route('suppliers.view_all') }}">View All</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="d-flex flex-wrap align-items-center text-center bg-white shadow">
                                @foreach ($salesagents as $salesagent)
                                    <div class="clients-logo">
                                        <a href="{{ route('supplier.salesagent.showHome', $salesagent->id) }}">

                                        <img class="img-fluid" src="{{ asset('uploads/' . $salesagent->image) }}"
                                            alt="">
                                        <div class="text mt-3">

                                                <p class="mb-0">
                                                    <img src="{{ asset('assets/images/china-flag.jpg') }}"
                                                        style="width: 25px;" alt="">
                                                    {{ $salesagent->company }}
                                                </p>
                                            </a>
                                            <div class="d-flex justify-content-center mt-3 categ">

                                                <span class="mb-0"> {{ $salesagent->business_category }}</span>
                                            </div>

                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="p-0 mt-5">
                <div class="container">
                    <div class="row  mb-3">
                        <div class="col-12">
                            <div class="d-flex justify-content-between">
                                <div>
                                    <h4 class="mb-0">Recommended Distributors</h4>
                                </div>
                                <div class="text-end">
                                    <a href="{{ route('distributors.view_all') }}">View All</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="d-flex flex-wrap align-items-center text-center bg-white shadow">



                                @foreach ($distributors as $distributor)
                                    <div class="clients-logo">
                                        <a href="{{ route('supplier.distributor.showHome', $distributor->id) }}">

                                        <img class="img-fluid" src="{{ asset('uploads/' . $distributor->image) }}"
                                            alt="">
                                        <div class="text mt-3">

                                                <p class="mb-0">
                                                    <img src="{{ asset('assets/images/china-flag.jpg') }}"
                                                        style="width: 25px;" alt="">
                                                    {{ $distributor->company }}
                                                </p>
                                            </a>
                                            <div class="d-flex justify-content-center mt-3 categ">

                                                <span class="mb-0"> {{ $distributor->business_category }}</span>
                                            </div>

                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!--product ad start-->

            <section class="p-0 mt-5">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="position-relative rounded overflow-hidden">
                                <!-- Background -->
                                <img class="img-fluid hover-zoom ad-img" src="assets/images/product-ad/07.jpg"
                                    alt="">
                                <!-- Body -->
                                <div class="position-absolute top-50 translate-middle-y ps-5">
                                    <h6 class="text-dark">2024 Collection</h6>
                                    <!-- Heading -->
                                    <h3>New Stylish Trend<br>Running Shoe</h3>
                                    <!-- Link --> <a class="btn btn-sm btn-primary btn-animated" href="#">Shop
                                        Now
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!--product ad end-->

            <section class="p-0 mt-5">
                <div class="container">
                    <div class="row  mb-3">
                        <div class="col-12">
                            <div class="d-flex justify-content-between">
                                <div>
                                    <h4 class="mb-0">Recommended Suppliers</h4>
                                </div>
                                <div class="text-end">
                                    <a href="{{ route('suppliers.view_all') }}">View All</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="d-flex flex-wrap align-items-center text-center bg-white shadow">
                            @foreach ($suppliers as $supplier)
                                <div class="clients-logo">
                                    <a href="{{ route('supplier.supplier.showHome', $supplier->id) }}">

                                    <img class="img-fluid" src="{{ asset('uploads/' . $supplier->image) }}"
                                        alt="">
                                    <div class="text mt-3">

                                            <p class="mb-0">
                                                <img src="{{ asset('assets/images/china-flag.jpg') }}"
                                                    style="width: 25px;" alt="">
                                                {{ $supplier->company }}
                                            </p>
                                        </a>
                                        <div class="d-flex justify-content-center mt-3 categ">

                                            <span class="mb-0"> {{ $supplier->business_category }}</span>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </section>

            <!--multi sec end-->


            <!--blog start-->

            <section>
                <div class="container">
                    <div class="row justify-content-center text-center mb-5">
                        <div class="col-12 col-md-10 col-lg-8">
                            <div>
                                <h6 class="text-primary mb-1">
                                    Events
                                </h6>
                                <h4 class="mb-0">Upcoming Events</h4>
                            </div>
                        </div>
                    </div>
                    <!-- / .row -->
                    <div class="row">
                        <div class="col-12 col-lg-4">
                            <!-- Blog Card -->
                            <div class="card border-0 bg-transparent">
                                <div class="position-relative rounded overflow-hidden">
                                    <div
                                        class="position-absolute z-index-1 bottom-0 bg-white text-primary shadow-primary text-center py-1 px-2 rounded ms-3 mb-3">
                                        20 Apr</div>
                                    <img class="card-img-top hover-zoom" src="assets/images/blog/01.jpg"
                                        alt="Image">
                                </div>
                                <div class="card-body px-0 pb-0">
                                    <div> <a class="d-inline-block link-title btn-link text-small me-2"
                                            href="#">Cloth,</a>
                                        <a class="d-inline-block link-title btn-link text-small"
                                            href="#">Fashion</a>
                                    </div>
                                    <h2 class="h5 font-w-5 mt-2 mb-0">
                                        <a class="link-title" href="{{ route('blog-single') }}">Spring summer fashion
                                            trends new
                                            high collection</a>
                                    </h2>
                                </div>
                                <div></div>
                            </div>
                            <!-- End Blog Card -->
                        </div>
                        <div class="col-12 col-lg-4 mt-5 mt-lg-0">
                            <!-- Blog Card -->
                            <div class="card border-0 bg-transparent">
                                <div class="position-relative rounded overflow-hidden">
                                    <div
                                        class="position-absolute z-index-1 bottom-0 bg-white text-primary shadow-primary text-center py-1 px-2 rounded ms-3 mb-3">
                                        15 Mar</div>
                                    <img class="card-img-top hover-zoom" src="assets/images/blog/02.jpg"
                                        alt="Image">
                                </div>
                                <div class="card-body px-0 pb-0">
                                    <div> <a class="d-inline-block link-title btn-link text-small me-2"
                                            href="#">Cloth,</a>
                                        <a class="d-inline-block link-title btn-link text-small"
                                            href="#">Fashion</a>
                                    </div>
                                    <h2 class="h5 font-w-5 mt-3">
                                        <a class="link-title" href="{{ route('blog-single') }}">When it’s Winter
                                            outdoors gifts
                                            but you feel like</a>
                                    </h2>
                                </div>
                                <div></div>
                            </div>
                            <!-- End Blog Card -->
                        </div>
                        <div class="col-12 col-lg-4 mt-5 mt-lg-0">
                            <!-- Blog Card -->
                            <div class="card border-0 bg-transparent">
                                <div class="position-relative rounded overflow-hidden">
                                    <div
                                        class="position-absolute z-index-1 bottom-0 bg-white text-primary shadow-primary text-center py-1 px-2 rounded ms-3 mb-3">
                                        13 Apr</div>
                                    <img class="card-img-top hover-zoom"
                                        src="{{ asset('assets/images/blog/03.jpg') }}" alt="Image">
                                </div>
                                <div class="card-body px-0 pb-0">
                                    <div> <a class="d-inline-block link-title btn-link text-small me-2"
                                            href="#">Cloth,</a>
                                        <a class="d-inline-block link-title btn-link text-small"
                                            href="#">Fashion</a>
                                    </div>
                                    <h2 class="h5 font-w-5 mt-3">
                                        <a class="link-title" href="{{ route('blog-single') }}">Whatever the will
                                            make it
                                            special For You Every</a>
                                    </h2>
                                </div>
                                <div></div>
                            </div>
                            <!-- End Blog Card -->
                        </div>
                    </div>
                    <div class="row justify-content-center text-center mt-5">
                        <div class="col"> <a class="btn btn-dark" href="blog-card.html">View All Events</a>
                        </div>
                    </div>
                </div>
            </section>

            <!--blog end-->


            <!--accordian start-->

            <section class="p-0 mb-5">
                <div class="container p-0">
                    <div class="row justify-content-center text-center mb-5">
                        <div class="col-12 col-md-10 col-lg-8">
                            <div>
                                <h6 class="text-primary mb-1">
                                    Knowledge
                                </h6>
                                <h4 class="mb-0">Knowledge base and community</h4>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="know-box">
                                <p class="text-black"><a href="blog-single.html">How to find a reliable partner from
                                        b2b platform ? </a></p>
                            </div>
                            <div class="know-box">
                                <p class="text-black"><a href="blog-single.html">My first experience of importing from
                                        china.</a></p>
                            </div>
                            <div class="know-box">
                                <p class="text-black"><a href="blog-single.html">Interior nigeria delar Mr. Ahmad
                                        hassen </a></p>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="know-box">
                                <p class="text-black"><a href="blog-single.html">How to find a reliable partner from
                                        b2b platform ? </a></p>
                            </div>
                            <div class="know-box">
                                <p class="text-black"><a href="blog-single.html">My first experience of importing from
                                        china.</a></p>
                            </div>
                            <div class="know-box">
                                <p class="text-black"><a href="blog-single.html">Interior nigeria delar Mr. Ahmad
                                        hassen </a></p>
                            </div>
                        </div>
                    </div>

                </div>
            </section>

            <!--accordian end-->

        </div>

        <!--body content end-->


        <!--footer start-->

        <footer class="py-11 bg-dark">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-3"> <a class="footer-logo text-white h2 mb-0" href="index.html">
                            Sales<span class="text-primary">Africa</span>
                        </a>
                        <p class="my-3 text-muted">Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                            Asperiores, suscipit iusto! Sequi recusandae perspiciatis sit incidunt? Iure doloribus iste
                            natus?.</p>
                        <ul class="list-inline mb-0">
                            <li class="list-inline-item"><a class="text-light ic-2x" href="#"><i
                                        class="la la-facebook"></i></a>
                            </li>
                            <li class="list-inline-item"><a class="text-light ic-2x" href="#"><i
                                        class="la la-dribbble"></i></a>
                            </li>
                            <li class="list-inline-item"><a class="text-light ic-2x" href="#"><i
                                        class="la la-instagram"></i></a>
                            </li>
                            <li class="list-inline-item"><a class="text-light ic-2x" href="#"><i
                                        class="la la-twitter"></i></a>
                            </li>
                            <li class="list-inline-item"><a class="text-light ic-2x" href="#"><i
                                        class="la la-linkedin"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12 col-lg-6 mt-6 mt-lg-0">
                        <div class="row">
                            <div class="col-12 col-sm-4 navbar-dark">
                                <h5 class="mb-4 text-white">Quick Links</h5>
                                <ul class="navbar-nav list-unstyled mb-0">
                                    <li class="mb-3 nav-item"><a class="nav-link"
                                            href="{{ route('index') }}">Home</a>
                                    </li>
                                    <li class="mb-3 nav-item"><a class="nav-link"
                                            href="{{ route('index') }}">About</a>
                                    </li>
                                    <li class="mb-3 nav-item"><a class="nav-link" href="faq.html">Faq</a>
                                    </li>
                                    <li class="nav-item"><a class="nav-link" href="{{ route('contactUs') }}">Contact
                                            Us</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-4 mt-6 mt-sm-0 navbar-dark">
                                <h5 class="mb-4 text-white">Top Products</h5>
                                <ul class="navbar-nav list-unstyled mb-0">
                                    <li class="mb-3 nav-item"><a class="nav-link" href="#">T-Shirts</a>
                                    </li>
                                    <li class="mb-3 nav-item"><a class="nav-link" href="#">Sneakers &
                                            Athletic</a>
                                    </li>
                                    <li class="mb-3 nav-item"><a class="nav-link" href="#">Shirts & Tops</a>
                                    </li>
                                    <li class="mb-3 nav-item"><a class="nav-link" href="#">Sunglasses</a>
                                    </li>
                                    <li class="mb-3 nav-item"><a class="nav-link" href="#">Bags & Wallets</a>
                                    </li>
                                    <li class="mb-3 nav-item"><a class="nav-link" href="#">Accessories</a>
                                    </li>
                                    <li class="nav-item"><a class="nav-link" href="#">Shoes</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-4 mt-6 mt-sm-0 navbar-dark">
                                <h5 class="mb-4 text-white">Features</h5>
                                <ul class="navbar-nav list-unstyled mb-0">
                                    <li class="mb-3 nav-item"><a class="nav-link"
                                            href="terms-and-conditions.html">Term Of Service</a>
                                    </li>
                                    <li class="mb-3 nav-item"><a class="nav-link" href="privacy-policy.html">Privacy
                                            Policy</a>
                                    </li>
                                    <li class="mb-3 nav-item"><a class="nav-link" href="#">Support</a>
                                    </li>
                                    <li class="mb-3 nav-item"><a class="nav-link" href="#">Shipping &
                                            Returns</a>
                                    </li>
                                    <li class="mb-3 nav-item"><a class="nav-link" href="#">Careers</a>
                                    </li>
                                    <li class="nav-item"><a class="nav-link" href="#">Our Story</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-3 mt-6 mt-lg-0">
                        <div class="d-flex mb-3">
                            <div class="me-2"> <i class="las la-map ic-2x text-primary"></i>
                            </div>
                            <div>
                                <h6 class="mb-1 text-light">Store address</h6>
                                <p class="mb-0 text-muted">423B, Road Wordwide Country, Africa</p>
                            </div>
                        </div>
                        <div class="d-flex mb-3">
                            <div class="me-2"> <i class="las la-envelope ic-2x text-primary"></i>
                            </div>
                            <div>
                                <h6 class="mb-1 text-light">Email Us</h6>
                                <a class="text-muted" href="mailto:themeht23@gmail.com"> themeht23@gmail.com</a>
                            </div>
                        </div>
                        <div class="d-flex mb-3">
                            <div class="me-2"> <i class="las la-mobile ic-2x text-primary"></i>
                            </div>
                            <div>
                                <h6 class="mb-1 text-light">Phone Number</h6>
                                <a class="text-muted" href="tel:+912345678900">+91-234-567-8900</a>
                            </div>
                        </div>
                        <div class="d-flex">
                            <div class="me-2"> <i class="las la-clock ic-2x text-primary"></i>
                            </div>
                            <div>
                                <h6 class="mb-1 text-light">Working Hours</h6>
                                <span class="text-muted">Mon - Fri: 10AM - 7PM</span>
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="my-8">
                <div class="row text-muted align-items-center">
                    <div class="col-md-7">Copyright ©2024 All rights reserved</u>
                    </div>
                    <div class="col-md-5 text-md-end mt-3 mt-md-0">
                        <ul class="list-inline mb-0">
                            <li class="list-inline-item">
                                <a href="#">
                                    {{-- <img class="img-fluid" src="assets/images/pay-icon/01.png" alt=""> --}}
                                    <img class="img-fluid" src="{{ asset('assets/images/pay-icon/01.png') }}"
                                        alt="">

                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#">
                                    <img class="img-fluid" src="assets/images/pay-icon/02.png" alt="">
                                </a>
                            </li>
                            <li class="list-inline-item">

                                <a href="#">

                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#">
                                    <img class="img-fluid" src="assets/images/pay-icon/04.png" alt="">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>

        <!--footer end-->

    </div>

    <!-- page wrapper end -->


    <!--back-to-top start-->

    <div class="scroll-top"><a class="smoothscroll" href="#top"><i class="las la-angle-up"></i></a></div>

    <!--back-to-top end-->

    <!-- inject js start -->

    {{-- <script src="assets/js/theme-plugin.js"></script>
<script src="assets/js/theme-script.js"></script> --}}

    <script src="{{ asset('assets/js/theme-plugin.js') }}"></script>
    <script src="{{ asset('assets/js/theme-script.js') }}"></script>
    <script>
        const tooltipTriggerList = document.querySelectorAll('[data-bs-toggle="tooltip"]')
        const tooltipList = [...tooltipTriggerList].map(tooltipTriggerEl => new bootstrap.Tooltip(tooltipTriggerEl))
    </script>

    <!-- inject js end -->

</body>

</html>
