<!DOCTYPE html>
<html lang="en">

<head>

    <!-- meta tags -->
    <meta charset="utf-8">
    <meta name="keywords" content="bootstrap 5, premium, multipurpose, sass, scss, saas, eCommerce, Shop, Fashion" />
    <meta name="description" content="Bootstrap 5 Landing Page Template" />
    <meta name="author" content="www.themeht.com" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Title -->
    <title>SalesAfrica</title>


    <link rel="shortcut icon" href="{{ asset('assets/images/favicon.ico') }}" />

    <!-- inject css start -->
    <link href="{{ asset('assets/css/theme-plugin.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/theme.min.css') }}" rel="stylesheet" />

    <!-- inject css end -->

</head>

<body>

    <!-- page wrapper start -->

    <div class="page-wrapper">
        <!--body content start-->

        <div class="page-content">

            <!--login start-->

            <section>
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-lg-7 col-12">
                            <img class="img-fluid" src="assets/images/login.png" alt="">
                        </div>
                        <div class="col-lg-5 col-12">
                            <div>
                                <h3 class="text-center mb-3 text-uppercase">Reset Password</h3>
                                @if (session('success'))
                                    <p class="text-success text-center h6">
                                        {{ session('success') }}
                                    </p>
                                @elseif (session('error'))
                                    <p class="text-danger text-center h6">
                                        {{ session('error') }}
                                    </p>
                                @else
                                @endif

                                <form action="{{ route('reset-password-post') }}" method="POST">
                                    @csrf
                                    <input type="text" name="token" value="{{ $token }}" hidden>
                                    <div class="form-group">
                                        <div class="input-group mb-3">
                                            <span class="input-group-text bg-transparent"><i
                                                    class="fa-solid fa-envelope text-muted"></i></span>
                                            <input type="email"
                                                class="form-control ps-15 bg-transparent @error('email') is-invalid @enderror"
                                                name="email"
                                                placeholder="{{ $errors->has('email') ? $errors->first('email') : 'Email' }}"
                                                value="{{ old('email') }}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group mb-3">
                                            <span class="input-group-text bg-transparent"><i
                                                    class="fa-solid fa-lock text-muted"></i></span>
                                            <input type="password"
                                                class="form-control ps-15 bg-transparent @error('password') is-invalid @enderror"
                                                name="password"
                                                placeholder="{{ $errors->has('password') ? $errors->first('password') : 'Password' }}">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="input-group mb-3">
                                            <span class="input-group-text bg-transparent"><i
                                                    class="fa-solid fa-lock text-muted"></i></span>
                                            <input type="password"
                                                class="form-control ps-15 bg-transparent @error('password_confirmation') is-invalid @enderror"
                                                name="password_confirmation"
                                                placeholder="{{ $errors->has('password_confirmation') ? $errors->first('password_confirmation') : 'Retype Password' }}">
                                        </div>
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-12 text-center">
                                        <button type="submit" class="btn btn-primary w-p100 mt-3">Reset
                                            password</button>
                                    </div>
                                    <!-- /.col -->
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!--login end-->

        </div>

        <!--body content end-->

    </div>

    <!-- page wrapper end -->

    <!-- inject js start -->

    {{-- <script src="{{ asset('assets/js/theme-plugin.js') }}"></script>
<script src="{{ asset('assets/js/theme-script.js') }}"></script> --}}

    <!-- inject js end -->

</body>

</html>
