<!DOCTYPE html>
<html>
<head>
    <style>
        body {
            font-family: Arial, sans-serif;
            line-height: 1.6;
            color: #333;
            background-color: #f4f4f4;
            padding: 20px;
        }
        .email-container {
            max-width: 600px;
            margin: 0 auto;
            background-color: #ffffff;
            padding: 20px;
            border: 1px solid #ddd;
            border-radius: 5px;
        }
        h1 {
            color: #391F3C;
        }
        p {
            margin-bottom: 1em;
        }
        a {
            color: #391F3C;
            text-decoration: none;
        }
        a:hover {
            text-decoration: underline;
        }
        .footer {
            font-size: 12px;
            color: #777;
        }
    </style>
</head>
<body>
    <div class="email-container">
        <h1>Password Reset Request</h1>
        <p>We received a request to reset the password for your SalesAfrica account. To proceed with resetting your password, please click the link below:</p>
        <a href="{{ route('reset-password', ['token' => $token]) }}" style="display: inline-block; padding: 10px 20px; margin: 20px 0; font-size: 16px; color: #fff; background-color: #391F3C; text-align: center; text-decoration: none; border-radius: 5px;">Reset password</a>
        <p>This link will be valid for the next 24 hours. If you did not request a password reset, please ignore this email. Please note that this inbox is unmonitored. If you need assistance or have any questions, feel free to reach out to our support team at <a href="mailto:salesafrica@gmail.com">salesafrica@gmail.comm</a></p>
        <p>Thank you,<br>The SalesAfrica Team</p>
        <p class="footer">Please do not reply to this email. This inbox is not monitored. If you need assistance, contact support at <a href="mailto:salesafrica@gmail.com">salesafrica@gmail.com</a>.</p>
    </div>
</body>
</html>
