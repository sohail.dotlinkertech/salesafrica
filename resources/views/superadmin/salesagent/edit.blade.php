@include('superadmin.layouts.header')

<div class="container-scroller">

    @include('superadmin.layouts.navbar')
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">

        @include('superadmin.layouts.sidebar')
        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">
                <div class="row">
                    <div class="col-12 grid-margin">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Edit Sales Agent</h4>



                                <form method="POST"
                                    action="{{ route('superadmin.sales-agent.update', $salesAgent->id) }}"
                                    enctype="multipart/form-data">
                                    @csrf
                                    @method('PUT')
                                    <div class="messages"></div>
                                    <div class="row">

                                        <input type="hidden" name="salesagent_type" value="0">


                                        <div class="row mb-2" id="supplierr" class="d-none">


                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="company">Company Name:</label>
                                                    <input type="text" class="form-control" name="company"
                                                        placeholder="Company name"
                                                        value="{{ old('company', $salesAgent->company) }}">
                                                    @error('company')
                                                        <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                             
                                            </div>
                                            <div class="col-md-12">
                                                   <!-- Checkbox input -->
                                                   <input class="form-check-input float-none" type="checkbox"
                                                   name="salesagent_type" id="customCheck1" value="1"
                                                   {{ old('salesagent_type', $salesAgent->salesagent_type) ? 'checked' : '' }}>

                                                   <label class="form-check-label" for="customCheck1">I am an
                                                       individual sales-agent who distribute products</label>
                                                       <br>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="location">Location:</label>
                                                    <input type="text" class="form-control" name="location"
                                                        placeholder="Location"
                                                        value="{{ old('location', $salesAgent->location) }}">
                                                    @error('location')
                                                        <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>


                                            <div class="col-md-6">
                                                @php
                                                    $countries = [
                                                        'Algeria',
                                                        'Angola',
                                                        'Benin',
                                                        'Botswana',
                                                        'Burkina Faso',
                                                        'Burundi',
                                                        'Cabo Verde',
                                                        'Cameroon',
                                                        'Central African Republic',
                                                        'Chad',
                                                        'Comoros',
                                                        'Congo (Brazzaville)',
                                                        'Congo (Kinshasa)',
                                                        'Djibouti',
                                                        'Egypt',
                                                        'Equatorial Guinea',
                                                        'Eritrea',
                                                        'Eswatini',
                                                        'Ethiopia',
                                                        'Gabon',
                                                        'Gambia',
                                                        'Ghana',
                                                        'Guinea',
                                                        'Guinea-Bissau',
                                                        'Ivory Coast',
                                                        'Kenya',
                                                        'Lesotho',
                                                        'Liberia',
                                                        'Libya',
                                                        'Madagascar',
                                                        'Malawi',
                                                        'Mali',
                                                        'Mauritania',
                                                        'Mauritius',
                                                        'Morocco',
                                                        'Mozambique',
                                                        'Namibia',
                                                        'Niger',
                                                        'Nigeria',
                                                        'Rwanda',
                                                        'Sao Tome and Principe',
                                                        'Senegal',
                                                        'Seychelles',
                                                        'Sierra Leone',
                                                        'Somalia',
                                                        'South Africa',
                                                        'South Sudan',
                                                        'Sudan',
                                                        'Tanzania',
                                                        'Togo',
                                                        'Tunisia',
                                                        'Uganda',
                                                        'Zambia',
                                                        'Zimbabwe',
                                                    ];
                                                @endphp

                                                <div class="form-group">
                                                    <label for="country">Country:</label>
                                                    <select class="form-control form-select" name="country">
                                                        <option value="">Select Country</option>
                                                        @foreach ($countries as $country)
                                                            <option value="{{ $country }}"
                                                                {{ old('country', $salesAgent->country) == $country ? 'selected' : '' }}>
                                                                {{ $country }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                     
                                                    @error('country')
                                                        <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>


                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="business_category">Business Category:</label>
                                                    <input type="text" class="form-control" name="business_category"
                                                        placeholder="Business Category"
                                                        value="{{ old('business_category', $salesAgent->buisness_category) }}">
                                                    @error('business_category')
                                                        <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="company_website">Company Website:</label>
                                                    <input type="text" class="form-control" name="company_website"
                                                        placeholder="Company Website"
                                                        value="{{ old('company_website', $salesAgent->company_website) }}">
                                                    @error('company_website')
                                                        <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>

                                        </div>

                                        <h4>Information of Contact Person</h4>



                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="form_name">First Name</label>
                                                    <input id="form_name" type="text" name="first_name"
                                                        class="form-control" placeholder="First name"
                                                        value="{{ old('first_name', $salesAgent->user->first_name) }}"
                                                        required="required" data-error="Firstname is required.">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="form_lastname">Last Name</label>
                                                    <input id="form_lastname" type="text" name="last_name"
                                                        class="form-control" placeholder="Last name" required="required"
                                                        data-error="Lastname is required."
                                                        value="{{ old('last_name', $salesAgent->user->last_name) }}">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="form_email">Email</label>
                                                    <input id="form_email" type="email" name="email"
                                                        class="form-control" placeholder="Email" required="required"
                                                        value="{{ old('email', $salesAgent->user->email) }}"
                                                        data-error="Valid email is required." disabled>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="form_phone">Phone</label>
                                                    <input id="form_phone" type="tel" name="phone"
                                                        class="form-control" placeholder="Phone" required="required"
                                                        {{ old('phone', $salesAgent->user->phone) }}
                                                        data-error="Phone is required">
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            {{-- <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="email_verification">Email Verification Code</label>
                                                    <input id="email_verification" type="text"
                                                        name="email_verification_code" class="form-control"
                                                        placeholder="Enter email verification code">
                                                    <button type="button" class="btn btn-sm btn-secondary mt-2"
                                                        onclick="sendEmailVerificationCode()">Send Verification
                                                        Code</button>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="phone_verification">Phone Verification Code</label>
                                                    <input id="phone_verification" type="text"
                                                        name="phone_verification_code" class="form-control"
                                                        placeholder="Enter phone verification code">
                                                    <button type="button" class="btn btn-sm btn-secondary mt-2"
                                                        onclick="sendPhoneVerificationCode()">Send Verification
                                                        Code</button>
                                                </div>
                                            </div> --}}

                                        </div>

                                        <div class="row mt-5">
                                            <div class="col-md-12">
                                                <div class="remember-checkbox clearfix mb-5">
                                                    <div class="form-check">
                                                        <input class="form-check-input float-none" type="checkbox"
                                                            name="customCheck1" id="customCheck1" required>
                                                        <label class="form-check-label" for="customCheck1">I agree to
                                                            the term of use and privacy policy</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">


                                                <button type="submit" class="btn btn-primary">Update
                                                    SalesAgent</button>

                                            </div>
                                        </div>
                                </form>


                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- content-wrapper ends -->

            @include('superadmin.layouts.footer')
