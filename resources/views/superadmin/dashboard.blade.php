@include('superadmin.layouts.header')

<div class="container-scroller">


    @include('superadmin.layouts.navbar')

    <!-- partial -->
    <div class="container-fluid page-body-wrapper">


        <!-- sidebar -->

        @include('superadmin.layouts.sidebar')


        <div class="main-panel">
            <div class="content-wrapper">
                
                <div class="row">
                    <div class="col-sm-12">
                        <div class="home-tab">
                            <div class="d-sm-flex align-items-center justify-content-between ">
                                <ul class="nav " role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link ">Overview</a>
                                        {{-- <h3>Overview</h3> --}}
                                    </li>

                                </ul>
                                <div>

                                </div>
                            </div>
                            <div class="tab-content tab-content-basic">
                                <div class="tab-pane fade show active" id="overview" role="tabpanel"
                                    aria-labelledby="overview">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="statistics-details d-flex align-items-center justify-content-between">
                                                <div class="text-center">
                                                    <p class="statistics-title">Total Suppliers</p>
                                                    <h3 class="rate-percentage">{{ $totalSuppliers }}</h3>
                                                    {{-- <p class="text-danger d-flex"><i class="mdi mdi-menu-down"></i><span>-0.5%</span></p> --}}
                                                </div>
                                                <div class="text-center">
                                                    <p class="statistics-title">Total Distributors</p>
                                                    <h3 class="rate-percentage">{{ $totalDistributors }}</h3>
                                                    {{-- <p class="text-success d-flex"><i class="mdi mdi-menu-up"></i><span>10.2%</span></p> --}}
                                                </div>
                                                <div class="text-center">
                                                    <p class="statistics-title">Total Sales Agent</p>
                                                    <h3 class="rate-percentage">{{ $totalSalesAgents }}</h3>
                                                    {{-- <p class="text-success d-flex"><i class="mdi mdi-menu-up"></i><span>2%</span></p> --}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                   
                                    <div class="row">
                                        <div class="col-lg-12 d-flex flex-column">
                                            <div class="row flex-grow">
                                                <div class="col-12 grid-margin stretch-card">
                                                    <div class="card card-rounded">
                                                        <div class="card-body">
                                                            <div
                                                                class="d-sm-flex justify-content-between align-items-start">
                                                                <div>
                                                                    <h4 class="card-title card-title-dash">Pending
                                                                        Requests</h4>
                                                                </div>
                                                                <div>

                                                                </div>
                                                            </div>
                                                            <div class="table-responsive  mt-1 mx-2">
                                                                <table class="table select-table">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Name</th>
                                                                            <th>Company</th>
                                                                            <th>Country</th>
                                                                            <th>Status</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        @foreach ($distributors as $distributor)
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="d-flex ">
                                                                                        <img src="{{ url('images/faces/face1.jpg') }}"
                                                                                            alt="">
                                                                                        <div>
                                                                                            <h6>{{ $distributor->user->name }}
                                                                                            </h6>
                                                                                            <p>{{ $distributor->company }}
                                                                                            </p>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                                <td>
                                                                                    <h6>{{ $distributor->company }}</h6>
                                                                                    {{-- <p>company type</p> --}}
                                                                                </td>
                                                                                <td>{{ $distributor->country }}</td>
                                                                                <td>
                                                                                    @if ($distributor->status == 0)
                                                                                        <div
                                                                                            class="badge badge-opacity-danger">
                                                                                            Pending</div>
                                                                                    @else
                                                                                        <div
                                                                                            class="badge badge-opacity-success">
                                                                                            Approved</div>
                                                                                    @endif
                                                                                </td>
                                                                            </tr>
                                                                        @endforeach

                                                                        @foreach ($salesAgents as $salesAgent)
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="d-flex">
                                                                                        <img src="{{ url('images/faces/face5.jpg') }}"
                                                                                            alt="">
                                                                                        <div>
                                                                                            <h6>{{ $salesAgent->user->name }}
                                                                                            </h6>
                                                                                            <p>{{ $salesAgent->company }}
                                                                                            </p>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                                <td>
                                                                                    <h6>{{ $salesAgent->company }}</h6>
                                                                                    {{-- <p>company type</p> --}}
                                                                                </td>
                                                                                <td>{{ $salesAgent->country }}</td>
                                                                                <td>
                                                                                    @if ($salesAgent->status == 0)
                                                                                        <div
                                                                                            class="badge badge-opacity-danger">
                                                                                            Pending</div>
                                                                                    @else
                                                                                        <div
                                                                                            class="badge badge-opacity-success">
                                                                                            Approved</div>
                                                                                    @endif
                                                                                </td>
                                                                            </tr>
                                                                        @endforeach

                                                                        @foreach ($suppliers as $supplier)
                                                                            <tr>
                                                                                <td>
                                                                                    <div class="d-flex">
                                                                                        <img src="{{ url('images/faces/face3.jpg') }}"
                                                                                            alt="">
                                                                                        <div>
                                                                                            <h6>{{ $supplier->user->name }}
                                                                                            </h6>
                                                                                            <p>{{ $supplier->company }}
                                                                                            </p>
                                                                                        </div>
                                                                                    </div>
                                                                                </td>
                                                                                <td>
                                                                                    <h6>{{ $supplier->company }}</h6>
                                                                                    {{-- <p>company type</p> --}}
                                                                                </td>
                                                                                <td>{{ $supplier->country }}</td>
                                                                                <td>
                                                                                    @if ($supplier->status == 0)
                                                                                        <div
                                                                                            class="badge badge-opacity-danger">
                                                                                            Pending</div>
                                                                                    @else
                                                                                        <div
                                                                                            class="badge badge-opacity-success">
                                                                                            Approved</div>
                                                                                    @endif
                                                                                </td>
                                                                            </tr>
                                                                        @endforeach
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- container-scroller -->
            @include('superadmin.layouts.footer')
