@include('superadmin.layouts.header')

<div class="container-scroller">
    @include('superadmin.layouts.navbar')
    <div class="container-fluid page-body-wrapper">
        @include('superadmin.layouts.sidebar')

        
        <link rel="stylesheet" href="{{ asset('assets/css/buyer.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">      
        <link rel="stylesheet" href="{{ asset('assets/css/stepCounter.css') }}">
       
        <section class="buyer w-100">
            <!-- overlay -->
       
                <!-- main content -->
                <div class="product seller px-lg-5 mt-4">
                    <div class="container">
    
                        <section class="Products pb-5">
                         
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-4 pe-0">
                                            <div class="card h-100">
                                                <div class="search-user p-3">
                                                    <input type="text" class="form-control" placeholder="Search">
                                                    <img src="{{asset('assets/icons/search2.png')}}" alt="">
                                                </div>
                                                <div class="user-main">
                                                    <div class="user cursor py-3 p-3">
                                                        <div class="d-flex">
                                                            <div class="img me-2">
                                                                <img src="{{asset('assets/icons/Avatar.png')}}" alt="">
                                                            </div>
                                                            <div style="flex: 1;">
                                                                <h6 class="name">Bill Kuphal</h6>
                                                                <div class="short-msg">
                                                                    The weather will be perfect for...
                                                                </div>
                                                            </div>
                                                            <div class="time ms-2 align-self-end">
                                                                <span>9:41 AM</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="user cursor bg-white py-3 p-3">
                                                        <div class="d-flex">
                                                            <div class="img me-2">
                                                                <img src="{{asset('assets/icons/Avatar.png')}}" alt="">
                                                            </div>
                                                            <div style="flex: 1;">
                                                                <h6 class="name">Bill Kuphal</h6>
                                                                <div class="short-msg">
                                                                    The weather will be perfect for...
                                                                </div>
                                                            </div>
                                                            <div class="time ms-2 align-self-end">
                                                                <span>9:41 AM</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="user cursor bg-white py-3 p-3">
                                                        <div class="d-flex">
                                                            <div class="img me-2">
                                                                <img src="{{asset('assets/icons/Avatar.png')}}" alt="">
                                                            </div>
                                                            <div style="flex: 1;">
                                                                <h6 class="name">Bill Kuphal</h6>
                                                                <div class="short-msg">
                                                                    The weather will be perfect for...
                                                                </div>
                                                            </div>
                                                            <div class="time ms-2 align-self-end">
                                                                <span>9:41 AM</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="user cursor bg-white py-3 p-3">
                                                        <div class="d-flex">
                                                            <div class="img me-2">
                                                                <img src="{{asset('assets/icons/Avatar.png')}}" alt="">
                                                            </div>
                                                            <div style="flex: 1;">
                                                                <h6 class="name">Bill Kuphal</h6>
                                                                <div class="short-msg">
                                                                    The weather will be perfect for...
                                                                </div>
                                                            </div>
                                                            <div class="time ms-2 align-self-end">
                                                                <span>9:41 AM</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="user cursor bg-white py-3 p-3">
                                                        <div class="d-flex">
                                                            <div class="img me-2">
                                                                <img src="{{asset('assets/icons/Avatar.png')}}" alt="">
                                                            </div>
                                                            <div style="flex: 1;">
                                                                <h6 class="name">Bill Kuphal</h6>
                                                                <div class="short-msg">
                                                                    The weather will be perfect for...
                                                                </div>
                                                            </div>
                                                            <div class="time ms-2 align-self-end">
                                                                <span>9:41 AM</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="user cursor bg-white py-3 p-3">
                                                        <div class="d-flex">
                                                            <div class="img me-2">
                                                                <img src="{{asset('assets/icons/Avatar.png')}}" alt="">
                                                            </div>
                                                            <div style="flex: 1;">
                                                                <h6 class="name">Bill Kuphal</h6>
                                                                <div class="short-msg">
                                                                    The weather will be perfect for...
                                                                </div>
                                                            </div>
                                                            <div class="time ms-2 align-self-end">
                                                                <span>9:41 AM</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="user cursor bg-white py-3 p-3">
                                                        <div class="d-flex">
                                                            <div class="img me-2">
                                                                <img src="{{asset('assets/icons/Avatar.png')}}" alt="">
                                                            </div>
                                                            <div style="flex: 1;">
                                                                <h6 class="name">Bill Kuphal</h6>
                                                                <div class="short-msg">
                                                                    The weather will be perfect for...
                                                                </div>
                                                            </div>
                                                            <div class="time ms-2 align-self-end">
                                                                <span>9:41 AM</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="user cursor bg-white py-3 p-3">
                                                        <div class="d-flex">
                                                            <div class="img me-2">
                                                                <img src="{{asset('assets/icons/Avatar.png')}}" alt="">
                                                            </div>
                                                            <div style="flex: 1;">
                                                                <h6 class="name">Bill Kuphal</h6>
                                                                <div class="short-msg">
                                                                    The weather will be perfect for...
                                                                </div>
                                                            </div>
                                                            <div class="time ms-2 align-self-end">
                                                                <span>9:41 AM</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="user cursor bg-white py-3 p-3">
                                                        <div class="d-flex">
                                                            <div class="img me-2">
                                                                <img src="{{asset('assets/icons/Avatar.png')}}" alt="">
                                                            </div>
                                                            <div style="flex: 1;">
                                                                <h6 class="name">Bill Kuphal</h6>
                                                                <div class="short-msg">
                                                                    The weather will be perfect for...
                                                                </div>
                                                            </div>
                                                            <div class="time ms-2 align-self-end">
                                                                <span>9:41 AM</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="user cursor bg-white py-3 p-3">
                                                        <div class="d-flex">
                                                            <div class="img me-2">
                                                                <img src="{{asset('assets/icons/Avatar.png')}}" alt="">
                                                            </div>
                                                            <div style="flex: 1;">
                                                                <h6 class="name">Bill Kuphal</h6>
                                                                <div class="short-msg">
                                                                    The weather will be perfect for...
                                                                </div>
                                                            </div>
                                                            <div class="time ms-2 align-self-end">
                                                                <span>9:41 AM</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="user cursor bg-white py-3 p-3">
                                                        <div class="d-flex">
                                                            <div class="img me-2">
                                                                <img src="{{asset('assets/icons/Avatar.png')}}" alt="">
                                                            </div>
                                                            <div style="flex: 1;">
                                                                <h6 class="name">Bill Kuphal</h6>
                                                                <div class="short-msg">
                                                                    The weather will be perfect for...
                                                                </div>
                                                            </div>
                                                            <div class="time ms-2 align-self-end">
                                                                <span>9:41 AM</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="user cursor bg-white py-3 p-3">
                                                        <div class="d-flex">
                                                            <div class="img me-2">
                                                                <img src="{{asset('assets/icons/Avatar.png')}}" alt="">
                                                            </div>
                                                            <div style="flex: 1;">
                                                                <h6 class="name">Bill Kuphal</h6>
                                                                <div class="short-msg">
                                                                    The weather will be perfect for...
                                                                </div>
                                                            </div>
                                                            <div class="time ms-2 align-self-end">
                                                                <span>9:41 AM</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="user cursor bg-white py-3 p-3">
                                                        <div class="d-flex">
                                                            <div class="img me-2">
                                                                <img src="{{asset('assets/icons/Avatar.png')}}" alt="">
                                                            </div>
                                                            <div style="flex: 1;">
                                                                <h6 class="name">Bill Kuphal</h6>
                                                                <div class="short-msg">
                                                                    The weather will be perfect for...
                                                                </div>
                                                            </div>
                                                            <div class="time ms-2 align-self-end">
                                                                <span>9:41 AM</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="user cursor bg-white py-3 p-3">
                                                        <div class="d-flex">
                                                            <div class="img me-2">
                                                                <img src="{{asset('assets/icons/Avatar.png')}}" alt="">
                                                            </div>
                                                            <div style="flex: 1;">
                                                                <h6 class="name">Bill Kuphal</h6>
                                                                <div class="short-msg">
                                                                    The weather will be perfect for...
                                                                </div>
                                                            </div>
                                                            <div class="time ms-2 align-self-end">
                                                                <span>9:41 AM</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="user cursor bg-white py-3 p-3">
                                                        <div class="d-flex">
                                                            <div class="img me-2">
                                                                <img src="{{asset('assets/icons/Avatar.png')}}" alt="">
                                                            </div>
                                                            <div style="flex: 1;">
                                                                <h6 class="name">Bill Kuphal</h6>
                                                                <div class="short-msg">
                                                                    The weather will be perfect for...
                                                                </div>
                                                            </div>
                                                            <div class="time ms-2 align-self-end">
                                                                <span>9:41 AM</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="user cursor bg-white py-3 p-3">
                                                        <div class="d-flex">
                                                            <div class="img me-2">
                                                                <img src="{{asset('assets/icons/Avatar.png')}}" alt="">
                                                            </div>
                                                            <div style="flex: 1;">
                                                                <h6 class="name">Bill Kuphal</h6>
                                                                <div class="short-msg">
                                                                    The weather will be perfect for...
                                                                </div>
                                                            </div>
                                                            <div class="time ms-2 align-self-end">
                                                                <span>9:41 AM</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="user cursor bg-white py-3 p-3">
                                                        <div class="d-flex">
                                                            <div class="img me-2">
                                                                <img src="{{asset('assets/icons/Avatar.png')}}" alt="">
                                                            </div>
                                                            <div style="flex: 1;">
                                                                <h6 class="name">Bill Kuphal</h6>
                                                                <div class="short-msg">
                                                                    The weather will be perfect for...
                                                                </div>
                                                            </div>
                                                            <div class="time ms-2 align-self-end">
                                                                <span>9:41 AM</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="user cursor bg-white py-3 p-3">
                                                        <div class="d-flex">
                                                            <div class="img me-2">
                                                                <img src="{{asset('assets/icons/Avatar.png')}}" alt="">
                                                            </div>
                                                            <div style="flex: 1;">
                                                                <h6 class="name">Bill Kuphal</h6>
                                                                <div class="short-msg">
                                                                    The weather will be perfect for...
                                                                </div>
                                                            </div>
                                                            <div class="time ms-2 align-self-end">
                                                                <span>9:41 AM</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="user cursor bg-white py-3 p-3">
                                                        <div class="d-flex">
                                                            <div class="img me-2">
                                                                <img src="{{asset('assets/icons/Avatar.png')}}" alt="">
                                                            </div>
                                                            <div style="flex: 1;">
                                                                <h6 class="name">Bill Kuphal</h6>
                                                                <div class="short-msg">
                                                                    The weather will be perfect for...
                                                                </div>
                                                            </div>
                                                            <div class="time ms-2 align-self-end">
                                                                <span>9:41 AM</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-8 ps-0">
                                            <div class="chat">
                                                <div class="card">
                                                    <div class="card-body p-0">
                                                        <div class="chat-box">
                                                            <div
                                                                class="d-flex align-items-center justify-content-between  p-3 chat-header">
                                                                <div class="d-flex align-items-center userInfo">
                                                                    <div class="img me-2">
                                                                        <img src="{{asset('assets/icons/Avatar.png')}}" alt="">
                                                                        <img src="{{asset('assets/icons/online.png')}}"
                                                                            class="green-dot" alt="">
                                                                    </div>
                                                                    <div class="text">
                                                                        <p>Thomas David</p>
                                                                    
                                                                        <span>
                                                                            Online for 10 mins
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                                <div class="cross p-2">
                                                                    <img src="../images/icons/x-circle.png" alt="">
                                                                </div>
                                                            </div>
                                                            <div class="chat-body px-3 py-4">
                                                                <div class="d-flex userInfo mb-3">
                                                                    <div class="img me-2">
                                                                        <img src="{{asset('assets/icons/Avatar.png')}}" alt="">
                                                                    </div>
                                                                    <div class="text">
                                                                        <p>omg, this is amazing</p>
                                                                        <p>perfect! ✅</p>
                                                                        <small>2.14 <span>PM</span></small>
                                                                    </div>
                                                                </div>
                                                                <div class="d-flex flex-row-reverse userInfo sender mb-3">
                                                                 
                                                                    <div class="text  me-2"
                                                                        style="text-align: -webkit-right;">
                                                                        <p>omg, this is amazing</p>
                                                                        <p>perfect! ✅</p>
                                                                        <small><img src="../images/icons/Read status.png"
                                                                                alt=""> 2.14 <span>PM</span></small>
                                                                    </div>
                                                                </div>
                                                                <div class="d-flex flex-row-reverse userInfo sender mb-3">
                                                                    <div class="text  me-2"
                                                                        style="text-align: -webkit-right;">
                                                                        <div class="mainn">
                                                                            <p>
                                                                            <div class="d-flex align-items-end mb-2">
                                                                                <span class="img-small me-1">
                                                                                    <img src="{{asset('assets/icons/Avatar.png')}}" alt="">
                                                                                </span>
                                                                                <div class="d-flex upper ms-1">
                                                                                    <img src="{{asset('assets/icons/Avatar.png')}}" alt="">
                                                                                    <p class="p-0">Lorem, ipsum dolor sit
                                                                                        amet consectetur adipisicing elit.
                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                           
                                                                            </p>
                                                                        </div>
                                                                        <small><img src="{{asset('assets/icons/Read status.png')}}"
                                                                                alt=""> 2.14 <span>PM</span></small>
                                                                    </div>
                                                                </div>
                                                           
                                                            </div>
                                                            <div class="d-flex align-items-center p-3 chat-footer">
                                                                <div class="d-flex align-items-center pin">
                                                                    <div class="img me-2">
                                                                        <img src="{{asset('assets/icons/pluscircle.svg')}}"
                                                                            class="cursor" alt="">
                                                                    </div>
                                                                </div>
                                                                <div class="inpt p-2" style="flex: 1;">
                                                                    <input type="text" class="form-control"
                                                                        placeholder="Type a message">
                                                                    <img src="{{asset('assets/icons/emoji.svg')}}" alt="">
                                                                </div>
                                                                <div class="d-flex align-items-center send">
                                                                    <div class="img me-2">
                                                                        <img src="{{asset('assets/icons/Send Button.svg')}}"
                                                                            class="cursor" alt="">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
    
        </section>
    </div>
</div>
