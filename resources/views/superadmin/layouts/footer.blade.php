<!-- content-wrapper ends -->

<footer class="footer" style="">
    <div class="text-center d-sm-flex justify-content-center justify-content-sm-between">

        <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center">Copyright © 2024. All rights
            reserved.</span>
    </div>
</footer>

</div>
</div>
</div>


<!-- plugins:js -->
{{-- <script src="{{ url('vendors/js/vendor.bundle.base.js')}}"></script> --}}
<!-- endinject -->
<!-- Plugin js for this page -->
<script src="{{ url('vendors/chart.js/Chart.min.js') }}"></script>
{{-- <script src="{{ url('vendors/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script> --}}
<script src="{{ url('vendors/progressbar.js/progressbar.min.js') }}"></script>

<!-- End plugin js for this page -->
<!-- inject:js -->
<script src="{{ url('js/off-canvas.js') }}"></script>
<script src="{{ url('js/hoverable-collapse.js') }}"></script>
<script src="{{ url('js/template.js') }}"></script>
{{-- <script src="{{ url('js/settings.js')}}"></script> --}}
<script src="{{ url('js/todolist.js') }}"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<script src="{{ url('js/jquery.cookie.js" ') }}"></script>
<script src="{{ url('js/dashboard.js') }}"></script>
<script src="{{ url('js/Chart.roundedBarCharts.js') }}"></script>
<!-- End custom js for this page-->

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
{{-- <script>
  $(document).ready(function() {
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
      });

      // Use event delegation to bind the click event to dynamically added elements
      $(document).on('click', '.not_approved', function() {
          var userId = $(this).data('user-id');
          var data = {
              id: userId,
              status: 1
          };

          $.ajax({
              url: "{{ route('approval_status') }}",
              type: 'POST',
              data: data,
              success: function(response) {
                  var button = $('#not_approved' + userId);
                  button.removeClass('btn btn-danger');
                  button.addClass('btn btn-success');
                  button.css('background-color', '');
                  button.text('Approved');
              },
              error: function(xhr) {
                  console.log(xhr.responseText);
              }
          });
      });
  });
</script> --}}


<script>
    $(document).ready(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        // Use event delegation to bind the click event to dynamically added elements
        $(document).on('click', '.not_approved', function() {
            var userId = $(this).data('user-id');
            var button = $('#not_approved' + userId);

            // Show confirmation popup if the button is not already approved
            var isConfirmed = confirm("Are you sure you want to approve this user?");

            if (isConfirmed) {
                var data = {
                    id: userId,
                    status: 1
                };

                $.ajax({
                    url: "{{ route('superadmin.approval_status') }}",
                    type: 'POST',
                    data: data,
                    success: function(response) {
                        button.removeClass('btn btn-danger not_approved');
                        button.addClass('btn btn-success');
                        button.css('background-color', '');
                        button.text('Approved');
                    },
                    error: function(xhr) {
                        console.log(xhr.responseText);
                    }
                });
            }
        });
    });
</script>
</body>

</html>
