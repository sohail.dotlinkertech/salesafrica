
<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
    
      <li class="nav-item">
        <a class="nav-link" data-bs-toggle="collapse" href="{{ url('superadmin/dashboard') }}" aria-expanded="false" aria-controls="form-elements">
          <i class="menu-icon mdi mdi-card-text-outline"></i>
          <span class="menu-title">Dashboard</span>
        </a>
      
      </li>

      
      <li class="nav-item">
        <a class="nav-link" data-bs-toggle="collapse" href="{{ url('superadmin/admin') }}" aria-expanded="false" aria-controls="tables">
          <i class="menu-icon mdi mdi-account"></i>
          <span class="menu-title">Admin</span>
        </a>
        
      </li>

      <li class="nav-item">
        <a class="nav-link" data-bs-toggle="collapse" href="{{ url('superadmin/suppliers') }}" aria-expanded="false" aria-controls="tables">
          <i class="menu-icon mdi mdi-truck"></i>
          <span class="menu-title">Suppliers</span>
        </a>
        
      </li>
      <li class="nav-item">
        <a class="nav-link" data-bs-toggle="collapse" href="{{ url('superadmin/distributors') }}" aria-expanded="false" aria-controls="charts">
          <i class="menu-icon mdi mdi-account-switch"></i>
          <span class="menu-title">Distributors</span>
        </a>
       
      </li>
      
      <li class="nav-item">
        <a class="nav-link" data-bs-toggle="collapse" href="{{ url('superadmin/sales-agent') }}" aria-expanded="false" aria-controls="icons">
          <i class="menu-icon mdi mdi-run"></i>
          <span class="menu-title">Sales Agent</span>
        </a>
        
      </li>

      <li class="nav-item">
        <a class="nav-link" data-bs-toggle="collapse" href="{{ url('superadmin/news-approval') }}" aria-expanded="false" aria-controls="icons">
          <i class="menu-icon mdi mdi-checkbox-marked-circle"></i>
          <span class="menu-title">News Approval</span>
        </a>
        
      </li>
      
      <li class="nav-item">
        <a class="nav-link" data-bs-toggle="collapse" href="{{ url('superadmin/messages') }}" aria-expanded="false" aria-controls="icons">
          <i class="menu-icon mdi mdi-message-text"></i>
          <span class="menu-title">Messages</span>
        </a>
      </li>
      
    </ul>
  </nav>
