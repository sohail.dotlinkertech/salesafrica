@include('superadmin.layouts.header')

<div class="container-scroller">

    @include('superadmin.layouts.navbar')
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">

        @include('superadmin.layouts.sidebar')
        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">
                <div class="row">
                    <div class="col-12 grid-margin">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Edit Admin</h4>
                                <form class="form-sample" method="POST"
                                    action="{{ route('superadmin.admin.update', ['id' => $admin->user['id']]) }}"
                                    enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-6 col-form-label">
                                            <div class="form-group row">
                                                <label class="col-sm-3">First Name <span
                                                        class="text-danger">*</span></label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="first_name" class="form-control"
                                                        placeholder="Enter Your First Name"
                                                        value="{{ old('first_name',$admin->user->first_name) }}" />
                                                    @error('first_name')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-form-label">
                                            <div class="form-group row">
                                                <label class="col-sm-3">Last Name <span
                                                        class="text-danger">*</span></label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="last_name" class="form-control"
                                                        placeholder="Enter Your Last Name"
                                                        value="{{ old('last_name',$admin->user->last_name) }}" />
                                                    @error('last_name')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-md-6 col-form-label">
                                            <div class="form-group row">
                                                <label class="col-sm-3">Phone <span class="text-danger">*</span></label>
                                                <div class="col-sm-6">
                                                    <input type="text" name="phone" class="form-control"
                                                        placeholder="Enter Your Phone" value="{{ old('phone',$admin->user->phone) }}" />
                                                    @error('phone')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                                <div class="col-sm-3">
                                                    <button type="button" class="btn btn-secondary">Send OTP</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-form-label">
                                            <div class="form-group row">
                                                <label class="col-sm-3">Email <span class="text-danger">*</span></label>
                                                <div class="col-sm-6">
                                                    <input type="email" name="email" class="form-control"
                                                        placeholder="Enter Your Email Address"
                                                        value="{{ old('email',$admin->user->email) }}" />
                                                    @error('email')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                                <div class="col-sm-3">
                                                    <button type="button" class="btn btn-secondary">Verify</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        @php
                                            $countries = [
                                                'Algeria',
                                                'Angola',
                                                'Benin',
                                                'Botswana',
                                                'Burkina Faso',
                                                'Burundi',
                                                'Cabo Verde',
                                                'Cameroon',
                                                'Central African Republic',
                                                'Chad',
                                                'Comoros',
                                                'Congo (Brazzaville)',
                                                'Congo (Kinshasa)',
                                                'Djibouti',
                                                'Egypt',
                                                'Equatorial Guinea',
                                                'Eritrea',
                                                'Eswatini',
                                                'Ethiopia',
                                                'Gabon',
                                                'Gambia',
                                                'Ghana',
                                                'Guinea',
                                                'Guinea-Bissau',
                                                'Ivory Coast',
                                                'Kenya',
                                                'Lesotho',
                                                'Liberia',
                                                'Libya',
                                                'Madagascar',
                                                'Malawi',
                                                'Mali',
                                                'Mauritania',
                                                'Mauritius',
                                                'Morocco',
                                                'Mozambique',
                                                'Namibia',
                                                'Niger',
                                                'Nigeria',
                                                'Rwanda',
                                                'Sao Tome and Principe',
                                                'Senegal',
                                                'Seychelles',
                                                'Sierra Leone',
                                                'Somalia',
                                                'South Africa',
                                                'South Sudan',
                                                'Sudan',
                                                'Tanzania',
                                                'Togo',
                                                'Tunisia',
                                                'Uganda',
                                                'Zambia',
                                                'Zimbabwe',
                                            ];

                                        @endphp
                                        <div class="col-md-6 col-form-label">
                                            <div class="form-group row">
                                                <label class="col-sm-3">Country <span
                                                        class="text-danger">*</span></label>
                                                <div class="col-sm-9">
                                                    <select class="form-control form-select" name="country">
                                                        <option value="">Select Country</option>
                                                        @foreach ($countries as $country)
                                                            <option value="{{ $country }}"
                                                                {{ old('country',$admin->country) == $country ? 'selected' : '' }}>
                                                                {{ $country }}
                                                            </option>
                                                        @endforeach


                                                    </select>
                                                    @error('country')
                                                        <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-md-6 col-form-label">
                                            <div class="form-group row">
                                                <label class="col-sm-3">Photo</label>
                                                <div class="col-sm-9">
                                                    <input type="file" name="image" class="form-control" />
                                                    @error('image')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>


                                    </div>

                                    <button class="btn btn-primary">Save</button>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- content-wrapper ends -->

        {{-- @include('superadmin.layouts.footer') --}}
