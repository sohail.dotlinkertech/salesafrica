@include('superadmin.layouts.header')

<div class="container-scroller">

    @include('superadmin.layouts.navbar')
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">

        @include('superadmin.layouts.sidebar')
        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">
                <div class="row">
                    <div class="col-lg-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Admins</h4>
                                <div class="row mb-3 justify-content-end">
                                    <div class="col-auto">
                                        <a href="{{ url('superadmin/add/admin') }}">
                                            <button class="btn btn-primary">Add</button>
                                        </a>
                                    </div>
                                </div>

                                <div class="row mb-3">
                                    <div class="col-md-4">
                                        <input type="text" placeholder="search" class="form-control" id="filter">
                                    </div>


                                    <div class="col-md-2"></div>


                                    <div class="col-md-3">

                                        <select class="form-control form-select filters" id="country">
                                            <option value="">Select Country</option>
                                            <option value="Algeria">Algeria</option>
                                            <option value="Angola">Angola</option>
                                            <option value="Benin">Benin</option>
                                            <option value="Botswana">Botswana</option>
                                            <option value="Burkina Faso">Burkina Faso</option>
                                            <option value="Burundi">Burundi</option>
                                            <option value="Cabo Verde">Cabo Verde</option>
                                            <option value="Cameroon">Cameroon</option>
                                            <option value="Central African Republic">Central African Republic</option>
                                            <option value="Chad">Chad</option>
                                            <option value="Comoros">Comoros</option>
                                            <option value="Congo (Brazzaville)">Congo (Brazzaville)</option>
                                            <option value="Congo (Kinshasa)">Congo (Kinshasa)</option>
                                            <option value="Djibouti">Djibouti</option>
                                            <option value="Egypt">Egypt</option>
                                            <option value="Equatorial Guinea">Equatorial Guinea</option>
                                            <option value="Eritrea">Eritrea</option>
                                            <option value="Eswatini">Eswatini</option>
                                            <option value="Ethiopia">Ethiopia</option>
                                            <option value="Gabon">Gabon</option>
                                            <option value="Gambia">Gambia</option>
                                            <option value="Ghana">Ghana</option>
                                            <option value="Guinea">Guinea</option>
                                            <option value="Guinea-Bissau">Guinea-Bissau</option>
                                            <option value="Ivory Coast">Ivory Coast</option>
                                            <option value="Kenya">Kenya</option>
                                            <option value="Lesotho">Lesotho</option>
                                            <option value="Liberia">Liberia</option>
                                            <option value="Libya">Libya</option>
                                            <option value="Madagascar">Madagascar</option>
                                            <option value="Malawi">Malawi</option>
                                            <option value="Mali">Mali</option>
                                            <option value="Mauritania">Mauritania</option>
                                            <option value="Mauritius">Mauritius</option>
                                            <option value="Morocco">Morocco</option>
                                            <option value="Mozambique">Mozambique</option>
                                            <option value="Namibia">Namibia</option>
                                            <option value="Niger">Niger</option>
                                            <option value="Nigeria">Nigeria</option>
                                            <option value="Rwanda">Rwanda</option>
                                            <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                            <option value="Senegal">Senegal</option>
                                            <option value="Seychelles">Seychelles</option>
                                            <option value="Sierra Leone">Sierra Leone</option>
                                            <option value="Somalia">Somalia</option>
                                            <option value="South Africa">South Africa</option>
                                            <option value="South Sudan">South Sudan</option>
                                            <option value="Sudan">Sudan</option>
                                            <option value="Tanzania">Tanzania</option>
                                            <option value="Togo">Togo</option>
                                            <option value="Tunisia">Tunisia</option>
                                            <option value="Uganda">Uganda</option>
                                            <option value="Zambia">Zambia</option>
                                            <option value="Zimbabwe">Zimbabwe</option>
                                        </select>

                                    </div>


                                    <div class="col-md-3">
                                        <select name="" class='form-control  form-select filters' id="account_type">
                                            <option value="">Select Account Type</option>
                                            <option value="VIP">V.I.P</option>
                                            <option value="Standard">Standard</option>
                                        </select>
                                    </div>


                                </div>

                                <div class="table-responsive pt-3">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Region</th>
                                                <th>Email</th>
                                                <th>Image</th>
                                                <th>Account Type</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($admins as $admin)
                                                <tr>
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td>{{ $admin->country }}</td>
                                                    <td>{{ $admin->user->email }}</td>
                                                    <td>
                                                        @if ($admin->image)
                                                        <a href="{{ url('uploads/' . $admin->image) }}" target="_blank">
                                                            <img src="{{ url('uploads/' . $admin->image) }}" alt="Admin Image" width="50" style="cursor: pointer;">
                                                        </a>
                                                        
                                                        @else
                                                            <img src="{{ url('images/faces/face1.jpg') }}"
                                                                alt="Default Image" width="50">
                                                        @endif
                                                    </td>
                                                    <td style="color:rgb(80, 83, 80); text-align:center;">
                                                        <strong>{{ $admin->account_type }}</strong>
                                                    </td>
                                                    <td>
                                                        @if ($admin->status == 1)
                                                            <button class="btn btn-success"
                                                                id="approved{{ $admin->user_id }}">Approved</button>
                                                        @else
                                                            <button id="not_approved{{ $admin->user_id }}"
                                                                class="btn btn-danger not_approved"
                                                                data-user-id="{{ $admin->user_id }}"
                                                                style="background-color:rgb(198, 8, 8)">Not
                                                                Approved</button>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <a href="{{ url('superadmin/edit/admin/' . $admin->user_id) }}">
                                                            <button class="btn btn-primary">Edit</button>
                                                        </a>
                                                        <a href="{{ url('superadmin/delete/admin/' . $admin->user_id) }}">
                                                            <button type="submit"
                                                                class="btn btn-danger">Delete</button>
                                                        </a>

                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <!-- content-wrapper ends -->

            @include('superadmin.layouts.footer')

            <script>
                $(document).ready(function() {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $('.filters').change(function() {

                        var data = {
                            country: $('#country').val(),
                            account_type: $('#account_type').val(),


                        };
                        // alert('Selected country: ' + data.country);

                        $.ajax({
                            url: "{{ route('filter_admin') }}",
                            type: 'POST',
                            data: data,
                            success: function(response) {
                                $('tbody').html(response.html);
                            },
                            error: function(xhr) {
                                console.log(xhr.responseText);
                            }
                        });
                    });
                });
            </script>
