<!DOCTYPE html>
<html lang="en">

<head>

    <!-- meta tags -->
    <meta charset="utf-8">
    <meta name="keywords" content="bootstrap 5, premium, multipurpose, sass, scss, saas, eCommerce, Shop, Fashion" />
    <meta name="description" content="Bootstrap 5 Landing Page Template" />
    <meta name="author" content="www.themeht.com" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Title -->
    <title>SalesAfrica</title>

    <!-- Favicon Icon -->
    <link rel="shortcut icon" href="assets/images/favicon.ico" />

    <!-- inject css start -->

    <link href="{{ asset('assets/css/theme-plugin.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/theme.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/custom.css') }}" rel="stylesheet" />

    <!-- inject css end -->

</head>

<body>

    <!-- page wrapper start -->

    <div class="page-wrapper">

        <!-- preloader start -->

        <div id="ht-preloader">
            <div class="loader clear-loader">
                <img class="img-fluid" src="assets/images/loader.gif" alt="">
            </div>
        </div>

        <!-- preloader end -->


        <!--header start-->

        <header class="site-header">
            <div id="header-wrap" class="shadow-sm py-md-5 py-3 fixed-top">
                <div class="container">
                    <div class="row">
                        <!--menu start-->
                        <div class="col">
                            <nav class="navbar navbar-expand-lg navbar-light">
                                <!-- <a class="navbar-brand logo d-lg-none" href="index.html">
              <img class="img-fluid" src="assets/images/logo.png" alt="">
            </a> -->
                                <a class="navbar-brand logo fs25 me-4" href="index.html">
                                    Sales<span class="text-primary">Africa</span>
                                </a>
                                <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                                    <span class="navbar-toggler-icon"></span>
                                </button>
                                <div class="collapse navbar-collapse" id="navbarNav">
                                    <ul class="navbar-nav">
                                        <li class="nav-item"> <a class="nav-link" href="company.html">company</a>
                                        </li>
                                        <li class="nav-item"> <a class="nav-link" href="quotation.html">RFQ</a>
                                        </li>
                                        <li class="nav-item"> <a class="nav-link" href="sales-agent.html">Sales
                                                Agents</a>
                                        </li>
                                        <li class="nav-item"> <a class="nav-link"
                                                href="distributor.html">Distributors</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="right-nav align-items-center d-flex justify-content-end">
                                    <span class="me-2">
                                        <div class="dropdown">
                                            <div class="dropdown-toggle" data-bs-toggle="dropdown"
                                                aria-expanded="false">
                                                <img src="assets/images/thumbnail/01.jpg" class="profile"
                                                    alt="">
                                            </div>
                                            <ul class="dropdown-menu">
                                                <li><a class="dropdown-item" href="products.html">Products</a></li>
                                                <li><a class="dropdown-item" href="ads.html">Ads</a></li>
                                                <li><a class="dropdown-item" href="#" id="logoutBtn">Logout</a>
                                            </ul>
                                        </div>
                                    </span>
                                </div>
                            </nav>
                        </div>
                        <!--menu end-->
                    </div>
                </div>
            </div>
        </header>

        <!--header end-->


        <!--body content start-->

        <div class="page-content">

            <!--product details start-->

            <section class="company">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-12 mt-5 mt-lg-0">
                            <div class="d-flex justify-content-between mb-4">
                                <h3>Company Info</h3>
                                <a href="edit-company.html"> <button class="btn btn-primary">Edit Company</button></a>
                            </div>
                            <div class="d-md-flex product-details">
                                <div class="d-flex justify-content-center me-3">
                                    <div class="c-img mb-3">
                                        <img src="assets/images/furniture-company.jpg" alt="">
                                        <div class="vip">
                                            <img src="assets/images/luxury-vip-badge.webp" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex">
                                    <div class="me-4">
                                        <h5 class="link-title mb-0">Chinese Office furniture manufacturer seeking
                                            distributor in africa</h5>
                                        <p class="link-title mb-0">Opportunities: Sales agent, full-time markter,
                                            distributor</p>
                                        <p class="link-title mb-0">Business Category: Furniture, Building Material</p>
                                        <p class="link-title mb-0">Products: Sofa wooden, Bed, Wardrobe, Dining table
                                        </p>
                                        <p class="link-title mb-0">Zone of interest: Nigeria, Ghana, south africa</p>
                                        <div class="mt-3">
                                            <h6 class="link-title mb-0">Supplier: Shandong xaion xinhua office
                                                furniture Co., Ltd.</h6>
                                            <p class="link-title mb-0">Location: <img
                                                    src="assets/images/china-flag.jpg" style="width: 30px;"
                                                    alt=""> China</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="upgrade">
                                <button class="btn btn-success">Updrade to VIP</button>
                            </div>
                            <div class="about mt-5">
                                <h4>About Us</h4>
                                <p class="mb-4">Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula
                                    suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada Nulla eget sem
                                    vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec eleifend
                                    Pellentesque eu quam sem, ac malesuada Nulla eget sem vitae eros pharetra viverra
                                    Nam vitae luctus ligula suscipit risus nec eleifend Pellentesque eu quam sem, ac
                                    malesuada Nulla eget sem vitae eros pharetra viverra Nam vitae luctus ligula
                                    suscipit risus nec eleifend Pellentesque eu quam sem, ac malesuada Nulla eget sem
                                    vitae eros pharetra viverra Nam vitae luctus ligula suscipit risus nec eleifend
                                    Pellentesque eu quam sem, ac malesuada</p>
                            </div>
                            <!--product ad start-->

                            <div class="my-5">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="position-relative rounded overflow-hidden">
                                            <!-- Background -->
                                            <img class="img-fluid hover-zoom ad-img"
                                                src="assets/images/product-ad/07.jpg" alt="">
                                            <!-- Body -->
                                            <div class="position-absolute top-50 translate-middle-y ps-5">
                                                <h6 class="text-dark">2024 Collection</h6>
                                                <!-- Heading -->
                                                <h3>New Stylish Trend<br>Running Shoe</h3>
                                                <!-- Link --> <a class="btn btn-sm btn-primary btn-animated"
                                                    href="#">Shop Now
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--product ad end-->
                            <div class="others">
                                <div class="mb-5">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="inspection">
                                                <h4>Inspection Reports</h4>
                                                <div class="d-flex align-items-center">
                                                    <div class="pdf me-3">
                                                        <img src="assets/images/pdf.png" alt="">
                                                    </div>
                                                    <div class="pdf">
                                                        <p>Inspection Date: 25th may 2024</p>
                                                        <p>Inspector: Sino Inspection</p>
                                                        <button class="btn btn-outline-primary btn-sm mt-2">View the
                                                            report details</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="inspection">
                                                <h4>Product Catalogue</h4>
                                                <div class="d-flex align-items-center">
                                                    <div class="pdf me-3">
                                                        <img src="assets/images/pdf.png" alt="">
                                                    </div>
                                                    <div class="pdf">
                                                        <button class="btn btn-outline-primary btn-sm mt-2">View the
                                                            catalogue online</button> <br>
                                                        <button class="btn btn-outline-primary btn-sm mt-2">Download
                                                            the catalogue</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="video">
                                    <h5>VIP only</h5>
                                    <iframe class="w-100" height="315"
                                        src="https://www.youtube.com/embed/ABTdTTnnEU8?si=ttIOs4TlPcMtho5e"
                                        title="YouTube video player" frameborder="0"
                                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                                        referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
                                </div>
                                <div class=" brand-news shadow mt-3 p-5">
                                    <h4>Brand News</h4>
                                    <ul>
                                        <li>Global Markets Rally Amid Economic Optimism</li>
                                        <li>New Tech Innovations Unveiled at Major Conference</li>
                                        <li>Climate Change Summit Highlights Urgent Action Needed</li>
                                        <li>Breakthrough in Medical Research Offers Hope for Cure</li>
                                        <li>Historic Peace Agreement Signed in Conflict Zone</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!--product details end-->
        </div>

        <!--body content end-->


        <!--footer start-->

        <footer class="py-11 bg-dark">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-3"> <a class="footer-logo text-white h2 mb-0" href="index.html">
                            Sales<span class="text-primary">Africa</span>
                        </a>
                        <p class="my-3 text-muted">Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                            Asperiores, suscipit iusto! Sequi recusandae perspiciatis sit incidunt? Iure doloribus iste
                            natus?.</p>
                        <ul class="list-inline mb-0">
                            <li class="list-inline-item"><a class="text-light ic-2x" href="#"><i
                                        class="la la-facebook"></i></a>
                            </li>
                            <li class="list-inline-item"><a class="text-light ic-2x" href="#"><i
                                        class="la la-dribbble"></i></a>
                            </li>
                            <li class="list-inline-item"><a class="text-light ic-2x" href="#"><i
                                        class="la la-instagram"></i></a>
                            </li>
                            <li class="list-inline-item"><a class="text-light ic-2x" href="#"><i
                                        class="la la-twitter"></i></a>
                            </li>
                            <li class="list-inline-item"><a class="text-light ic-2x" href="#"><i
                                        class="la la-linkedin"></i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12 col-lg-6 mt-6 mt-lg-0">
                        <div class="row">
                            <div class="col-12 col-sm-4 navbar-dark">
                                <h5 class="mb-4 text-white">Quick Links</h5>
                                <ul class="navbar-nav list-unstyled mb-0">
                                    <li class="mb-3 nav-item"><a class="nav-link" href="index.html">Home</a>
                                    </li>
                                    <li class="mb-3 nav-item"><a class="nav-link" href="about-us-1.html">About</a>
                                    </li>
                                    <li class="mb-3 nav-item"><a class="nav-link" href="faq.html">Faq</a>
                                    </li>
                                    <li class="nav-item"><a class="nav-link" href="contact-us.html">Contact Us</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-4 mt-6 mt-sm-0 navbar-dark">
                                <h5 class="mb-4 text-white">Top Products</h5>
                                <ul class="navbar-nav list-unstyled mb-0">
                                    <li class="mb-3 nav-item"><a class="nav-link" href="#">T-Shirts</a>
                                    </li>
                                    <li class="mb-3 nav-item"><a class="nav-link" href="#">Sneakers &
                                            Athletic</a>
                                    </li>
                                    <li class="mb-3 nav-item"><a class="nav-link" href="#">Shirts & Tops</a>
                                    </li>
                                    <li class="mb-3 nav-item"><a class="nav-link" href="#">Sunglasses</a>
                                    </li>
                                    <li class="mb-3 nav-item"><a class="nav-link" href="#">Bags & Wallets</a>
                                    </li>
                                    <li class="mb-3 nav-item"><a class="nav-link" href="#">Accessories</a>
                                    </li>
                                    <li class="nav-item"><a class="nav-link" href="#">Shoes</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-4 mt-6 mt-sm-0 navbar-dark">
                                <h5 class="mb-4 text-white">Features</h5>
                                <ul class="navbar-nav list-unstyled mb-0">
                                    <li class="mb-3 nav-item"><a class="nav-link"
                                            href="terms-and-conditions.html">Term Of Service</a>
                                    </li>
                                    <li class="mb-3 nav-item"><a class="nav-link" href="privacy-policy.html">Privacy
                                            Policy</a>
                                    </li>
                                    <li class="mb-3 nav-item"><a class="nav-link" href="#">Support</a>
                                    </li>
                                    <li class="mb-3 nav-item"><a class="nav-link" href="#">Shipping &
                                            Returns</a>
                                    </li>
                                    <li class="mb-3 nav-item"><a class="nav-link" href="#">Careers</a>
                                    </li>
                                    <li class="nav-item"><a class="nav-link" href="#">Our Story</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-3 mt-6 mt-lg-0">
                        <div class="d-flex mb-3">
                            <div class="me-2"> <i class="las la-map ic-2x text-primary"></i>
                            </div>
                            <div>
                                <h6 class="mb-1 text-light">Store address</h6>
                                <p class="mb-0 text-muted">423B, Road Wordwide Country, Africa</p>
                            </div>
                        </div>
                        <div class="d-flex mb-3">
                            <div class="me-2"> <i class="las la-envelope ic-2x text-primary"></i>
                            </div>
                            <div>
                                <h6 class="mb-1 text-light">Email Us</h6>
                                <a class="text-muted" href="mailto:themeht23@gmail.com"> themeht23@gmail.com</a>
                            </div>
                        </div>
                        <div class="d-flex mb-3">
                            <div class="me-2"> <i class="las la-mobile ic-2x text-primary"></i>
                            </div>
                            <div>
                                <h6 class="mb-1 text-light">Phone Number</h6>
                                <a class="text-muted" href="tel:+912345678900">+91-234-567-8900</a>
                            </div>
                        </div>
                        <div class="d-flex">
                            <div class="me-2"> <i class="las la-clock ic-2x text-primary"></i>
                            </div>
                            <div>
                                <h6 class="mb-1 text-light">Working Hours</h6>
                                <span class="text-muted">Mon - Fri: 10AM - 7PM</span>
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="my-8">
                <div class="row text-muted align-items-center">
                    <div class="col-md-7">Copyright ©2024 All rights reserved</u>
                    </div>
                    <div class="col-md-5 text-md-end mt-3 mt-md-0">
                        <ul class="list-inline mb-0">
                            <li class="list-inline-item">
                                <a href="#">
                                    <img class="img-fluid" src="assets/images/pay-icon/01.png" alt="">
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#">
                                    <img class="img-fluid" src="assets/images/pay-icon/02.png" alt="">
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#">
                                    <img class="img-fluid" src="assets/images/pay-icon/03.png" alt="">
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a href="#">
                                    <img class="img-fluid" src="assets/images/pay-icon/04.png" alt="">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>

        <!--footer end-->

    </div>

    <!-- page wrapper end -->


    <!--back-to-top start-->

    <div class="scroll-top"><a class="smoothscroll" href="#top"><i class="las la-angle-up"></i></a></div>

    <!--back-to-top end-->

    <!-- inject js start -->

    <script src="assets/js/theme-plugin.js"></script>
    <script src="assets/js/theme-script.js"></script>

    <!-- inject js end -->
    <!-- Ensure jQuery is included before this script if you're using it -->
    <script>
        // Assuming you have jQuery included
        $(document).ready(function() {
            $('#logoutBtn').click(function(e) {
                e.preventDefault();
                $.post('{{ route('logout') }}', {
                    _token: '{{ csrf_token() }}'
                }).done(function() {
                    window.location.href = '/'; // Redirect to homepage or another page after logout
                });
            });
        });
    </script>

</body>

</html>
