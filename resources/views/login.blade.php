<!DOCTYPE html>
<html lang="en">

<head>

    <!-- meta tags -->
    <meta charset="utf-8">
    <meta name="keywords" content="bootstrap 5, premium, multipurpose, sass, scss, saas, eCommerce, Shop, Fashion" />
    <meta name="description" content="Bootstrap 5 Landing Page Template" />
    <meta name="author" content="www.themeht.com" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Title -->
    <title>SalesAfrica</title>

    {{-- <!-- Favicon Icon -->
<link rel="shortcut icon" href="assets/images/favicon.ico" />

<!-- inject css start -->

<link href="assets/css/theme-plugin.css" rel="stylesheet" />
<link href="assets/css/theme.min.css" rel="stylesheet" /> --}}

    <link rel="shortcut icon" href="{{ asset('assets/images/favicon.ico') }}" />

    <!-- inject css start -->
    <link href="{{ asset('assets/css/theme-plugin.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/css/theme.min.css') }}" rel="stylesheet" />

    <!-- inject css end -->

</head>

<body>

    <!-- page wrapper start -->

    <div class="page-wrapper">
        <!--body content start-->

        <div class="page-content">

            <!--login start-->



            <section>
                <div class="container">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                    <div class="row align-items-center">
                        <div class="col-lg-7 col-12">
                            <img class="img-fluid" src="assets/images/login.png" alt="">
                        </div>
                        <div class="col-lg-5 col-12">
                            <div>
                                <h3 class="text-center mb-3 text-uppercase">User Login</h3>
                                <form id="contact-form" method="post" action="{{ route('login.post') }}">
                                    @csrf

                                    <div class="messages"></div>
                                    <div class="form-group">
                                        <input id="form_name" type="email" name="email" class="form-control"
                                            placeholder="User name" required="required"
                                            data-error="Username is required.">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="form-group">
                                        <input id="form_password" type="password" name="password" class="form-control"
                                            placeholder="Password" required="required"
                                            data-error="password is required.">
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="form-group mt-4 mb-5">
                                        <div
                                            class="remember-checkbox d-flex align-items-center justify-content-between">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" value=""
                                                    id="check1">
                                                <label class="form-check-label" for="check1">Remember me</label>
                                            </div><a class="btn-link" href="{{ route('forgot-password') }}">Forgot Password?</a>

                                        </div>
                                        <div class="mt-5">
                                            <button type="submit" class="btn btn-primary btn-block">Login Now</button>

                                        </div>

                                </form>
                                <div class="d-flex align-items-center text-center justify-content-center mt-4"> <span
                                        class="text-muted me-1">Don't have an account?</span>
                                    <a href="{{ route('signup') }}">Sign Up</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <!--login end-->

        </div>

        <!--body content end-->

    </div>

    <!-- page wrapper end -->

    <!-- inject js start -->

    {{-- <script src="{{ asset('assets/js/theme-plugin.js') }}"></script>
<script src="{{ asset('assets/js/theme-script.js') }}"></script> --}}

    <!-- inject js end -->

</body>

</html>
