@include('admin.layouts.header')

<div class="container-scroller">
    @include('admin.layouts.navbar')
    <div class="container-fluid page-body-wrapper">
        @include('admin.layouts.sidebar')
        <div class="main-panel">
            <div class="content-wrapper">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif

                <div class="col-lg-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Update Profile</h4>
                            <div class="row justify-content-end mx-4">
                                <form class="form-sample" method="POST" action="{{ route('admin.profile.update', ['id' => auth()->user()->id]) }}" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="user_id" value="{{ auth()->user()->id }}">
                                    <div class="row">
                                        <div class="col-md-6 col-form-label">
                                            <div class="form-group row">
                                                <label class="col-sm-3">Name <span class="text-danger">*</span></label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" name="name" placeholder="Enter Your Name"
                                                        value="{{ old('name', auth()->user()->name) }}" readonly/>
                                                    @error('name')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-form-label">
                                            <div class="form-group row">
                                                <label class="col-sm-3">Email <span class="text-danger">*</span></label>
                                                <div class="col-sm-9">
                                                    <input type="email" class="form-control" name="email" placeholder="Enter Your Email Address"
                                                        value="{{ old('email', auth()->user()->email) }}" readonly/>
                                                    @error('email')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                
                                    <div class="row">
                                        <div class="col-md-6 col-form-label">
                                            <div class="form-group row">
                                                <label class="col-sm-3">Phone <span class="text-danger">*</span></label>
                                                <div class="col-sm-9">
                                                    <input type="text" name="phone" class="form-control" placeholder="Enter Your Phone No"
                                                        value="{{ old('phone', auth()->user()->phone) }}" readonly>
                                                    @error('phone')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-form-label">
                                            <div class="form-group row">
                                                <label class="col-sm-3">Photo</label>
                                                <div class="col-sm-9">
                                                    @if(auth()->user()->admin && auth()->user()->admin->image)
                                                        <img src="{{ asset('uploads/' . auth()->user()->admin->image) }}" alt="Current Photo" style="max-width: 100px; max-height: 100px; margin-bottom: 10px;">
                                                    @endif
                                                    <input type="file" name="image" class="form-control" value="{{ old('image') }}"/>
                                                    @error('image')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                
                                    <div class="row">
                                        @php
                                        $countries = [
                                        'Algeria',
                                        'Angola',
                                        'Benin',
                                        'Botswana',
                                        'Burkina Faso',
                                        'Burundi',
                                        'Cabo Verde',
                                        'Cameroon',
                                        'Central African Republic',
                                        'Chad',
                                        'Comoros',
                                        'Congo (Brazzaville)',
                                        'Congo (Kinshasa)',
                                        'Djibouti',
                                        'Egypt',
                                        'Equatorial Guinea',
                                        'Eritrea',
                                        'Eswatini',
                                        'Ethiopia',
                                        'Gabon',
                                        'Gambia',
                                        'Ghana',
                                        'Guinea',
                                        'Guinea-Bissau',
                                        'Ivory Coast',
                                        'Kenya',
                                        'Lesotho',
                                        'Liberia',
                                        'Libya',
                                        'Madagascar',
                                        'Malawi',
                                        'Mali',
                                        'Mauritania',
                                        'Mauritius',
                                        'Morocco',
                                        'Mozambique',
                                        'Namibia',
                                        'Niger',
                                        'Nigeria',
                                        'Rwanda',
                                        'Sao Tome and Principe',
                                        'Senegal',
                                        'Seychelles',
                                        'Sierra Leone',
                                        'Somalia',
                                        'South Africa',
                                        'South Sudan',
                                        'Sudan',
                                        'Tanzania',
                                        'Togo',
                                        'Tunisia',
                                        'Uganda',
                                        'Zambia',
                                        'Zimbabwe',
                                        ];
                                        $cities = [
                                        'Lagos',
                                        'Abuja',
                                        'Kano',
                                        'Ibadan',
                                        'Port Harcourt',
                                        'Benin City',
                                        'Maiduguri',
                                        'Zaria',
                                        'Aba',
                                        'Jos',
                                        ];
                                        @endphp
                                
                                        <div class="col-md-6 col-form-label">
                                            <div class="form-group row">
                                                <label class="col-sm-3">Country <span class="text-danger">*</span></label>
                                                <div class="col-sm-9">
                                                    <select class="form-control form-select" name="country">
                                                        <option value="">Select Country</option>
                                                        @foreach ($countries as $country)
                                                            <option value="{{ $country }}"
                                                                {{ old('country', auth()->user()->admin->country) == $country ? 'selected' : '' }}>
                                                                {{ $country }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                    @error('country')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-form-label">
                                            <div class="form-group row">
                                                <label class="col-sm-3">City <span class="text-danger">*</span></label>
                                                <div class="col-sm-9">
                                                    <select name="city" class="form-control form-select">
                                                        <option value="">Select City</option>
                                                        @foreach ($cities as $city)
                                                            <option value="{{ $city }}"
                                                                {{ old('city', auth()->user()->admin->city) == $city ? 'selected' : '' }}>
                                                                {{ $city }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                    @error('city')
                                                        <div class="text-danger">{{ $message }}</div>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                
                                    <button class="btn btn-primary">Save</button>
                                </form>
                            </div>
                            <p class="card-description"></p>
                        </div>
                    </div>
                </div>
            </div>
            @include('admin.layouts.footer')
