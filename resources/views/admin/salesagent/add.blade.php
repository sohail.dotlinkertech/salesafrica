@include('admin.layouts.header')

<div class="container-scroller">

    @include('admin.layouts.navbar')
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">

        @include('admin.layouts.sidebar')
        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">
                <div class="row">
                    <div class="col-12 grid-margin">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Add Sales Agent</h4>
                                <form action="{{ route('admin.sales-agent.store') }}" method="POST"
                                    enctype="multipart/form-data">
                                    @csrf


                                    <div class="row">
                                        <div class="col-md-6 col-form-label">
                                            <div class="form-group row">
                                                <label class="col-sm-3">First Name <span
                                                        class="text-danger"></span></label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" name="first_name"
                                                        placeholder="Enter Your First Name"
                                                        value="{{ old('first_name') }}" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-form-label">
                                            <div class="form-group row">
                                                <label class="col-sm-3">Last Name <span
                                                        class="text-danger"></span></label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" name="last_name"
                                                        placeholder="Enter Your Last Name"
                                                        value="{{ old('last_name') }}" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6 col-form-label">
                                            <div class="form-group row">
                                                <label class="col-sm-3">Company</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" name="company"
                                                        placeholder="Enter Your Company Name (Optional)"
                                                        value="{{ old('company') }}" />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6 col-form-label">
                                            <div class="form-group row">
                                                <label class="col-sm-3">Date of Birth <span
                                                        class="text-danger">*</span></label>
                                                <div class="col-sm-9">
                                                    <input type="date" class="form-control" name="date_of_birth"
                                                        value="{{ old('date_of_birth') }}" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-md-6 col-form-label">
                                            <div class="form-group row">
                                                <label class="col-sm-3">Phone <span class="text-danger">*</span></label>
                                                <div class="col-sm-6">
                                                    <input type="phone" class="form-control" name="phone"
                                                        placeholder="Enter Your Phone" value="{{ old('phone') }}" />
                                                    @error('phone')
                                                        <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                                <div class="col-sm-1">
                                                    <button class="btn btn-secondary" type="button"
                                                        onclick="sendOTP()">Send&nbsp;OTP</button>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3">OTP</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" name="phone_otp"
                                                        placeholder="Enter OTP" value="{{ old('phone_otp') }}" />
                                                    @error('phone_otp')
                                                        <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6 col-form-label">
                                            <div class="form-group row">
                                                <label class="col-sm-3">Email <span class="text-danger">*</span></label>
                                                <div class="col-sm-6">
                                                    <input type="email" class="form-control" name="email"
                                                        placeholder="Enter Your Email Address"
                                                        value="{{ old('email') }}" />
                                                    @error('email')
                                                        <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                                <div class="col-sm-1">
                                                    <button class="btn btn-secondary" type="button"
                                                        onclick="sendEmailVerification()">Verify</button>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="col-sm-3">Email OTP</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" name="email_otp"
                                                        placeholder="Enter Email OTP" value="{{ old('email_otp') }}" />
                                                    @error('email_otp')
                                                        <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        @php
                                            $countries = [
                                                'Algeria',
                                                'Angola',
                                                'Benin',
                                                'Botswana',
                                                'Burkina Faso',
                                                'Burundi',
                                                'Cabo Verde',
                                                'Cameroon',
                                                'Central African Republic',
                                                'Chad',
                                                'Comoros',
                                                'Congo (Brazzaville)',
                                                'Congo (Kinshasa)',
                                                'Djibouti',
                                                'Egypt',
                                                'Equatorial Guinea',
                                                'Eritrea',
                                                'Eswatini',
                                                'Ethiopia',
                                                'Gabon',
                                                'Gambia',
                                                'Ghana',
                                                'Guinea',
                                                'Guinea-Bissau',
                                                'Ivory Coast',
                                                'Kenya',
                                                'Lesotho',
                                                'Liberia',
                                                'Libya',
                                                'Madagascar',
                                                'Malawi',
                                                'Mali',
                                                'Mauritania',
                                                'Mauritius',
                                                'Morocco',
                                                'Mozambique',
                                                'Namibia',
                                                'Niger',
                                                'Nigeria',
                                                'Rwanda',
                                                'Sao Tome and Principe',
                                                'Senegal',
                                                'Seychelles',
                                                'Sierra Leone',
                                                'Somalia',
                                                'South Africa',
                                                'South Sudan',
                                                'Sudan',
                                                'Tanzania',
                                                'Togo',
                                                'Tunisia',
                                                'Uganda',
                                                'Zambia',
                                                'Zimbabwe',
                                            ];
                                            $cities = [
                                                'Lagos',
                                                'Abuja',
                                                'Kano',
                                                'Ibadan',
                                                'Port Harcourt',
                                                'Benin City',
                                                'Maiduguri',
                                                'Zaria',
                                                'Aba',
                                                'Jos',
                                            ];
                                        @endphp
                                        <div class="col-md-6 col-form-label">
                                            <div class="form-group row">
                                                <label class="col-sm-3">Country <span
                                                        class="text-danger">*</span></label>
                                                <div class="col-sm-9">
                                                    <select class="form-control form-select" name="country">
                                                        <option value="">Select Country</option>



                                                        @foreach ($countries as $country)
                                                            <option value="{{ $country }}"
                                                                {{ old('country') == $country ? 'selected' : '' }}>
                                                                {{ $country }}
                                                            </option>
                                                        @endforeach


                                                    </select>
                                                    @error('country')
                                                        <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>

                                            </div>
                                        </div>

                                        <div class="col-md-6 col-form-label">
                                            <div class="form-group row">
                                                <label class="col-sm-3 ">City <span
                                                        class="text-danger">*</span></label>
                                                <div class="col-sm-9">
                                                    <select name="city" id=""
                                                        class="form-control form-select">
                                                        <option value="">Select a city</option>
                                                        @foreach ($cities as $city)
                                                            <option value="{{ $city }}"
                                                                {{ old('city') == $city ? 'selected' : '' }}>
                                                                {{ $city }}
                                                            </option>
                                                        @endforeach

                                                    </select>
                                                    @error('city')
                                                        <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="col-md-6 col-form-label">
                                            <div class="form-group row">
                                                <label class="col-sm-3">Industries <span
                                                        class="text-danger">*</span></label>
                                                <div class="col-sm-9">
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox"
                                                            id="tech" name="industries[]" value="technology">
                                                        <label class="form-check-label"
                                                            for="tech">Technology</label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox"
                                                            id="healthcare" name="industries[]" value="healthcare">
                                                        <label class="form-check-label"
                                                            for="healthcare">Healthcare</label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox"
                                                            id="finance" name="industries[]" value="finance">
                                                        <label class="form-check-label" for="finance">Finance</label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox"
                                                            id="education" name="industries[]" value="education">
                                                        <label class="form-check-label"
                                                            for="education">Education</label>
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="checkbox"
                                                            id="retail" name="industries[]" value="retail">
                                                        <label class="form-check-label" for="retail">Retail</label>
                                                    </div>
                                                    @error('industries')
                                                        <span class="text-danger">{{ $message }}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6 col-form-label">
                                            <div class="form-group row">
                                                <label class="col-sm-3">Introduction <span
                                                        class="text-danger">*</span></label>
                                                <div class="col-sm-9">
                                                    <textarea class="form-control" name="introduction" placeholder="Write your brief introduction or cover letter here"
                                                        maxlength="2000" style="height: 4rem;"></textarea>
                                                    <small class="form-text text-muted">Maximum 2000
                                                        characters.</small>
                                                </div>
                                                @error('introduction')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary">Save</button>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- content-wrapper ends -->

            {{-- @include('admin.layouts.footer') --}}
            <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>


            <script>
                // Your JavaScript in Blade template
function sendOTP() {
    var phone = document.querySelector('input[name="phone"]').value;
    fetch('{{ route('send.phone.otp') }}', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute('content')
        },
        body: JSON.stringify({ phone: phone })
    })
    .then(response => response.json())
    .then(data => {
        alert(data.message);
    })
    .catch(error => console.error('Error:', error));
}

function sendEmailVerification() {
    var email = document.querySelector('input[name="email"]').value;
    fetch('{{ route('send.email.verification') }}', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute('content')
        },
        body: JSON.stringify({ email: email })
    })
    .then(response => response.json())
    .then(data => {
        alert(data.message);
    })
    .catch(error => console.error('Error:', error));
}

            </script>
            