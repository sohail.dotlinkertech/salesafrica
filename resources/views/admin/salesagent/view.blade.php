@include('admin.layouts.header')

<div class="container-scroller">

    @include('admin.layouts.navbar')
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">

        @include('admin.layouts.sidebar')
        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif

                <div class="row">
                    <div class="col-lg-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Sales Agent</h4>
                                <div class="row mb-3 justify-content-end">
                                    <div class="col-auto">
                                        <a href="{{ route('admin.add_sales-agent') }}">
                                            <button class="btn btn-primary">Add</button>
                                        </a>
                                    </div>
                                </div>
                                <p class="card-description">

                                </p>
                                <div class="row">
                                    <div class="col-md-4">
                                        <input type="text" placeholder="search" class="form-control" id="search">
                                    </div>

                                    <div class="col-md-2"></div>

                                    <div class="col-md-2">

                                        <select class="form-control form-select">
                                            <option value="">Select Country</option>
                                            <option value="Algeria">Algeria</option>
                                            <option value="Angola">Angola</option>
                                            <option value="Benin">Benin</option>
                                            <option value="Botswana">Botswana</option>
                                            <option value="Burkina Faso">Burkina Faso</option>
                                            <option value="Burundi">Burundi</option>
                                            <option value="Cabo Verde">Cabo Verde</option>
                                            <option value="Cameroon">Cameroon</option>
                                            <option value="Central African Republic">Central African Republic</option>
                                            <option value="Chad">Chad</option>
                                            <option value="Comoros">Comoros</option>
                                            <option value="Congo (Brazzaville)">Congo (Brazzaville)</option>
                                            <option value="Congo (Kinshasa)">Congo (Kinshasa)</option>
                                            <option value="Djibouti">Djibouti</option>
                                            <option value="Egypt">Egypt</option>
                                            <option value="Equatorial Guinea">Equatorial Guinea</option>
                                            <option value="Eritrea">Eritrea</option>
                                            <option value="Eswatini">Eswatini</option>
                                            <option value="Ethiopia">Ethiopia</option>
                                            <option value="Gabon">Gabon</option>
                                            <option value="Gambia">Gambia</option>
                                            <option value="Ghana">Ghana</option>
                                            <option value="Guinea">Guinea</option>
                                            <option value="Guinea-Bissau">Guinea-Bissau</option>
                                            <option value="Ivory Coast">Ivory Coast</option>
                                            <option value="Kenya">Kenya</option>
                                            <option value="Lesotho">Lesotho</option>
                                            <option value="Liberia">Liberia</option>
                                            <option value="Libya">Libya</option>
                                            <option value="Madagascar">Madagascar</option>
                                            <option value="Malawi">Malawi</option>
                                            <option value="Mali">Mali</option>
                                            <option value="Mauritania">Mauritania</option>
                                            <option value="Mauritius">Mauritius</option>
                                            <option value="Morocco">Morocco</option>
                                            <option value="Mozambique">Mozambique</option>
                                            <option value="Namibia">Namibia</option>
                                            <option value="Niger">Niger</option>
                                            <option value="Nigeria">Nigeria</option>
                                            <option value="Rwanda">Rwanda</option>
                                            <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                            <option value="Senegal">Senegal</option>
                                            <option value="Seychelles">Seychelles</option>
                                            <option value="Sierra Leone">Sierra Leone</option>
                                            <option value="Somalia">Somalia</option>
                                            <option value="South Africa">South Africa</option>
                                            <option value="South Sudan">South Sudan</option>
                                            <option value="Sudan">Sudan</option>
                                            <option value="Tanzania">Tanzania</option>
                                            <option value="Togo">Togo</option>
                                            <option value="Tunisia">Tunisia</option>
                                            <option value="Uganda">Uganda</option>
                                            <option value="Zambia">Zambia</option>
                                            <option value="Zimbabwe">Zimbabwe</option>
                                        </select>

                                    </div>
                                    <div class="col-md-2">

                                        <select name="" id="industries" class='form-control form-select filters'>
                                            <option value="">Select Industries</option>
                                            <option value="technology">Technology</option>
                                            <option value="healthcare">Healthcare</option>
                                            <option value="">Finance</option>
                                            <option value="finance">Finance</option>
                                            <option value="education">Education</option>
                                            <option value="retail">Retail</option>

                                        </select>
                                    </div>
                                    <div class="col-md-2">

                                        <select name="" id="account_type" class='form-control form-select filters'>
                                            <option value="">Select Account Type</option>
                                            <option value="vip">VIP</option>
                                            <option value="standard">Standard</option>
                                        </select>
                                    </div>


                                    <div class="table-responsive pt-3">
                                        <table class="table table-bordered text-center">
                                            <thead>
                                                <tr>
                                                    <th>
                                                        #
                                                    </th>
                                                    <th>
                                                        Name
                                                    </th>


                                                    <th>
                                                        Documents
                                                    </th>
                                                    <th>
                                                        Account Type
                                                    </th>
                                                    <th>Status</th>

                                                    <th>
                                                        Action
                                                    </th>
                                                </tr>
                                            </thead>


                                            <tbody>
                                                @include('admin.salesagent.salesAgent_filtering', [
                                                    'salesAgents' => $salesAgents,
                                                ])
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- content-wrapper ends -->

                @include('admin.layouts.footer')


                <script>
                    $(document).ready(function() {
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });

                        $('.filters, #search').on('change keyup', function() {
                            var data = {
                                country: $('#country').val(),
                                industries: $('#industries').val(),
                                account_type: $('#account_type').val(),
                                search: $('#search').val(),
                            };

                            $.ajax({
                                url: "{{ route('filter.salesAgent') }}",
                                type: 'POST',
                                data: data,
                                success: function(response) {
                                    $('tbody').html(response.html);
                                },
                                error: function(xhr) {
                                    console.log(xhr.responseText);
                                }
                            });
                        });
                    });
                </script>
