@if ($salesAgents->isEmpty())
    <tr>
        <td colspan="6">No Sales Agents Found</td>
    </tr>
    <script>
        setTimeout(function() {
            location.reload();
        }, 5000);
    </script>
@else
    @foreach ($salesAgents as $index => $salesAgent)
        <tr>
            <td>{{ $index + 1 }}</td>

            <td>{{ $salesAgent->first_name }} {{ $salesAgent->last_name }}
            </td>
            
            <td>-</td>
        
            <td style="text-align:center; @if ($salesAgent->account_type == 'vip') color: green; @endif">
                <strong>{{ $salesAgent->account_type }}</strong>
            </td>
            

            <td>
                @if ($salesAgent->status == 1)
                    <button id="approved{{ $salesAgent->user_id }}" class="btn btn-success">Approved</button>

                @else
                <button id="not_approved{{ $salesAgent->user_id }}"
                    class="btn btn-danger not_approved"
                    data-user-id="{{ $salesAgent->user_id }}"
                    style="background-color:rgb(198, 8, 8)">Not
                    Approved</button>
                @endif
            </td>


            <td>
                <a href="{{ route('admin.edit.sales-agent', $salesAgent->id) }}" class="btn btn-primary">Edit</a>
             
                <form action="{{ route('admin.delete.sales-agent', $salesAgent->id) }}" method="POST" class="d-inline">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger"
                        onclick="return confirm('Are you sure you want to delete this Sales Agent?')">Delete</button>
                </form>



            </td>
        </tr>
    @endforeach
@endif
