@if ($distributors->isEmpty())
    <tr>
        <td colspan="6">No Distributors Found</td>
    </tr>
    <script>
        setTimeout(function() {
            location.reload();
        }, 5000);
    </script>
@else
    @foreach ($distributors as $index => $distributor)
        <tr>
            <td>{{ $index + 1 }}</td>
            <td>{{ $distributor->company }}</td>
            <td>
                @if ($distributor->company_registration_paper)
                    <a href="{{ asset('uploads/company_registration_paper/' . $distributor->company_registration_paper) }}" target="_blank">
                        <img src="{{ asset('uploads/company_registration_paper/' . $distributor->company_registration_paper) }}" alt="company_registration_paper" style="width: 80px; height: 80px;">
                    </a>
                @else
                    No company registration paper
                @endif
            </td>
            
            <td style="text-align:center; @if ($distributor->account_type == 'vip') color: green; @endif">
                <strong>{{ $distributor->account_type }}</strong>
            </td>


            <td>
                @if ($distributor->status == 1)
                    <button id="approved{{ $distributor->user_id }}" class="btn btn-success">Approved</button>
                @else
                    <button id="not_approved{{ $distributor->user_id }}" class="btn btn-danger not_approved" 
                        data-user-id="{{ $distributor->user_id }}" style="background-color:rgb(198, 8, 8)">Not Approved</button>
                @endif
            </td>
            
            <td>
                <a href="{{ url('admin/edit/distributors/' . $distributor->id) }}">
                    <button class="btn btn-primary">Edit</button>
                </a>
                <form action="{{ route('admin.delete.distributor', $distributor->id) }}" method="POST" class="d-inline">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger"
                        onclick="return confirm('Are you sure you want to delete this Distributor?')">Delete</button>
                </form>
            </td>
        </tr>
    @endforeach

@endif
