@if ($suppliers->isEmpty())
    <tr>
        <td colspan="6">No Suppliers Found</td>
    </tr>
    <script>
        setTimeout(function() {
            location.reload();
        }, 5000);
    </script>
@else
    @foreach ($suppliers as $index => $supplier)
        <tr>
            <td>{{ $index + 1 }}</td>

            <td>{{ $supplier->company }}</td>

            <td>
                @if ($supplier->inspection_report)
                    <img src="{{ asset('uploads/inspection_reports/' . $supplier->inspection_report) }}"
                        alt="Inspection Report" style="width: 100px; height: auto;">
                @else
                    No Inspection Report
                @endif
            </td>
            <td>
                @if ($supplier->letter_of_authorization)
                    <img src="{{ asset('uploads/authorization_letters/' . $supplier->letter_of_authorization) }}"
                        alt="Letter of Authorization" style="width: 100px; height: auto;">
                @else
                    No Letter of Authorization
                @endif
            </td>


            <td style="text-align:center; @if ($supplier->account_type == 'vip') color: green; @endif">
                <strong>{{ $supplier->account_type }}</strong>
            </td>

            <td>
                @if ($supplier->status == 1)
                    <button id="approved{{ $supplier->user_id }}" class="btn btn-success">Approved</button>
                @else
                    <button id="not_approved{{ $supplier->user_id }}" class="btn btn-danger not_approved"
                        data-user-id="{{ $supplier->user_id }}" style="background-color:rgb(198, 8, 8)">Not
                        Approved</button>
                @endif
            </td>

            <td>
                <a href="{{ route('admin.edit.supplier', $supplier->id) }}" class="btn btn-primary">Edit</a>

                <form action="{{ route('admin.delete.supplier', $supplier->id) }}" method="POST" class="d-inline">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger"
                        onclick="return confirm('Are you sure you want to delete this supplier?')">Delete</button>
                </form>
            </td>
        </tr>
    @endforeach
@endif
