@include('admin.layouts.header')

<div class="container-scroller">

    @include('admin.layouts.navbar')
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">

        @include('admin.layouts.sidebar')
        <!-- partial -->
        <div class="main-panel">
            <div class="content-wrapper">
                <div class="row">
                    <div class="col-lg-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">News Approval</h4>

                                <div class="row">

                                    <div class="col-md-4">
                                        <input type="text" placeholder="search" class="form-control">

                                    </div>
                                    <div class="col-md-4"></div>
                                    <div class="col-md-4">

                                        <select name="" id="" class='form-control'>
                                            <option value="">Filter By Status</option>
                                            <option value="">Approved</option>
                                            <option value="">Not Approved</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="table-responsive pt-3">
                                    <table class="table table-bordered text-center">
                                        <thead>
                                            <tr>
                                                <th>
                                                    #
                                                </th>
                                                <th>
                                                    News Title
                                                </th>
                                                <th>
                                                    Posted By
                                                </th>
                                                <th>
                                                    Thumbnail
                                                </th>
                                                <th>Status</th>

                                                <th>
                                                    Action
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    1
                                                </td>
                                                <td>
                                                    Test
                                                </td>

                                                <td>Posted By Dotlinker</td>

                                                <td><img src="{{ url('images/faces/face1.jpg') }}" alt=""></td>
                                                <td><button class="btn btn-success"> Approved</button></td>
                                                <td>

                                                    <button class="btn btn-danger">Delete</button>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    2
                                                </td>
                                                <td>
                                                    xyz
                                                </td>

                                                <td>Posted by - xyz</td>

                                                <td><img src="{{ url('images/faces/face1.jpg') }}" alt=""></td>
                                                <td><button class="btn btn-danger"
                                                        style="background-color:rgb(198, 8, 8)"> Not Approved</button>
                                                </td>
                                                <td>
                                                    <button class="btn btn-danger">Delete</button>

                                                </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- content-wrapper ends -->

            @include('superadmin.layouts.footer')
