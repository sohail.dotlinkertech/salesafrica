<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Distributor>
 */
class DistributorFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $countries = [
            'Algeria', 'Angola', 'Benin', 'Botswana', 'Burkina Faso', 'Burundi', 'Cabo Verde', 
            'Cameroon', 'Central African Republic', 'Chad', 'Comoros', 'Congo (Brazzaville)', 
            'Congo (Kinshasa)', 'Djibouti', 'Egypt', 'Equatorial Guinea', 'Eritrea', 'Eswatini', 
            'Ethiopia', 'Gabon', 'Gambia', 'Ghana', 'Guinea', 'Guinea-Bissau', 'Ivory Coast', 
            'Kenya', 'Lesotho', 'Liberia', 'Libya', 'Madagascar', 'Malawi', 'Mali', 'Mauritania', 
            'Mauritius', 'Morocco', 'Mozambique', 'Namibia', 'Niger', 'Nigeria', 'Rwanda', 
            'Sao Tome and Principe', 'Senegal', 'Seychelles', 'Sierra Leone', 'Somalia', 
            'South Africa', 'South Sudan', 'Sudan', 'Tanzania', 'Togo', 'Tunisia', 'Uganda', 
            'Zambia', 'Zimbabwe'
        ];
        return [
            // 'user_id' => User::factory(), // Automatically create a User for each Distributor

            'user_id' => User::factory()->state(function (array $attributes) {
                return [
                    'name' => $this->faker->company,
                    'email' => $this->faker->unique()->safeEmail,
                    'role' => 4,
                ];
            }),
            'company' => $this->faker->company,
            'foundation_year' => $this->faker->date('Y-m-d', 'now'),
            'contact_person' => $this->faker->name,
            'title' => $this->faker->jobTitle,
            'phone' => $this->faker->phoneNumber,
            'email' => $this->faker->unique()->safeEmail,
            'country' => $this->faker->randomElement($countries),
            'city' => $this->faker->city,
            'industries' => $this->faker->randomElements(['technology', 'healthcare', 'finance', 'retail'], 2),
            'company_registration_paper' => 'assets/uploads/company_registration_paper/' . $this->faker->image('public/assets/uploads/company_registration_paper', 640, 480, null, false),
            'shop_photos' => 'assets/uploads/shop_photos/' . $this->faker->image('public/assets/uploads/shop_photos', 640, 480, null, false),
            'introduction' => $this->faker->paragraph,
            'account_type' => $this->faker->randomElement(['vip', 'Standard']),
            'status' => $this->faker->randomElement(['1', '0']),
        ];
    }
}
