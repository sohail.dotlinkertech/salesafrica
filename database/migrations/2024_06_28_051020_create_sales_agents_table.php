<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('sales_agents', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->nullable();
            $table->string('company')->nullable();
            $table->integer('salesagent_type')->default(0);
            $table->string('location')->nullable();
            $table->string('buisness_category')->nullable();
            $table->string('company_website')->nullable();
            $table->string('image')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->string('country')->nullable();
            $table->string('city')->nullable();
            $table->string('industries')->nullable();
            $table->string('introduction')->nullable();
            $table->string('account_type')->default('Standard');
            $table->integer('status')->default(0);
            $table->boolean('individual')->default(0);
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('sales_agents');
    }
};
