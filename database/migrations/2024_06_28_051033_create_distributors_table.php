<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('distributors', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->nullable();
            $table->string('company')->nullable();
            $table->integer('distributor_type')->default(0);
            $table->string('location')->nullable();
            $table->string('buisness_category')->nullable();
            $table->string('company_website')->nullable();
            $table->string('image')->nullable();
            $table->date('foundation_year')->nullable();
            $table->string('contact_person')->nullable();
            $table->string('title')->nullable();
            $table->string('country')->nullable();
            $table->string('city')->nullable();
            $table->string('industries')->nullable();
            $table->string('company_registration_paper')->nullable();
            $table->string('shop_photos')->nullable();
            $table->string('introduction')->nullable();
            $table->string('account_type')->default('Standard');
            $table->integer('status')->default(0);
            $table->boolean('individual')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('distributors');
    }
};
