<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\auth\AuthController;
use App\Http\Controllers\auth\ForgotPasswordController;
use App\Http\Controllers\ChatController;
use App\Http\Controllers\Distributor\PostController;
use App\Http\Controllers\Distributor\ProfileController as DistributorProfileController;
use App\Http\Controllers\DistributorController;
use App\Http\Controllers\Frontend\HomeController;
use App\Http\Controllers\QuoteController;
use App\Http\Controllers\SalesAgent\QuoteController as SalesAgentPostController;
use App\Http\Controllers\SalesAgent\ProfileController as SalesAgentProfileController;
use App\Http\Controllers\SalesAgentController;
use App\Http\Controllers\SuperAdminController;
use App\Http\Controllers\Supplier\NewsController;
use App\Http\Controllers\Supplier\ProductController;
use App\Http\Controllers\Supplier\ProfileController;
use App\Http\Controllers\SupplierController;
use App\Http\Controllers\VerificationController;
use App\Models\Distributor;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;


/*
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
*/

Route::get('/', [HomeController::class, 'index'])->name('index');
Route::get('/contactUs', [HomeController::class, 'contactUs'])->name('contactUs');

Route::get('/distributors', [HomeController::class, 'viewAllDistributors'])->name('distributors.view_all');
Route::get('/distributor/home/{id}', [HomeController::class, 'viewDistributorHome'])->name('supplier.distributor.showHome');

Route::get('/salesagents', [HomeController::class, 'viewAllSalesagents'])->name('salesagents.view_all');
Route::get('/salesagent/home/{id}', [HomeController::class, 'showSalesAgentHome'])->name('supplier.salesagent.showHome');

Route::get('/suppliers', [HomeController::class, 'viewAllSuppliers'])->name('suppliers.view_all');
Route::get('/supplier/home/{id}', [HomeController::class, 'showSupplierHome'])->name('supplier.supplier.showHome');


Route::get('/full-description', [HomeController::class, 'fullDescription'])->name('full-description');
Route::get('/blog-single', [HomeController::class, 'blogSingle'])->name('blog-single');



Route::get('/chat/message-supplier', [ChatController::class, 'messageSupplier'])->name('chat.messageSupplier');
Route::post('/chat/send-message', [ChatController::class, 'sendMessage'])->name('chat.sendMessage');
Route::get('/chat/fetch-messages-with-receiver', [ChatController::class, 'fetchMessagesWithReceiver'])->name('chat.fetchMessagesWithReceiver');


Route::get('/chat/message', [ChatController::class, 'messageDistributor'])->name('distributor.message');
Route::get('/message', [ChatController::class, 'messageSalesagent'])->name('salesagent.message');


Route::get('/supplier/chat/{userId}', [ChatController::class, 'messagePage'])->name('supplier.personalMessage');

// ********** Auth Routes *********
Route::get('/login', [AuthController::class, 'login_view'])->name('login');
Route::get('/signup', [AuthController::class, 'register_view'])->name('signup');
Route::post('/signup-post', [AuthController::class, 'signupPost'])->name('signup.post');
Route::post('/loginPost', [AuthController::class, 'loginPost'])->name('login.post');
Route::post('/logout', [AuthController::class, 'logout'])->name('logout');


// Route to show forgot password form
Route::get('/forgot-password', function () {
    return view('auth.forgot-password');
})->name('forgot-password');
Route::post('/forgot-passwordpost', [ForgotPasswordController::class, 'forgotPassword'])->name('forgot-password-post');

Route::get('/reset-password/{token}', [ForgotPasswordController::class, 'resetPassword'])->name('reset-password');
Route::post('/reset-password', [ForgotPasswordController::class, 'resetPasswordPost'])->name('reset-password-post');

/*********************************************************************************
                                supplier routes 
 **********************************************************************************/
Route::prefix('supplier')->middleware(['auth', 'supplier'])->group(function () {

    Route::get('/', [SupplierController::class, 'opportunites'])->name('supplier.opportunites');

    Route::get('/edit', [ProfileController::class, 'editSupplier'])->name('supplier.edit');
    Route::put('/{id}', [ProfileController::class, 'update'])->name('supplier.update');

    Route::get('/myaccount', [SupplierController::class, 'myAccountSupplier'])->name('supplier.myaccount');
    Route::get('/profile', [SupplierController::class, 'profileSupplier'])->name('supplier.profile');

    Route::get('/view', [SupplierController::class, 'viewSupplier'])->name('supplier.view');
    Route::get('/transaction-invoicer', [SupplierController::class, 'transactionInvoicerSupplier'])->name('supplier.transaction-invoicer');
    Route::get('/message', [ChatController::class, 'messageSupplier'])->name('supplier.message');
    Route::get('/my-rfq-supplier', [SupplierController::class, 'myRfqSupplier'])->name('supplier.my-rfq-supplier');
    Route::get('/products', [ProductController::class, 'productsSupplier'])->name('supplier.products');
    Route::get('/add-products', [ProductController::class, 'addProductsSupplier'])->name('supplier.productsAdd');
    Route::post('/store-products', [ProductController::class, 'store'])->name('supplier.products.store');

    Route::get('/quotation', [SupplierController::class, 'quotationSupplier'])->name('supplier.quotation');
    Route::get('/shortlisted-distributor', [SupplierController::class, 'shortlistedDistributorSupplier'])->name('supplier.shortlisted-distributor');
    Route::get('/shortlisted-salesagent', [SupplierController::class, 'shortlistedSalesagentSupplier'])->name('supplier.shortlisted-salesagent');

    Route::get('/brandnews', [SupplierController::class, 'brandnewsSupplier'])->name('supplier.brandnews');
    Route::get('/news/create', [NewsController::class, 'create'])->name('supplier.news.create');
    Route::post('/news/store', [NewsController::class, 'store'])->name('supplier.news.store');

    Route::get('/news/{id}/edit', [NewsController::class, 'editNews'])->name('supplier.news.edit');
    Route::put('/news/{id}', [NewsController::class, 'updateNews'])->name('supplier.news.update');
    Route::delete('/news/{id}', [NewsController::class, 'deleteNews'])->name('supplier.news.destroy');

    Route::get('/salesagent', [SupplierController::class, 'salesagentSupplier'])->name('supplier.salesagent');
    Route::get('/salesagent/{id}', [SupplierController::class, 'viewSalesAgent'])->name('supplier.salesagent.show');

    Route::get('/distributor', [SupplierController::class, 'distributorSupplier'])->name('supplier.distributor');
    Route::get('/distributor/{id}', [SupplierController::class, 'viewDistributor'])->name('supplier.distributor.show');

    Route::get('/suppliers', [SupplierController::class, 'suppliersSupplier'])->name('supplier.suppliers');
    Route::get('/supplier/{id}', [SupplierController::class, 'showSupplier'])->name('supplier.supplier.show');

    Route::get('/subscription', [SupplierController::class, 'subscription'])->name('supplier.subscription');
});

/*********************************************************************************
                                distributor routes 
 **********************************************************************************/
Route::prefix('distributor')->middleware(['auth', 'distributor'])->group(function () {

    Route::get('/', [DistributorController::class, 'dashboard'])->name('distributor.dashboard');
    Route::get('/edit', [DistributorProfileController::class, 'editDistributor'])->name('distributor.edit');
    Route::put('/{id}', [DistributorProfileController::class, 'update'])->name('distributor.update');

    Route::get('/myaccount', [DistributorController::class, 'myAccountDistributor'])->name('distributor.myaccount');
    Route::get('/profile', [DistributorController::class, 'profileDistributor'])->name('distributor.profile');
    Route::get('/view', [DistributorController::class, 'viewDistributor'])->name('distributor.view');
    Route::get('/transaction-invoicer', [DistributorController::class, 'transactionInvoicerDistributor'])->name('distributor.transaction-invoicer');
    Route::get('/my-rfq-sales-agent', [DistributorController::class, 'myRfqSalesAgent'])->name('distributor.my-rfq-sales-agent');


    Route::get('/myquotes', [DistributorController::class, 'myQuotes'])->name('distributor.myPost');
    Route::get('/quotation', [DistributorController::class, 'quotation'])->name('distributor.quotation');
    Route::get('/shortlistedOpprtunity', [DistributorController::class, 'shortlistedOpprtunity'])->name('distributor.shortlistedOpprtunity');
    Route::get('/sales-agent-panel', [DistributorController::class, 'salesAgentPanelDistributor'])->name('distributor.sales-agent-panel');
    Route::get('/subscription', [DistributorController::class, 'subscription'])->name('distributor.subscription');
});

Route::prefix('quotes')->group(function () {

    Route::get('/create', [QuoteController::class, 'create'])->name('quotes.create');
    Route::post('/store', [QuoteController::class, 'store'])->name('quotes.store');
    Route::post('/reply/{id}', [QuoteController::class, 'quoteReply'])->name('quotes.reply');
});
/*********************************************************************************
                                salesagent routes 
 **********************************************************************************/
Route::prefix('salesagent')->middleware(['auth', 'salesagent'])->group(function () {

    Route::get('/', [SalesAgentController::class, 'dashboard'])->name('salesagent.dashboard');
    Route::get('myaccount', [SalesAgentController::class, 'myAccountSalesagent'])->name('salesagent.myaccount');
    Route::get('/edit', [SalesAgentProfileController::class, 'editSalesagent'])->name('salesagent.edit');
    Route::put('/{id}', [SalesAgentProfileController::class, 'update'])->name('salesagent.update');

    Route::get('/profile', [SalesAgentController::class, 'profileSalesagent'])->name('salesagent.profile');
    Route::get('/my-rfq-sales-agent', [SalesAgentController::class, 'myRfqSalesAgent'])->name('salesagent.my-rfq-sales-agent');
    Route::get('/shortlistedOpprtunity', [SalesAgentController::class, 'shortlistedOpprtunity'])->name('salesagent.shortlistedOpprtunity');
    Route::get('/transaction-invoicer', [SalesAgentController::class, 'transactionInvoicerSalesagent'])->name('salesagent.transaction-invoicer');

    Route::get('/view', [SalesAgentController::class, 'viewSalesagent'])->name('salesagent.view');
    // Route::get('/mypost', [SalesAgentPostController::class, 'salesagentMyPost'])->name('salesagent.myPost');
    // Route::get('/posts/create', [SalesAgentPostController::class, 'create'])->name('salesagent.posts.create');
    // Route::post('/posts/store', [SalesAgentPostController::class, 'store'])->name('salesagent.posts.store');

    Route::get('/recievied-rfq-sales-agent', [SalesAgentController::class, 'recieviedRfqSalesAgent'])->name('salesagent.recievied-rfq-sales-agent');
    Route::get('/sales-agent-panel', [SalesAgentController::class, 'salesAgentPanelSalesagent'])->name('salesagent.sales-agent-panel');
    Route::get('/subscription', [SalesAgentController::class, 'subscription'])->name('salesagent.subscription');
});




Route::prefix('admin')->group(function () {
    // Admin Login
    Route::get('/login', [AuthController::class, 'adminLogin'])->name('admin.login');
    Route::post('/login', [AuthController::class, 'adminLoginPost'])->name('admin.login.post');

    // Admin Register
    Route::get('/register', [AuthController::class, 'adminRegister'])->name('admin.register');
    Route::post('/register', [AuthController::class, 'adminRegisterPost'])->name('admin.register.post');

    // Admin Logout
    Route::post('/logout', [AuthController::class, 'adminLogout'])->name('admin.logout');
});

Route::prefix('admin')->middleware(['auth', 'admin'])->group(function () {

    // Dashbaord Route
    Route::get('/dashboard', [AdminController::class, 'dashboard']);

    // Distributors Route
    Route::get('/distributors', [AdminController::class, 'distributors'])->name('admin.distributor');
    Route::get('/add/distributors', [AdminController::class, 'add_distributors'])->name('admin.add_distributor');
    Route::post('/add/distributors', [AdminController::class, 'store_distributors'])->name('admin.store_distributor');
    Route::get('/edit/distributors/{id}', [AdminController::class, 'edit_distributors']);
    Route::put('/update/distributors/{id}', [AdminController::class, 'update_distributors'])->name('admin.distributor.update');
    Route::delete('/delete/distributors/{id}', [AdminController::class, 'delete_distributors'])->name('admin.delete.distributor');
    Route::post('/filter.distributor', [AdminController::class, 'distributors'])->name('filter.distributor');


    //Suppliers Route
    Route::get('/suppliers', [AdminController::class, 'suppliers'])->name('admin.suppliers');
    Route::get('/create/suppliers', [AdminController::class, 'add_suppliers'])->name('admin.add_supplier');
    Route::post('/add/suppliers', [AdminController::class, 'store_suppliers'])->name('admin.suppliers.store');
    Route::get('/edit/suppliers/{id}', [AdminController::class, 'edit_suppliers'])->name('admin.edit.supplier');
    Route::put('/suppliers/{id}', [AdminController::class, 'update_suppliers'])->name('admin.suppliers.update');
    Route::delete('/delete/suppliers/{id}', [AdminController::class, 'destroy'])->name('admin.delete.supplier');
    Route::post('/filter.suppliers', [AdminController::class, 'suppliers'])->name('filter.suppliers');


    // Sales Agents Route
    Route::get('/sales-agent', [AdminController::class, 'salesAgent'])->name('admin.sales-agent');
    Route::get('/add/sales-agent', [AdminController::class, 'add_salesAgent'])->name('admin.add_sales-agent');
    Route::post('/add/sales-agent', [AdminController::class, 'store_salesAgent'])->name('admin.sales-agent.store');
    Route::get('/edit/sales-agent/{id}', [AdminController::class, 'edit_salesAgent'])->name('admin.edit.sales-agent');
    Route::put('/sales-agent/{id}', [AdminController::class, 'update_salesAgent'])->name('admin.sales-agent.update');
    Route::delete('/delete/sales-agent/{id}', [AdminController::class, 'destroySalesAgent'])->name('admin.delete.sales-agent');
    Route::post('/filter.sales-agent', [AdminController::class, 'salesAgent'])->name('filter.salesAgent');

    Route::get('/news-approval', [AdminController::class, 'news_approval']);

    Route::post('/approval-status', [AdminController::class, 'approval_status'])->name('admin.approval_status');



    Route::get('/profile', [AdminController::class, 'edit'])->name('admin.profile.edit');
    Route::post('/profile/{id}', [AdminController::class, 'update'])->name('admin.profile.update');
});



// web.php
Route::post('/send-phone-otp', [VerificationController::class, 'sendPhoneOTP'])->name('send.phone.otp');
Route::post('/send-email-verification', [VerificationController::class, 'sendEmailVerification'])->name('send.email.verification');
Route::post('/verify-otp', [VerificationController::class, 'verifyOTP'])->name('verify.otp');
Route::post('/verify-email-otp', [VerificationController::class, 'verifyEmailOTP'])->name('verify.email.otp');


// Route::prefix('superadmin')->middleware(['auth', 'superadmin'])->group(function () {
Route::prefix('superadmin')->group(function () {

    // Dashbaord Route
    Route::get('/dashboard', [SuperAdminController::class, 'dashboard']);

    //Region Admin Route
    Route::get('/admin', [SuperAdminController::class, 'admin'])->name('superadmin.admin');
    Route::get('/add/admin', [SuperAdminController::class, 'add_admin']);
    Route::post('/add/admin', [SuperAdminController::class, 'store_admin']);
    Route::get('/edit/admin/{id}', [SuperAdminController::class, 'edit_admin']);
    Route::post('/update/admin/{id}', [SuperAdminController::class, 'update_admin'])->name('superadmin.admin.update');
    Route::get('/delete/admin/{id}', [SuperAdminController::class, 'delete_admin']);
    Route::post('/filter-admin', [SuperAdminController::class, 'filter_admin'])->name('filter_admin');


    // Distributors Route
    Route::get('/distributors', [SuperAdminController::class, 'distributors'])->name('superadmin.distributor');
    Route::get('/add/distributors', [SuperAdminController::class, 'add_distributors'])->name('superadmin.add_distributor');
    Route::post('/add/distributors', [SuperAdminController::class, 'store_distributors'])->name('superadmin.store_distributor');
    Route::get('/edit/distributors/{id}', [SuperAdminController::class, 'edit_distributors']);
    Route::put('/update/distributors/{id}', [SuperAdminController::class, 'update_distributors'])->name('superadmin.distributor.update');
    Route::delete('/delete/distributors/{id}', [SuperAdminController::class, 'delete_distributors'])->name('superadmin.delete.distributor');
    Route::post('/filter-distributor', [SuperAdminController::class, 'filter_distributors'])->name('filter_distributor');
    Route::post('/filter.distributor', [SuperAdminController::class, 'distributors'])->name('superadmin.filter.distributor');


        //Suppliers Route
    ;
    Route::get('/suppliers', [SuperAdminController::class, 'suppliers'])->name('superadmin.suppliers');
    Route::get('/create/suppliers', [SuperAdminController::class, 'add_suppliers'])->name('superadmin.add_supplier');
    Route::post('/add/suppliers', [SuperAdminController::class, 'store_suppliers'])->name('superadmin.suppliers.store');
    Route::get('/edit/suppliers/{id}', [SuperAdminController::class, 'edit_suppliers'])->name('superadmin.edit.supplier');
    Route::put('/suppliers/{id}', [SuperAdminController::class, 'update_suppliers'])->name('superadmin.suppliers.update');
    Route::delete('/suppliers/{id}', [SuperAdminController::class, 'destroy'])->name('superadmin.delete.supplier');
    Route::post('/filter-suppliers', [SuperAdminController::class, 'suppliers'])->name('superadmin.filter_suppliers');


    // Sales Agents Route
    Route::get('/sales-agent', [SuperAdminController::class, 'salesAgent'])->name('superadmin.sales-agent');
    Route::get('/add/sales-agent', [SuperAdminController::class, 'add_salesAgent'])->name('superadmin.add_sales-agent');
    Route::post('/add/sales-agent', [SuperAdminController::class, 'store_salesAgent'])->name('superadmin.sales-agent.store');

    Route::get('/edit/sales-agent/{id}', [SuperAdminController::class, 'edit_salesAgent'])->name('superadmin.edit.sales-agent');
    Route::put('/sales-agent/{id}', [SuperAdminController::class, 'update_salesAgent'])->name('superadmin.sales-agent.update');
    Route::delete('/sales-agent/{id}', [SuperAdminController::class, 'destroySalesAgent'])->name('superadmin.delete.sales-agent');
    Route::post('/filter/sales-agent', [SuperAdminController::class, 'salesAgent'])->name('superadmin.filter.salesAgent');

    //News Approval
    Route::get('/news-approval', [SuperAdminController::class, 'news_approval']);

    //messages
    Route::get('/messages', [SuperAdminController::class, 'messages']);


    Route::post('/approval-status', [SuperAdminController::class, 'approval_status'])->name('superadmin.approval_status');
});
